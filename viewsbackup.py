from models.Company import Company
from models.Member import Member
from models.WebhookManager import WebhookManager
from models.Customer import CompanyCustomer
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as Authlogout
from models.Logger import Logger
from django.shortcuts import redirect
import time
from Public.ViewDecorators import *
from Public.Responses import *

# views
@accessible.include()
class Load(object):
    @staticmethod
    @fillInputs(method='get')
    def user(member,httpResponse=True):
        isLoggedIn=False
        if member is not None:
            isLoggedIn=True
        response={
            'isLoggedIn': isLoggedIn,
        }
        if httpResponse is True:
            response=json.dumps(response)
            response=HttpResponse(response)
        return response
    @classmethod
    @fillInputs(method='get')
    def all(cls,member,pricingTemplate,customerPage=1,apiPage=1):
        company=member.getCompany()
        response={}
        response['user']=cls.user(byPass=True,member=member,httpResponse=False)
        if member is not None:
            # response['dashboard']=cls.dashboard(member=member)
            response['customers']=Customer.get(company=company,httpResponse=False,byPass=True)
            response['templates']=Template.get(byPass=True,pricingTemplate=pricingTemplate,httpResponse=False)
            # response['apiCalls']=cls.apiCalls(member=member,page=apiPage)
            # response['settings']=cls.settings(member=member)
            response['members']=MemberView.get(member=member,httpResponse=False,byPass=True)
        response=json.dumps(response)
        return HttpResponse(response)
@accessible.include()
class Template(object):
    @staticmethod
    @fillInputs(method='get')
    def get(pricingTemplate,httpResponse=True):
        response=pricingTemplate.json()
        if httpResponse is True:
            response=json.dumps(response)
            response=HttpResponse(response)
        return response
    @staticmethod
    @fillInputs(method='get')
    def list(unitTemplateManager):
        response=unitTemplateManager.json()
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
@accessible.include()
class Webhook(object):
    @staticmethod
    @fillInputs(method='post')
    def edit(pricingTemplate,webhookManager,webhooks):
        webhookManager.processRequest(webhooksInput=webhooks)
        pricingTemplate.edit(webhookManager=webhookManager)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get')
    def get(pricingTemplate,webhookManager,httpResponse=True):
        response={
            'list': webhookManager.json()
        }
        if httpResponse is True:
            response=json.dumps(response)
            response=HttpResponse(response)
        return response
@accessible.include(className='member')
class MemberView(object):
    @staticmethod
    @fillInputs(method='get')
    def edit(member,email):
        member.edit(email=email)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get')
    def editPassword(member,currentPassword,newPassword):
        state=member.editPassword(currentPassword=currentPassword,newPassword=newPassword)
        if state is False:
            return InfoDidntMatchRecordResponse.get(context={'entity':'password'})
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get')
    def remove(member,memberID):
        state=member.removeMember(memberID=memberID)
        if state==-1:
            return DoesntExistResponse.get(context={'entityName': 'Member'})
        if state is False:
            return DontHavePermissionResponse.get()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get',authenticated=False)
    def add(email,password,code):
        state=Member.Invitation.createMember(
            email=email,
            password=password,
            code=code
        )
        if state==-2:
            return ExpiredResponse.get(context={'entityName': 'The entered code'})
        if state==-3:
            return CustomResponse.get(context={
                "message": 'There already exists an account with the given email address'
            })
        if state==-4:
            return DoesntExistResponse.get(context={
                'entityName': 'Entered code'
            })
        if isinstance(state,Member):
            member=state.json()
            member=json.dumps(member)
            member=HttpResponse(member)
            return member
    @staticmethod
    @fillInputs(method='get',authenticated=False)
    def logout(request):
        Authlogout(request)
        return SuccessfulRequestResponse.get()
# MemberView.login=csrf_exempt(MemberView.login)
@accessible.include()
class Unit(object):
    @classmethod
    @fillInputs(method='post')
    def create(cls,unitID,unitName,unitDescription,pricingTemplate,unitManager,resetEachInterval=False,atEnd=False,pricing=[]):
        try:
            unitID=str(unitID)
            unit=unitManager.add(id=unitID,name=unitName,description=unitDescription)
        except:
            return AlreadyExistResponse.get(context={'EntityName': 'A unit with the given unit ID'})
        return cls.edit(
            byPass=True,
            unit=unit,
            pricing=pricing,
            pricingTemplate=pricingTemplate,
            resetEachInterval=resetEachInterval,
            atEnd=atEnd,
            unitManager=unitManager
        )
    @staticmethod
    @fillInputs(method='post')
    def edit(unit,unitManager,pricingTemplate,unitName=None,unitDescription=None,atEnd=None,resetEachInterval=None,pricing=[]): 
        unit.edit(name=unitName,description=unitDescription)
        unit.PricingManager.set(pricings=pricing)
        if resetEachInterval=='true': resetEachInterval=True
        elif resetEachInterval=='false': resetEachInterval=False
        if atEnd=='true': atEnd=True
        elif atEnd=='false': atEnd=False
        unit.Applier.edit(atEnd=atEnd)
        unit.Quantity.edit(resetEachInterval=resetEachInterval)
        pricingTemplate.edit(unitManager=unitManager)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post')
    def remove(pricingTemplate,unitManager,unitID):
        unitID=str(unitID)
        unitManager.remove(id=unitID)
        pricingTemplate.edit(unitManager=unitManager)
        return SuccessfulRequestResponse.get()
@accessible.include(className='company')
class CompanyView(object):
    @staticmethod
    @fillInputs(method='post')
    def getAPIKey(member,password):
        APIKey=member.getAPIKey(password=password)
        if APIKey is False:
            return WrongResponse.get(context={'entityName': 'password'})
        response={
            'APIKey': APIKey
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @fillInputs(method='get')
    def testAPIKey(company,APIKey):
        status=company.authenticate(APIKey=APIKey)
        response={
            'status': status
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @fillInputs(method='get')
    def addGateWayCredentials(company,gateWayConnector,GET):
        # try:
        gateWayConnector.Credentials.authorize(GETParameters=GET,company=company)
        # except:
        #     return AnErrorOccuredResponse.get()
        company.save()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get',authenticated=False)
    def isConnected(gateWayConnector):
        response={
            'isConnected': gateWayConnector.isConnected()
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @fillInputs(method='post')
    def edit(company,name=None,currency=None,endpointURL=None):
        company.edit(name=name,currency=currency,endpointURL=endpointURL)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post')
    def get(company,member=None,httpResponse=True):
        response=company.json()
        response['authenticatedUser']={}
        response['authenticatedUser']['loggedIn']=False
        if member is not None:
            response['authenticatedUser']['loggedIn']=True
        if httpResponse is True:
            response=json.dumps(response)
            response=HttpResponse(response)
        return response
    @staticmethod
    @fillInputs(method='get')
    def resetAPI(member,password):
        state=member.resetAPI(password=password)
        if state is None:
            return WrongResponse.get(context={'entityName': 'password'})
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post',authenticated=False)
    def register(request,placeholder,email,password):
        state=Company.register(
            placeholder=placeholder,
            email=email,
            password=password,
        )
        if state==-1:
            return AnErrorOccuredResponse.get()
        elif state==-2:
            return CustomResponse.get(context={
                "message": 'There already exists an account with the given email address'
            })
        # return SuccessfulRequestResponse.get()
        return login(
            byPass=True,
            request=request,
            email=email,
            password=password
        )
    @staticmethod
    @fillInputs(method='get')
    def getAPICalls(company):
        response=Logger.apiCallsView(company=company,json=True)
        response=HttpResponse(response)
        return response
    @staticmethod
    def registerEmptyCompany(request,*args,**kwargs):
        state=Company.createEmpty()
        if state==-1:
            return AlreadyExistResponse.get(context={'EntityName': 'company'})
        if state==-2:
            return AnErrorOccuredResponse.get()
        placeholder=state.name
        return login(
            byPass=True,
            request=request,
            email=placeholder,
            password=placeholder
        )
@accessible.include()
class Quantity(object):
    @staticmethod
    @fillInputs(method='get',APIAccessible=True,record=True)
    def add(company,customer,quantity,units=[]):
        pricingTemplate=customer.getPricingTemplate()
        unitManager=pricingTemplate.getUnitManager()
        units=units.split(',')
        for unitID in units:
            unit=unitManager.get(id=unitID)
            if unit is False:
                return WrongResponse.get(context={'entityName': 'unit id: '+ unitID})
            unit.Quantity.add(quantity=quantity)
        unitManager.save()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get',APIAccessible=True,record=True)
    def set(company,customer,quantity,units=[]):
        pricingTemplate=customer.getPricingTemplate()
        unitManager=pricingTemplate.getUnitManager()
        units=units.split(',')
        for unitID in units:
            unit=unitManager.get(id=unitID)
            if unit is False:
                return WrongResponse.get(context={'entityName': 'unit id: '+ unitID})
            unit.Quantity.set(quantity=quantity)
        unitManager.save()
        return SuccessfulRequestResponse.get()
@accessible.include()
class Customer(object):
    @staticmethod
    @fillInputs(method='get')
    def add(company,gateWayID,pricingTemplate):
        try:
            customer=company.addCustomer(
                gateWayID=gateWayID,
                pricingTemplate=pricingTemplate
            )
        except:
            return DoesntExistResponse.get(context={
                'entityName':"Your gateway doesn't have a customer with the given ID"
            })
        if customer == -1:
            return CustomResponse.get(context={
                'message': 'You have to connect to your payment gateway provider before you add customers'
            })
        if customer == -2:
            return AlreadyExistResponse.get(context={
                'EntityName': 'Customer with the given gateWayID'
            })
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post')
    def update(customer):
        customer.updatePricing()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post')
    def bulkUpdate(templateID,password,member):
        if not member.authenticate(password=password):
            return WrongResponse.get(context={'entityName': 'password'})

        CompanyCustomer.bulkUpdate(templateID=templateID)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get',authenticated=False)
    def sync(customer):
        customer.sync()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get',authenticated=False)
    def applyUsage(customer,subscriptionID):
        customer.apply(subscriptionID=subscriptionID)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='post',authenticated=False)
    def setCustomPricing(customer,unitID,unitName=None,unitDescription=None,atEnd=None,resetEachInterval=None,pricing=[]):
        if resetEachInterval=='true': resetEachInterval=True
        elif resetEachInterval=='false': resetEachInterval=False
        if atEnd=='true': atEnd=True
        elif atEnd=='false': atEnd=False

        pricingTemplate=customer.PricingTemplate
        unitManager=pricingTemplate.getUnitManager()
        unit=unitManager.get(id=unitID)

        unit.edit(name=unitName,description=unitDescription)
        unit.PricingManager.set(pricings=pricing)
        unit.Applier.edit(atEnd=atEnd)
        unit.Quantity.edit(resetEachInterval=resetEachInterval)

        customer.setCustom(unitManager=unitManager)

@accessible.include()
class GateWay(object):
    @staticmethod
    @fillInputs(method='post')
    def edit(gateWayConnector,preChargeAmount=0):
        try:
            preChargeAmount=int(preChargeAmount)
        except:
            return CustomResponse.get(context={
                'message': "The precharge amount has to be an integer"
            })
        gateWayConnector.setPreChargeAmount(preChargeAmount=preChargeAmount)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @fillInputs(method='get')
    def connect(company,gateWayConnector):
        # return HttpResponse(type(gateWayConnector))
        link=gateWayConnector.Credentials.buildLink()
        company.save()
        return redirect(link)
@accessible.include()
class StripeWebhookHandler(object):
    @staticmethod
    def getEvent(request):
        endpoint_secret="whsec_jWJuihmZlcil14nwVE2M26ih90VCPSMJ"
        payload=request.body
        sig_header=request.META['HTTP_STRIPE_SIGNATURE']
        event=None
        try:
            event=stripe.Webhook.construct_event(
                payload, sig_header, endpoint_secret
            )
        except ValueError as e:
            return False
        except stripe.error.SignatureVerificationError as e:
            return False
        return event
    @classmethod
    def handle(cls,request):
        event=cls.getEvent(request=request)
        if event is False: return HttpResponse(status=400)
        user_id=event.user_id
        company=Company.get(stripe_user_id=user_id)
        if company is False: return HttpResponse(status=400)
        if event.type=='customer.updated':
            cls.customerUpdate(event=event,company=company)
        return HttpResponse(status=200)
    @staticmethod
    def customerUpdate(event,company):
        gateWayID=event.data.object.id
        userID=event.user_id
        customer=company.getCustomer(gateWayID=gateWayID)
        customer.sync()
    def customerCreated(event,company):
        gateWayID=event.data.object.id
        company.addCustomer(gateWayID=gateWayID)
    def applyUsage(event,company):
        gateWayID=event.data.object.customer
        lines=event.data.object.lines.data
        subscriptionIDs=[]
        for subscription in lines:
            subscriptionID=subscription.id
            if subscriptionID[0:3]=='sub':
                company.apply(
                    subscriptionID=subscriptionID,
                    gateWayID=gateWayID
                )

@accessible.include(pattern='api/member/login')
@csrf_exempt
@fillInputs(method='post',authenticated=False)
def login(request,email,password):
    state=Member.login(
        request=request,
        email=email,
        password=password
    )
    if state==-1:
        return NotFoundResponse.get(context={'EntityName': 'Member'})
    if state==-2:
        return DoNotMatchResponse.get(context={'EntitiesName': 'Company name, email and password'})
    if state is True:
        return SuccessfulRequestResponse.get()
@accessible.include(className='js')
class JSLibrary(object):
    jsLibraryPath="Public/jsLibrary/"
    @staticmethod
    @fillInputs(authenticated=False,file=jsLibraryPath+'main.js')
    def load(request,publicKey,template):
        company=Company.get(publicAPIKey=publicKey)
        if company is False:
            return WrongResponse.get(context={'entityName': 'public API key'})
        unitTemplateManager=company.UnitTemplateManager
        pricingTemplate=unitTemplateManager.getDefault()
        if pricingTemplate is None:
            units=[]
        else:
            unitManager=pricingTemplate.UnitManager
            units=unitManager.json()
        data={
            'units': units,
        }
        data=json.dumps(data)
        template=template.render(DjangoContext({
            'PublicAPIKey': publicKey,
            'data': data
        }))
        
        template.replace('\n','')
        template.replace('\t','')
        return HttpResponse(template)
    @staticmethod
    @fillInputs(authenticated=False,file=jsLibraryPath+'customer.js')
    def customer(request,template,publickey=None,gatewayid=None):

        if publickey is None or gatewayid is None:
            return MissingArgumentsResponse.get()
        company=Company.get(publicAPIKey=publickey)
        if company is False:
            return WrongResponse.get(context={'entityName': 'public API key'})

        customer=CompanyCustomer.get(gateWayID=gatewayid,company=company)

        if customer is False:
            return WrongResponse.get(context={'entityName': 'customer gateway id'})
        customer=json.dumps(customer.json())
        template=template.render(DjangoContext({
            'customer': customer
        }))
        template.replace('\n','')
        template.replace('\t','')
        return HttpResponse(template)
@accessible.include()
def testing(request):
    return HttpResponse("console.log('lalalala');")
def functionRunner(func,*args,**kwargs):
    def wrapper():
        return func(*args,**kwargs)
    return wrapper


