import React, { Component } from 'react';
import { withRouter, matchPath } from 'react-router'
var generateID=function(){
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 30; i++)
       text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}

class ComponentRouter extends Component{
    constructor(props){
        super(props);
        this.state={routes: [],history: []}
    }
    componentDidMount(){
        for( var cursor in this.props.routes ){
            var route=this.props.routes[cursor];
            this.add(
                route.type,
                route.path,
                route.props,
                route.exact,
                route.strict
            );
        }
    }
    add(type,path,props,exact,strict){
        if(typeof exact == 'undefined'){
            exact=true;
        }
        if(typeof strict == 'undefined'){
            strict=false;
        }
        if(typeof props == 'undefined'){
            props={};
        }
        var routes=this.state.routes;
        var newRoute={
            match: {path: path, exact: exact, strict: strict},
            type: type,
            props: props
        };
        routes.push(newRoute);
        this.setState( {routes: routes} )
    }
    getContent(){
        var path=this.props.location.pathname
        this.state.history.push(path)
        for( var cursor=0; cursor < this.state.routes.length; cursor++ ){
            var route=this.state.routes[cursor];
            var match=route.match;
            var type=route.type;
            var matchResult=matchPath( path, match )
            if( matchResult ){
                var props=route.props
                for( var attrName in matchResult.params ){
                    props[attrName]=matchResult.params[attrName]
                }
                props['routeHistory']=this.state.history;
                props['currentPath']=path;
                props['key']=generateID();
                return React.createElement(
                    type,
                    props
                )
            }
        }
        return (
            <div className="notFound">
                <i className="symbol fa fa-frown-o" 
                aria-hidden="true"></i>
                Page was not found
            </div>
        )
    }
    render(){
        return <div>{this.getContent()}</div>
    }
}
ComponentRouter=withRouter(ComponentRouter)
export default ComponentRouter
