import React, { Component } from 'react';
import APPManager from './APPManager';
/*
Connect to stripe
MicroPayment
    Pre charge settings

Endpoint URL
Team management
*/
class ConnectToGateWay extends Component{
    constructor(props){
        super(props);
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    update(){
        this.setState({})
    }
    render(){
        var text='CONNECT TO STRIPE'
        if(APPManager.Retrievers.isConnected()){
            text='CONNECT TO ANOTHER STRIPE ACCOUNT';
        }
        return (
            <div className='section'>
                <a href="/api/gateway/connect" className='button'>
                    <i className="fa fa-cc-stripe symbol"
                        aria-hidden="true">
                    </i>
                    {text}
                </a>
            </div>
        );
    }
}
// class MicroPayments extends Component{
//     constructor(props){
//         super(props);
//         this.state={};
//         this.state.preChargeAmount=this.props.preChargeAmount;
//         this.handleInputChange=this.handleInputChange.bind(this);
//     }
//     handleInputChange(event){
//         const target = event.target;
//         var value = target.type === 'checkbox' ? target.checked : target.value;
//         const name = target.name;
//         value=parseInt(value);
//         if( isNaN( value )){value=0}
//         this.setState({preChargeAmount: value});
//         this.props.updater({preChargeAmount: value});
//     }
//     render(){
//         return (
//             <div className='section'>
//                 <div className='sectiontitle'>Pre charge settings</div>
//                 <div className='title'>
//                     Precharge amount(in cents).
//                     Customers will be precharged when their balance reaches zero
//                 </div>
//                 <div className='field'>
//                     <input type='text'
//                         className='text'
//                         name='amount'
//                         value={this.state.preChargeAmount}
//                         onChange={this.handleInputChange}
//                         placeholder='Amount in cents'/>
//                 </div>
//             </div>
//         );
//     }
// }
//Wraps everything
class WebhookEndpoint extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.endpointURL=this.props.endpointURL;
        this.handleInputChange=this.handleInputChange.bind(this)
    }
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({endpointURL: value});
        this.props.updater({endpointURL: value});
    }

    render(){
        return(
            <div className='section'>
                <div className='sectiontitle'>Webhooks' endpoint</div>

                <div className='title'>
                    The URL of your webhook handler
                </div>
                <div className='field long'>
                    <input type='url'
                        className='text'
                        name='amount'
                        placeholder='Your webhook handler URL'
                        onChange={this.handleInputChange}
                        value={this.state.endpointURL}/>
                    <br/>
                    <br/>
                    <div className='button' onClick={APPManager.Unit.generateMockWebhook}>Generate a mock webhook</div>
                </div>
            </div>

        )
    }
}
class SettingsView extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.settings=null;
        this.submit=this.submit.bind(this);
        this.update=this.update.bind(this);
        this.settingsStateUpdater=this.settingsStateUpdater.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    update(){
        var settings=APPManager.Retrievers.getSettings();
        if(settings===null){return null;}
        this.setState({settings:settings});
    }
    settingsStateUpdater(args){
        var settings=this.state.settings;
        for(var el in args){
            if(typeof el=='undefined'){continue;}
            settings[el]=args[el];
        }
        this.setState(settings);
    }
    submit(){
        APPManager.Company.updateSettings(this.state.settings);
    }
    render(){
        // <MicroPayments updater={this.settingsStateUpdater} preChargeAmount={this.state.settings.preChargeAmount}/>

        console.log(this.state.settings)
        if(this.state.settings===null){return <div></div>;}
        return (
            <div className='sectionswrapper'>
                <ConnectToGateWay/>
                <WebhookEndpoint updater={this.settingsStateUpdater} endpointURL={this.state.settings.endpointURL}/>
                <div className='submit' onClick={this.submit}>SAVE CHANGES</div>
            </div>

        );
    }
}
export default SettingsView;
