import React, { Component } from 'react';
import AjaxWrapper from './AjaxWrapper'
class Register extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.name='';
        this.state.email='';
        this.state.password='';
        this.handleInputChange=this.handleInputChange.bind(this);
        this.register=this.register.bind(this);
        this.state.errorMessage=""
    }
    register(){
        var requestData={};
        requestData.name=this.state.name;
        requestData.email=this.state.email;
        requestData.password=this.state.password;
        AjaxWrapper('register/', requestData ).then((function(response){
            var data=response.data;
            var status=data.status;
            if(status=='success'){
                window.location='/dashboard';
            }else{
                this.setState({errorMessage: response.message});
            }
        }).bind(this))
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
    render(){
        return (
            <div className='wrapper'>
                <div className='CenterForm'>
                    <div className='formTitle'>
                        Register your company
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='text'
                            className='text'
                            placeholder='Company name'
                            value={this.state.name}
                            name='name'
                            onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='text'
                            className='text'
                            placeholder='Email address'
                            value={this.state.email}
                            name='email'
                            onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='password'
                            className='text'
                            placeholder='password'
                            value={this.state.password}
                            name='password'
                            onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <div className='submit'
                                onClick={this.register}>REGISTER</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Register
