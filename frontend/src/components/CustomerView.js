import React, { Component } from 'react';
import ComponentRouter from './ComponentRouter';
import { Link } from 'react-router-dom';
import ReportView from './ReportView';

import {
    BrowserRouter as Router,
    Route,
    Switch 
} from 'react-router-dom';
import Chart from 'chart.js';
import APPManager from './APPManager';
var generateID=function(){
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 30; i++)
       text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}

//Customer view
class UnitWrapper extends Component{
    constructor(props){
        super(props);
    }
    render(){
        var idWidth=340;
        var nameWidth=13;

        var unitID=<b>UNIT ID</b>;
        var unitName=<b>UNIT NAME</b>;
        var linkStart="";
        var linkEnd="";
        var className="Element"

        if(typeof this.props.content!='undefined'){
            var unitID=this.props.content.id;
            var unitName=this.props.content.name;
            var className="Element"
        }
        // {typeof this.props.content!='undefined'?<Link to={"/units/"+unitID+"/editpricing"}>:''}
        var elementContent=(
            <div className={className}>
                <div className="column" style={{width:idWidth}}>
                    {unitID}
                </div>
                <div className="column" style={{width:'auto'}}>
                    {unitName}
                </div>
            </div>
        )
        if(typeof this.props.content!='undefined'){
            var unitEditorLink="/units/"+this.props.gateWayID+"/"+unitID+"/edit";
            elementContent=(
                <Link to={unitEditorLink}>
                    {elementContent}
                </Link>
            )
        }
        return (
            <div className='list'>
                <div className="ElementsWrapper">
                    {elementContent}
                </div>
            </div>
        )
    }
}
class ViewCustomer extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state['usageHistory']=null;
        this.state['usingCurrentPricing']=true;
        this.state['customer']={};
        this.state['units']=[];
        this.state['customerLoaded']=false;
        this.handleInputChange=this.handleInputChange.bind(this);
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    update(){
        // console.log("ViewCustomer:update 1==========================");
        //get gateway ids
        var gateWayID=this.props.gateWayID;
        var year=(new Date()).getFullYear();
        var state={};

        // console.log("ViewCustomer:update 2");
        //get the customer
        var customer=APPManager.Retrievers.getCustomer(gateWayID);

        if(customer==null){
            return null;
        }
        // console.log("ViewCustomer:update 3");
        state['customer']=customer;

        //Setting history/graph
        state['usageHistory']=APPManager.Retrievers.getCustomerHistory(gateWayID,customer);
        // console.log(state['usageHistory']);
        // console.log(customer);
        // console.log(state['usageHistory'])
        // console.log("ViewCustomer:update 4");

        //determine whether customer is using the default pricing or not
        state['usingCurrentPricing']=false;
        // var currentTemplate=APPManager.Retrievers.getTemplate();
        // if(!(currentTemplate===null)){
        //     var customerTemplate=APPManager.Retrievers.getCustomerTemplate(gateWayID);
        //     if(!(customerTemplate===null)){
        //         if(customerTemplate.id==currentTemplate.id){
        //             state['usingCurrentPricing']=true;
        //         }
        //     }
        // }
        // console.log("ViewCustomer:update 5");

        state['units']=APPManager.Retrievers.getCustomerUnits(state['customer']);

        if(state['units']===null){state['customer']=[];}
        // console.log("ViewCustomer:update 6");

        state['customerLoaded']=true;
        // console.log("ViewCustomer:update 7");

        this.setState(state);
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        // this.setState({[name]: value});
        // var selectedVersion=undefined;
        // if(name=='selectedTemplate'){
        //     if(value==this.state.initialTemplate){
        //         selectedVersion=this.state.initialVersion;
        //     }else{
        //         var template=this.getSelectedTemplate();
        //         selectedVersion=template.versions.length;
        //     }
        //     this.setState({selectedVersion: selectedVersion});
        // }
    }
    customerInfo(){
        return (
            <div className='infoWrapper'>
                <div className='infoPair'>
                    <div className="infoName">GATEWAY ID:</div> 
                    <div className='infoValue'>{this.props.gateWayID}</div>
                </div>
            </div>
        )
    }
    unitsList(){
        return (
            <div>
                <UnitWrapper/>
                {this.state.units.map((function(unit){
                    return <UnitWrapper content={unit} gateWayID={this.state.customer.gateWayID} />;
                }).bind(this))}
            </div>
        )
    }
    render(){
        if(!this.state.customerLoaded){
            return <Search/>
        }
        // console.log(this.state.usageHistory);
        return (
            <div>
                {this.unitsList()}
                <br/>
                <ReportView 
                    history={this.state.usageHistory} 
                    customer={true}/>
            </div>
        )
    }
}
//A list of customers
class List extends Component {
    constructor(props){
        super(props);
        this.state={
            customers: [],
            maxDisplayCount: 20,
            resultCount: 0,
            resultCursor: 1,
            search: ''
        };
        this.nextPage=this.nextPage.bind(this);
        this.prevPage=this.prevPage.bind(this);
        this.update=this.update.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
    update(){
        var state={};
        state.customers=APPManager.Retrievers.listCustomers();
        if(state.customers===null){state.customers=[];}
        state.resultCount=state.customers.length;
        this.setState(state);
    }
    nextPage(){
        var resultCursor=this.state.resultCursor+1;
        var requestCursor=resultCursor*this.state.maxDisplayCount;
        var fetch=this.fetch(requestCursor);
        this.setState({
            resultCursor: resultCursor,
            customers: fetch.customers,
            resultCount: fetch.resultCount,
        });
    }
    prevPage(){
        var resultCursor=this.state.resultCursor-1;
        var requestCursor=resultCursor*this.state.maxDisplayCount;
        var fetch=this.fetch(requestCursor);
        this.setState({
            resultCursor: resultCursor,
            customers: fetch.customers,
            resultCount: fetch.resultCount,
        });
    }
    buidList(){
        var emailWidth='600px';
        var gateWayIDWidth='300px'
        var list=[];
        this.state.customers.map(function(customer){
            list.push(
                <Link to={'/customers/view/'+customer.gateWayID} className='Element'>
                    <div className="column" style={{width:emailWidth}}>
                        {customer.email}
                    </div>
                    <div className='column' style={{width:gateWayIDWidth}}>
                        {customer.gateWayID}
                    </div>
                </Link>
            )
        });
        return list;
    }
    searchButton(){
        var button=(
            <Link className='searchButton' to={'/customers/view/'+this.state.search}>GO</Link>
        )
        if(this.state.search==''){
            return '';
        }
        return button;
    }
    loadMoreButton(){
        if(this.state.customers.length < 24){return}
        return (
            <div className='loadMore' onClick={APPManager.loadCustomers}>
                Load more
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className='toolBar'>
                    <input type='text' 
                        className='search' 
                        name='search'
                        onChange={this.handleInputChange}
                        value={this.state.search}
                        placeholder="Enter gateway ID, e.g. stripe id"/>
                    {this.searchButton()}
                </div>
                <div className='list'>
                    <div className='ElementsWrapper'>
                        {this.buidList()}
                    </div>
                    {this.loadMoreButton()}
                </div>
            </div>
        )
    }
}
class Search extends Component{
    constructor(props){
        super(props)
        this.handleInputChange=this.handleInputChange.bind(this);
        this.state={};
        this.state.search=''        
    }
    searchButton(){
        var button=(
            <Link className='searchButton' to={'/customers/view/'+this.state.search}>GO</Link>
        )
        // if(this.state.search==''){
        //     return '';
        // }
        return button;
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        // const name = target.name;
        this.setState({'search': value});
    }
    render(){
        return (
            <div className='customerSearch'>
                <div className='title'>View customers</div>
                <input placeholder='GATEWAY ID' 
                    className='searchBox' 
                    value={this.state.search} 
                    onChange={this.handleInputChange}/>
                {this.searchButton()}
            </div>
        )
    }
}
//Wraps everything
class CustomerView extends Component{
    constructor(props){
        super(props)
        var routes=[
            { type: List, path: '/customers/' },
            { type: ViewCustomer, path: '/customers/view/:gateWayID', props: {history: this.props.history} },
        ]
        this.state={routes: routes};
    }
    render(){
        return(
            <div>
                <ComponentRouter routes={this.state.routes} />
            </div>
        )
    }
}
export default CustomerView;
