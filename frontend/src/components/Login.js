import React, { Component } from 'react';
import AjaxWrapper from './AjaxWrapper';
class Login extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.companyName='';
		this.state.memberEmail='';
		this.state.password='';
		this.login=this.login.bind(this);
		this.handleInputChange=this.handleInputChange.bind(this);
	}
	login(){
		var data={};
		data['companyName']=this.state.companyName;
		data['email']=this.state.memberEmail;
		data['password']=this.state.password;
		AjaxWrapper('member/login',data).then(function(response){
		})
	}
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.state[name]=value;
    }
	render(){
		return (
			<div className='wrapper'>
				<div className='CenterForm'>
                    <div className='formTitle'>
                        Register your company
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='text'
                            className='text'
                            placeholder='Company name'
                            name='companyName'
                            onChange={this.handleInputChange}
                             />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='text'
                            className='text'
                            placeholder='Email address'
                            name='memberEmail'
                            onChange={this.handleInputChange}
                             />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <input type='password'
                            className='text'
                            placeholder='password'
                            name='password'
                            onChange={this.handleInputChange}
                             />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <div className='submit'
                            	onClick={this.login}
                                >LOGIN</div>
                        </div>
                    </div>
                </div>
			</div>
		);
	}
}
export default Login