import React, { Component } from 'react';
class Header extends Component {
    render(){
        return (
            <div className='Header'>
                <div className='Logo'>SubFrog</div>
                <a href="#" className='LogoutButton'>
                    LOGOUT
                    &nbsp;
                    <i className="fa fa-sign-out" aria-hidden="true"></i>
                </a>
            </div>
        )
    }
}

export default Header;
