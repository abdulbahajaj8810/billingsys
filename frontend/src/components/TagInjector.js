import React, {Component} from 'react'

const Inject=function(element,props,position){
	if(typeof props!='undefined'){
		for(var name in props){
			var value=props[name];
	        element.setAttribute(name,value);
		}
	}
	if(typeof position=='undefined'){
	    document.head.appendChild(element);
	}else{
		position.appendChild(element);
	}

}
const ScriptInjector=function(props){
	var script = document.createElement("script");
	Inject(script,props);
}
const ImageInjector=function(props,position){
	var script = document.createElement("image");
	Inject(script,props,position);

}


export {ScriptInjector,ImageInjector};