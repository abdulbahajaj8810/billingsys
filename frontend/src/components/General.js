import React, { Component } from 'react';
class Table extends Component{
    render(){
        return (
            <div className='Table'>
                {this.props.columnsWidth.map(function(columnWidth,count){
                    return (
                        <div className='column' style={{width: columnWidth}}>

                        </div>
                    )
                })}
            </div>
        )
    }
}
class InfoEditorRow extends Component{
    constructor(props){
        super(props);
    }
    buildContent(type){
        var content=type;
        if( typeof type=='string'){
            if(type=='text'){
                content=<input type='text' className='text'/>;
            }
        }
        return content;
    }
    buildSubRows(){
        return (
            <span>
                <div className='subRow'>
                    <div className='title'>
                        {this.props.title1}
                    </div>
                    <div className='content'>
                        {this.buildContent(this.props.content1)}
                    </div>
                </div>
                <div className='subRow'>
                    <div className='title'>
                        {this.props.title2}
                    </div>
                    <div className='content'>
                        {this.buildContent(this.props.content2)}
                    </div>
                </div>
            </span>
        )
    }
    buildFullRow(){
        return (
            <span>
                <div className='title'>
                    {this.props.title1}
                </div>
                <div className='content'>
                    {this.buildContent(this.props.content1)}
                </div>
            </span>
        )
    }
    buildRow(){
        if(typeof this.props.title1=='undefined'){return '';}
        if(typeof this.props.title2=='undefined'){return this.buildFullRow();}
        else{return this.buildSubRows();}
    }
    render(){
        return(
            <div className='infoRow'>
                {this.buildRow()}
            </div>
        )
    }
}
/*
<InfoEditor rows=[
    {title1: 'sometitle', content: 'text',title1: 'sometitle', content: 'other type of input' }
] />
*/
class InfoEditor extends Component{

    buildRows(){
        return this.props.rows.map(function(row){
            return (
            <InfoEditorRow
                title1={row['title1']}
                title2={row['title2']}
                content1={row['content1']}
                content2={row['content2']}/>
            )
        });
    }
    render(){
        return (
            <div className='infoWrapper'>
                {this.buildRows()}
            </div>
        )
    }
}
