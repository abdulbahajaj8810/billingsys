import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ComponentRouter from './ComponentRouter';
import {UnitEditor} from './UnitView';
import APPManager from './APPManager';
import {ScriptInjector,ImageInjector} from './TagInjector';
import StripeLogo from './images/StripeLogo.png';
import {
    XYPlot, 
    XAxis, 
    YAxis, 
    HorizontalGridLines,
    VerticalBarSeries, 
    LineSeries,
    VerticalGridLines,
    MarkSeries,
    Crosshair,
    LineMarkSeries
} from 'react-vis';



var generateID=function(){
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 30; i++)
       text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}
class Feature extends Component{
	render(){
		return (
			<div className='Feature'>
				<div className='title'>
					{this.props.title}
				</div>
				<div className='description'>
					{this.props.description}
				</div>
			</div>
		)
	}
}
class FeaturesList extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.features=[
			{
				title: 'Custom pricing for customers',
				description: 'Give your customers custom pricing and custom usage limits',
			},
			{
				title: 'Grandfather customers',
				description: 'Your customers can keep using the pricing that they are used to while you experiment with new pricing',
			},
			{
				title: 'Expressive pricing editor',
				description: 'Using our pricing models your can charge your customer differently based on their plan, trial status and usage',
			},
			{
				title: 'Stay in the loop with webhooks',
				description: 'Be notified when your customers reach their usage limit or any other usage related event through easy to setup webhooks.',
			},
			{
				title: 'Usage and revenue report',
				description: 'View your customer’s usage and your revenue on monthly basis',
			},
		]
	}
	render(){
		return (
			<div className='FeaturesList'>
				{this.state.features.map(function(feature){
					return <Feature title={feature.title} description={feature.description}/>
				})}
			</div>
		)
	}
}
class GetInButton extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.csrf=this.getCookie('csrftoken');
	}
	getCookie(cname){
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i <ca.length; i++){
	        var c = ca[i];
	        while (c.charAt(0) == ' '){
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0){
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}
	render(){
		var additionalClasses='';
		if(this.props.absolute===false){
			additionalClasses='notAbsolute';
		}
		return (
			<form action='http://localhost:8000/api/company/registeremptycompany' method='POST'>
				<input type='hidden' name='csrfmiddlewaretoken' value={this.state.csrf} />
				<input type='submit' 
					className={'GetInButton ' + additionalClasses}
					value='TRY IT'/>
			</form>
		)
	}
}
class CodeExample extends Component{
	render(){
		ScriptInjector({
			minHeight:'1000px',
			src:"https://embed.runkit.com",
			'data-element-id':"CodeExampleCodeWrapper",
		});
		return (
			<div className='CodeExample'>


				<div className='sectionTitle'>
					An easy to use and developer friendly REST API
				</div>
				<div id='CodeExampleCodeWrapper' title='lalalak'>
					var request = require('request');
					{'\n'}
					await request.post({'\n'}
						{'\t'}'https://requestb.in/x7sh7lx7',{'{'}// Subfrog's API endpoint{'\n'}				    
					    {'\t'}json:{'{'}{'\n'}
					        {'\t'}{'\t'}'gateWayID': 'cus_B5bR9ryRGp0tcv', //Stripe's id of the customer that you want to edit{'\n'}
					        {'\t'}{'\t'}'APIKey': '09a8e155992f4d80ab75bb63c0b11609', //Your subfrog stripe id{'\n'}
					        {'\t'}{'\t'}'changes': [ // A list of changes that you want to make to the customer{'\n'} 
					        {'\t'}{'\t'}{'\t'}{'{'}{'\n'}
					        	{'\t'}{'\t'}{'\t'}{'\t'}"id": "call", // The id of the unit that you want to edit{'\n'}
					        	{'\t'}{'\t'}{'\t'}{'\t'}"action": "add" // The operation that you want to apply to the unit{'\n'}
					        	{'\t'}{'\t'}{'\t'}{'\t'}"quantity": 10, // The quantity that you want to change the unit by{'\n'}
					        {'\t'}{'\t'}{'\t'}{'}'}{'\n'}
					        {'\t'}{'\t'}]{'\n'}
					    {'\t'}{'}'}{'\n'}

					{'}'});				
				</div>
			</div>
		)
	}
}
class Example extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.plans=[];
	}
	render(){
		return (
			<div className='Example'>
				<div className='view UnitView'>
					<UnitEditor demo={true} plans={this.props.plans} unit={this.props.unit}/>
				</div>
			</div>
		)
	}
}
class ExampleList extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.list={};
		this.state.creators=this.getElementCreator();
		this.state.unit={};
		this.state.plans=[];
		this.state.name='';
		this.exampleSelectorFactory=this.exampleSelectorFactory.bind(this);
	}
	componentDidMount(){
		this.setExample('APICalls');
	}
	exampleSelectorFactory(name){
		return (function(){
			this.setExample(name);
		}).bind(this);
	}
	addExample(name,unit,plans){
		var example={};
		example['unit']=unit;
		example['plans']=plans;
		this.state.list[name]=example;
	}
	setExample(name){
		this.setAPICallExample();
		this.setChatOperatorExample();
		this.setGBExample();
		var example=this.state.list[name];
		if(typeof example == 'undefined'){return null;}
		this.setState({
			name: name,
			unit: example.unit,
			plans: example.plans,
		});
	}
	getElementCreator(){
		return {
			PricingRule(above=0,each=1,cost=0){
				return {
		            above: above,
		            each: each,
		            cost: cost,
		            key: generateID()
		        }
		    },
			WebhookConstant: function(name='',value=''){
				return {
					name: name, 
					value: value,
					key: generateID()
				}
			},
			Webhook: function(plan=null,
				trialMode=true,
				unit=undefined,
				quantity=0,
				constants=[]){
				return {
					refKey: generateID(),
					plan: (trialMode) ? '' : plan,
					unit: unit,
					quantity: quantity,
					constants: [],
					trialMode: trialMode,
		        }
			},
			Pricing: function(model='volume',trialMode=true,plan=null,rules=[]){
				return {
		            model: model,
		            trialMode: trialMode,
		            plan: plan,
		            rules: rules
		        }
			},
			Quantity: function(resetEachInterval=true){
				return {
					resetEachInterval: resetEachInterval,
					value: ''
				}
			},
			InvoiceGenerator: function(atEnd=false){
				return {
		        	atEnd: atEnd,
		        }
			},
			Unit: function(id='',name='',description='',
					invoiceGenerator={},webhooks=[],
					pricing=[],quantity={}){
		        return {
		            id: id,
		            name: name,
		            description: description,
		            webhooks: webhooks,
		            invoiceGenerator: invoiceGenerator,
		            quantity: quantity,
		            pricing: pricing,
		        }
		    },
		}
	}
	setAPICallExample(){
		var invoiceGenerator=this.state.creators.InvoiceGenerator(true);
		var webhooksList=[];
		var quantity=this.state.creators.Quantity(true);
		var pricingList=[
			this.state.creators.Pricing(
				'volume',
				true,
				undefined,
				[
					this.state.creators.PricingRule(200,1,10), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'STARTER',
				[	this.state.creators.PricingRule(10,1,50), //above=0,each=1,cost=0
					this.state.creators.PricingRule(200,1,10), //above=0,each=1,cost=0
					this.state.creators.PricingRule(800,1,5), //above=0,each=1,cost=0
					this.state.creators.PricingRule(1400,1,1), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'PROFESSIONAL',
				[	this.state.creators.PricingRule(10,1,50), //above=0,each=1,cost=0
					this.state.creators.PricingRule(200,1,10), //above=0,each=1,cost=0
					this.state.creators.PricingRule(400,1,5), //above=0,each=1,cost=0
					this.state.creators.PricingRule(1000,1,1), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'ADVANCED',
				[	
					this.state.creators.PricingRule(1,1,50), //above=0,each=1,cost=0
					this.state.creators.PricingRule(100,1,10), //above=0,each=1,cost=0
					this.state.creators.PricingRule(200,1,5), //above=0,each=1,cost=0
				]
			),
		]
		var unit=this.state.creators.Unit(
			'call',
			'call',
			'Customers are going to be charged for each call they make according to the pricing logic specified below',
			invoiceGenerator,
			webhooksList,
			pricingList,
			quantity
		);
		this.addExample('APICalls',unit,[
			{id: 'STARTER'},
			{id: 'PROFESSIONAL'},
			{id: 'ADVANCED'},
		])
	}
	setChatOperatorExample(){
		var invoiceGenerator=this.state.creators.InvoiceGenerator(true);
		var webhooksList=[];
		var quantity=this.state.creators.Quantity(false);
		var pricingList=[
			this.state.creators.Pricing(
				'volume',
				true,
				undefined,
				[
					this.state.creators.PricingRule(200,1,10), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'MonthlyPlan',
				[	this.state.creators.PricingRule(10,1,50), //above=0,each=1,cost=0
					this.state.creators.PricingRule(200,1,10), //above=0,each=1,cost=0
					this.state.creators.PricingRule(400,1,5), //above=0,each=1,cost=0
					this.state.creators.PricingRule(1000,1,1), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'YearlyPlan',
				[	
					this.state.creators.PricingRule(1,1,50), //above=0,each=1,cost=0
					this.state.creators.PricingRule(100,1,10), //above=0,each=1,cost=0
					this.state.creators.PricingRule(200,1,5), //above=0,each=1,cost=0
				]
			),
		]
		var unit=this.state.creators.Unit(
			'ChatOperator',
			'Chat operator',
			'Customers are going to be charged per chat operator for this chat widget according to the pricing logic specified below',
			invoiceGenerator,
			webhooksList,
			pricingList,
			quantity
		);
		this.addExample('ChatOperator',unit,[
			{id: 'MonthlyPlan'},
			{id: 'YearlyPlan'},
		])
	}
	setGBExample(){
		var invoiceGenerator=this.state.creators.InvoiceGenerator(true);
		var webhooksList=[
			// this.state.creators.Webhook('STARTER',false,'call',500,[
			// 	// this.state.creators.WebhookConstant() //name='',value=''
			// ]),//plan=null,trialMode=true,unit=undefined,quantity=0,constants=[]
		];
		var quantity=this.state.creators.Quantity(true);
		var pricingList=[
			this.state.creators.Pricing(
				'volume',
				true,
				undefined,
				[
					this.state.creators.PricingRule(200,100,10), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'STARTER',[
					this.state.creators.PricingRule(100,1,5), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'PROFESSIONAL',[
					this.state.creators.PricingRule(5000,1,5), //above=0,each=1,cost=0
				]
			),
			this.state.creators.Pricing(
				'volume',
				false,
				'ADVANCED',
				[	
					this.state.creators.PricingRule(25000,1,10), //above=0,each=1,cost=0
				]
			),
		]
		var unit=this.state.creators.Unit(
			'FormEntry',
			'Form entry',
			'Customers have monthly form entry allowance depending on the their plan. Once they have exceeded their monthly allowance they are going to be charged 0.05 dollars per entry ',
			invoiceGenerator,
			webhooksList,
			pricingList,
			quantity
		);
		this.addExample('GB',unit,[
			{id: 'STARTER'},
			{id: 'PROFESSIONAL'},
			{id: 'ADVANCED'},
		])
	}
	render(){
		var APISelected='';
		var ChatOperatorSelected='';
		var GBSelected='';
		if(this.state.name=='APICalls'){
			APISelected='selected';
		}else if(this.state.name=='ChatOperator'){
			ChatOperatorSelected='selected';
		}else if(this.state.name=='GB'){
			GBSelected='selected';
		}
		return (
			<div className='ExamplesList'>
				<div className='sectionTitle'>
					Examples
				</div>
				<div className='selectors' >
					<div className={'selectorButton '+APISelected}
						onClick={this.exampleSelectorFactory('APICalls')}>
						Per API call
					</div>
					<div className={'selectorButton '+ ChatOperatorSelected}
						onClick={this.exampleSelectorFactory('ChatOperator')}>
						Per chat operators
					</div>
					<div className={'selectorButton '+ GBSelected}
						onClick={this.exampleSelectorFactory('GB')}>
						Exceeding form entry allowance
					</div>
				</div>
				<div className='BreakPoint'></div>
				<Example key={generateID()}
				  unit={this.state.unit} plans={this.state.plans}/>
			</div>
		)
	}
}
class SecuredByStripe extends Component{
	render(){
		return (
			<div className='SecuredByStripe'>
				Secured by stripe
				<div className='description'>
					All your sensative data is stored in stripe and none of it touch our server
				</div>
			</div>
		)
	}
}
class Overview extends Component{
	render(){
		return (
			<div>
				<div className='ProductDescription'>
					Pricing editor for metered billing
				</div>
				<GetInButton/>
				<SecuredByStripe/>
				<FeaturesList/>
				<div className='BreakPoint'></div>
				<ExampleList/>
				<CodeExample/>
			</div>
		)
	}
}
class Pricing extends Component{
	render(){
		// <div className='row'>
			// <div className='column'>$25,000 - $59,999</div>
			// <div className='column'>$45 / month</div>
		// </div>

		return (
			<div className='Pricing'>
				<div className='pricingTable'>
					<div className='header'>
							Pay only for what you make through SubFrog
					</div>
					<div className='row darkBlack'>
						<div className='column'>
							<div className='title'>
								The monthly amount you make through SubFrog in USD
							</div>
						</div>
						<div className='column'>
							<div className='title'>
								Monthly cost
							</div>
						</div>
					</div>
					<div className='row'>
						<div className='column'>$1 - $499</div>
						<div className='column'>Free</div>
					</div> 
					<div className='row'>
						<div className='column'>$500 - $999</div>
						<div className='column'>
							$8 / month &nbsp;
							&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i> 
							&nbsp;(1 meal)
						</div>
					</div> 
					<div className='row'>
						<div className='column'>$1,000 - $9,999</div>
						<div className='column'>
							$14 / month 
							&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
						</div>
					</div> 
					<div className='row'>
						<div className='column'>$10,000 - $24,999</div>
						<div className='column'>
							$24 / month 
							&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
						</div>
					</div>
					<div className='row'>
						<div className='column'>$60,000 - $99,999</div>
						<div className='column'>
							$44 / month 
							&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
						</div>
					</div>
					<div className='row'>
						<div className='column'>$100,000 and above</div>
						<div className='column'>
							$64 / month 
							&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
							&nbsp;&nbsp;<i className="fa fa-cutlery" aria-hidden="true"></i>
						</div>
					</div>
				</div>
				<GetInButton absolute={false}/>
			</div>
		);
	}
}
class Body extends Component{
	render(){
		var content='';
		if(this.props.select=='overview'){
			content=<Overview/>;
		}else{
			content=<Pricing/>;
		}
		return (
			<div className='Body'>
				{content}
			</div>
		)


	}
}
class NavLoginButton extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.password='';
		this.state.email='';
		this.handleInputChange=this.handleInputChange.bind(this);
	}
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: [value]});
    }
	render(){
		return (
			<form method='POST' action='http://localhost:8000/api/member/login'  className='LoginForm'>
				<input placeholder='Email'
					type='text'
					value={this.state.email}
					onChange={this.handleInputChange}
					name='email'
					className='text'/>
				<input placeholder='Password'
					type='password'
					value={this.state.password}
					onChange={this.handleInputChange}
					name='password'
					className='text'/>
				<input value='LOGIN' type='submit' className='navButton' />
			</form>
		)
	}
}
class NavButton extends Component{
	constructor(props){
		super(props);
		this.select=this.select.bind(this);
	}
	select(){
		var selector=this.props.selectors[this.props.text];
		if(typeof selector=='undefined'){return null;}
		selector();
	}
	render(){
		var position=this.props.position
		if(typeof position == 'undefined'){
			position=''
		}
		var selected='';
		if(typeof this.props.selected!='undefined'){
			if(this.props.selected===true){
				selected='selected';
			}
		}
		return (
			<div>
				<div className={'navButton '+position + ' ' + selected} 
					onClick={this.select}>
					{this.props.text.toUpperCase()}
				</div>
			</div>
		)
	}
}
class NavMenu extends Component{
	render(){
		var selectedOverview=false;
		var selectedPricing=false;
		if(this.props.select=='pricing'){
			selectedPricing=true;
		}
		else{
			selectedOverview=true;
		}
		return (
			<div className='NavMenu'>
				<div className='Logo'>
					SUBFROG
				</div>
				<NavLoginButton/>
				<NavButton position='left' 
					selectors={this.props.selectors}
					selected={selectedOverview}
					text='overview' 
					address='/login' />
				<NavButton position='left' 
					selectors={this.props.selectors}
					selected={selectedPricing}
					text='pricing' 
					address='/pricing' />
				<NavButton position='left' 
					selectors={this.props.selectors}
					text='documentation' 
					address='/login' />
			</div>
		)
	}
}
class Wrapper extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.view='pricing'
		this.setOverview=this.setOverview.bind(this);
		this.setPricing=this.setPricing.bind(this);
		this.state.selectors={
			overview: this.setOverview,
			pricing: this.setPricing,
		}
	}
	setOverview(){
		this.setState({view: 'overview'});
	}
	setPricing(){
		this.setState({view: 'pricing'});
	}
    randomData(){
        var output=[];
        for(var counter=1; counter <= 3; counter++){
            output.push({x: counter, y: Math.random()*1000, label: 'aa'})
        }
        return output;
    }
	render(){
                    // <XAxis tickValues={[1,2,3,4,5,6,7,8,9,10,11,12,13,14]} />
                    // <YAxis />

		return (
			<div className='FrontPage'>
				<NavMenu select={this.state.view} selectors={this.state.selectors}/>
				<Body select={this.state.view} />
			</div>
		)
	}
}



class FrontPage extends Component{
	constructor(props){
		super(props);
	    var routes=[
	        { type: Wrapper, path: '/' },
	    ]        
	    this.state={routes: routes};
	}
	componentDidMount(){
        APPManager.setAlertMessage('lalala',[
            {text: 'okey',func: 'close', type: 'green'}
        ]);
	}
	render(){
        return(
            <div className='FrontPage'>
                <ComponentRouter routes={this.state.routes} />
            </div>
        )
	}
}
export default FrontPage;


