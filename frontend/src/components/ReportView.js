import React, { Component } from 'react';
import ComponentRouter from './ComponentRouter';
import { Link } from 'react-router-dom';
import Chart from 'chart.js';
import APPManager from './APPManager'
import { createStore } from 'redux';
import {
    XYPlot, 
    XAxis, 
    YAxis, 
    HorizontalGridLines,
    VerticalBarSeries, 
    LineSeries,
    VerticalGridLines,
    MarkSeries,
    Crosshair,
    LineMarkSeries,
} from 'react-vis';
var generateID=function(){
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 30; i++)
       text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}
const ACTIONS={
    UPDATE: 'Update',
    SELECTYEAR: 'SelectYear',
}
const ItemColors={
    0:  '#f7d794' ,
    1:  '#778beb' ,
    2:  '#e77f67' ,
    3:  '#f3a683' ,
    4:  '#cf6a87' ,
    5:  '#786fa6' ,
    6:  '#f8a5c2' ,
    7:  '#63cdda' ,
    8:  '#ea8685' ,
    9: '#596275' ,
    10: '#f19066' ,
    11: '#f5cd79' ,
    12: '#546de5' ,
    13: '#e15f41' ,
    14: '#c44569' ,
    15: '#574b90' ,
    16: '#f78fb3' ,
    17: '#3dc1d3' ,
    18: '#e66767' ,
    19: '#303952' ,
}
const preProcessData=function(action){
    var response={}
    response['units']=[];
    response['years']=[];
    var state=GraphDataStore.getState();
    var selectedYear=state.selectedYear;
    var colorSelector=0;
    var history=action.history;
    var units=history.units;
    for(var unitID in units){
        var unit={};
        unit['color']=ItemColors[colorSelector];
        unit['usage']=[];
        unit['revenue']=[];
        unit['id']=unitID;
        var years=units[unitID];
        for(var year in years){
            response['years'].push(year);
            if(year!=selectedYear){
                continue;
            }
            var months=years[year];
            for(var monthNum in months){
                var cost=months[monthNum].cost;
                var quantity=months[monthNum].quantity;
                unit['revenue'].push({
                    y: cost,
                    x: parseInt(monthNum),
                });
                unit['usage'].push({
                    y: quantity,
                    x: parseInt(monthNum),
                });
            }
        }
        colorSelector++;
        response.units.push(unit);
    }
    return response;
}
const MainReducer=(state={},action)=>{
    switch(action.type){
        case ACTIONS.UPDATE:
            var data=preProcessData(action);
            state.units=data.units;
            state.years=data.years;
            return state;
        case ACTIONS.SELECTYEAR:
            state.selectedYear=action.year;
            return state;
        default:
            return state;
    }
}
let GraphDataStore=createStore(MainReducer,{
    selectedYear: 2018,
    years: [],
    units: [],
});
const numberToMonth={
    1:'January',
    2:'February',
    3:'March',
    4:'April',
    5:'May',
    6:'June',
    7:'July',
    8:'August',
    9:'September',
    10:'October',
    11:'November',
    12:'December',
};
class MonthPlot extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.crosshair=[];
    }
    generatePlot(){
        if(this.props.data.length==0){
            return <div className='noDataFound'>
                {this.props.yTitle +" data is not available"}
            </div>
        }
        return (
            <XYPlot height={this.props.height} width={this.props.width}>
                <XAxis tickValues={[1,2,3,4,5,6,7,8,9,10,11,12]}
                    tickFormat={
                        function(index,value){
                            return numberToMonth[index];
                        }
                    }/>
                <YAxis title={this.props.yTitle}/>
                <VerticalGridLines/>
                <HorizontalGridLines/>
                {this.props.data.map((function(data){
                    return (
                        <VerticalBarSeries
                            color={data.color}
                            onValueMouseOver={(function(datapoint, event){
                                this.setState({crosshair: [{
                                    x: datapoint.x,y: datapoint.y
                                }]});
                            }).bind(this)}
                            onSeriesMouseOut={(function(datapoint, event){
                                this.setState({crosshair: []});
                            }).bind(this)}
                            data={data.data} />
                    );
                }).bind(this))}
                <Crosshair values={this.state.crosshair}
                    titleFormat={function(point){
                        point=point[0];
                        return {title: 'Month',value: numberToMonth[point.x]}
                    }}
                    itemsFormat={(function(point){
                        point=point[0];
                        var y=Math.floor(point.y * 1000) / 1000
                        return [{title: this.props.yTitle,value: y}]
                    }).bind(this)}
                />
            </XYPlot>
        )
    }
    render(){
        return (
            <div className='MonthPlot'>
                <div className='plotTitle' style={{
                    width: this.props.width-20
                }}>{this.props.yTitle}</div>
                {this.generatePlot()}
            </div>
        )
    }
}
class PlotsWrapper extends Component{
    constructor(props){
        super(props);
        this.state = {};
        this.state.usage=[];
        this.state.revenue=[];
    }
    randomData(){
        var output=[];
        for(var counter=1; counter <= 12; counter++){
            output.push({x: counter, y: Math.random()*1000})
        }
        return output;
    }
    render(){
        var height=600;
        var width=794;

        var state=GraphDataStore.getState();
        var units=state.units;

        if(typeof units=='undefined'){
            width=950;
        }
        if(units.length==0){
            width=950;
        }

        var revenue=[];
        var usage=[];
        units.map(function(unit){
            var tempRevenue=unit.revenue;
            var tempUsage=unit.usage;
            delete unit['revenue'];
            delete unit['usage'];
            console.log('revenue',tempRevenue);
            console.log('usage',tempUsage);
            if(typeof tempRevenue != 'undefined'){
                if(tempRevenue.length!=0){
                    unit.data=tempRevenue;
                    revenue.push(JSON.parse(JSON.stringify(unit)));
                }
            }
            if(typeof tempUsage != 'undefined'){
                if(tempUsage.length!=0){
                    unit.data=tempUsage;
                    usage.push(unit);
                }
            }
        });
        return (
            <div className='PlotWrapper'>
                <MonthPlot width={width}
                    height={height}
                    yTitle='Revenue'
                    data={revenue}
                />
                <MonthPlot width={width}
                    height={height}
                    yTitle='Usage'
                    data={usage}
                />
            </div>
        )
    }
}
class YearsSelector extends Component{
    constructor(props){
        super(props);
    }
    selectYear(year){
        return function(){
            var action={};
            action.type=ACTIONS.SELECTYEAR;
            action.year=year;
            GraphDataStore.dispatch(action);
        }
    }
    render(){
        var store=GraphDataStore.getState();
        var years=store.years;
        if(typeof years=='undefined'){return <div></div>;}        
        if(years.length<=1){return <div></div>;}
        return (
            <div className='YearsWrapper'>
                <div className='title'>Years</div>
                {years.map((function(year){
                    return <div onClick={this.selectYear(year)} className='year'>{year}</div>
                }).bind(this))}
            </div>
        )
    }
}
class LegendsWrapper extends Component{
    render(){
        var store=GraphDataStore.getState();
        var units=store.units;
        if(typeof units=='undefined'){return <div></div>;}
        if(units.length==0){return <div></div>;}

        return (
            <div className='LegendsWrapper'>
                {units.map(function(unit){
                    return (
                        <div className='Legend'>
                            <div className='colorBar'
                                style={{backgroundColor: unit.color}}>
                                &nbsp;
                            </div>
                            {unit.id}
                        </div>
                    )
                })}
            </div>
        )
    }
}
class OptionsMenu extends Component{
    render(){
        var store=GraphDataStore.getState();
        var units=store.units;
        if(typeof units=='undefined'){return <div></div>;}
        if(units.length==0){return <div></div>}
        return (
            <div className='GraphOptions'>
                <LegendsWrapper/>
                <YearsSelector/>
            </div>
        )
    }
}
class ReportView extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.display=false;
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    update(){
        var history=null;
        if(this.props.customer===true){
            history=this.props.history;
        }else{
            history=APPManager.Retrievers.getHistory();
        }
        if(history===null){return null;}
        var total=history.total;
        if(typeof total=='undefined'){total={}}
        var units=history.units;
        if(typeof units=='undefined'){units={}}

        var action={};
        action.type=ACTIONS.UPDATE
        action.units=units;
        action.total=total;
        action.history=history;
        GraphDataStore.dispatch(action);

        this.setState({display: true});
    }
    render(){
        return (
            <span className='ReportView'>
                <PlotsWrapper key={generateID()}/>
                <OptionsMenu key={generateID()}/>
            </span>
        )
    }
}
export default ReportView;