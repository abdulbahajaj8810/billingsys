import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import APPManager from './APPManager';
class Button extends Component{
    constructor(props){
        super(props);
    }
    render(){
        var content=(
            <span>
                <i className={this.props.symbol} aria-hidden="true" ></i>
                {this.props.name.toUpperCase()}
            </span>
        );
        if(this.props.isLink){
            return (
                <a className='Button'
                    activeClassName='Button selected'
                    href={'/'+this.props.link+'/'} >
                    {content}
                </a>
            )
        }else{
            return (
                <NavLink className='Button'
                    activeClassName='Button selected'
                    to={'/'+this.props.link+'/'} >
                    {content}
                </NavLink>
            )
        }
    }
}
class SearchButton extends Component{
    render(){
        return (
            <div className='searchButton' onClick={APPManager.toggleSearch}>
                <i className="fa fa-search" aria-hidden="true"></i>
                CUSTOMERS
            </div>
        )
    }
}
class NavMenu extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state['update']=0;
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
    }
    update(){
        this.setState({update: this.state.update+1});
    }
    reportButton(){
        if(!APPManager.Retrievers.isConnected()){return '';}
        if(APPManager.Retrievers.getHistory()===null){
            return '';
        }
        var button=<Button name='report' link='report' symbol='fa fa-file' />
        
        // var history=APPManager.Retrievers.listCustomers();
        // if(history==null){return '';}
        // if(Object.keys(history).length==0){return ''}
        return button;
    }
    customersButton(){
        var isConnected=APPManager.Retrievers.isConnected();
        if(!isConnected){return '';}
        var units=APPManager.Retrievers.listUnits();
        if(units===null){return ''}
        if(units.length==0){return ''}
        var button=<Button name='customers' link='customers' symbol="fa fa-users" />
        return button;
    }
    unitsButton(){
        var button=<Button name='units' link='units' symbol='fa fa-tag' />
        var units=APPManager.Retrievers.listUnits();
        if(units==null){return '';}
        if(units.length==0){
            button=<Button name='create unit' link='units/create' symbol='fa fa-tag' />
        }
        return button;
    }
    apiButton(){
        var units=APPManager.Retrievers.listUnits();
        if(units==null){return '';}
        if(units.length==0){
            return '';
        }

        var button=<Button name='api' link='api' symbol='fa fa-code' />
        var apiCalls=APPManager.Retrievers.getAPICalls();
        if(apiCalls==null){return '';}
        if(apiCalls.length==0){
            button=<Button name='api' link='api/apikeys' symbol='fa fa-code' />
        }
        return button;
    }
    render(){
        var naveMenu=this;
        return (
            <div className='NavMenu'>
                <div className='logo'>SubFrog</div>
                {this.unitsButton()}
                {this.apiButton()}
                {this.reportButton()}
                {this.customersButton()}
                <div className='buttomButtomsWrapper'>
                    <Button name='settings' link='settings' symbol='fa fa-cog' />
                    <Button isLink={true} 
                    name='logout' 
                    link='api/member/logout' 
                    symbol='fa fa-sign-out' />
                </div>
            </div>
        )
    }
}

export default NavMenu;
