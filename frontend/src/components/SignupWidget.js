import React, { Component } from 'react';
import APPManager from './APPManager'
import { Link } from 'react-router-dom';

class SignupWidget extends Component{
	constructor(props){
		super(props);
		this.register=this.register.bind(this);
		this.state={};
		this.state.password='';
		this.state.email='';
		this.handleInputChange=this.handleInputChange.bind(this);
		this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
	}
	update(){
		this.setState({});
	}
	register(placeholder){
		if(APPManager.detectors.isRegistered()){
			return null;
		}
		var placeholder=APPManager.Retrievers.getCompanyName();
		if(placeholder==null){return;}
		if(this.state.email=='' || this.state.password==''){
			APPManager.setAlertMessage(
				'Fill all the required fields',
				[{func: 'close', text: 'sure!', type: 'green'}]
			);
		}
		var request=APPManager.Company.register({
			placeholder: placeholder,
			email: this.state.email,
			password: this.state.password
		});
		request.then(function(){window.location.reload();})
	}
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
	render(){
		if(!APPManager.detectors.isRegistered()){
			return (
				<div className='SignupWidget'>
					<div className='field'>
						<input value={this.state.email}
							name='email'
							type='text' 
							onChange={this.handleInputChange}
							placeholder='Email address'/>
					</div>
					<div className='field'>
						<input value={this.state.password}
							name='password'
							type='password'
							onChange={this.handleInputChange} 
							placeholder='Password'/>
					</div>
					<div className='submit' onClick={this.register}>
						SAVE ACCOUNT
					</div>
				</div>
			)
		}else if(!APPManager.Retrievers.isConnected()){
			return (
				<div className='SignupWidget small'>
					<Link className='submit' to="/api/gateway/connect">
						CONNECT TO STRIPE
					</Link>
				</div>
			)
		}
		return (<div></div>)
	}
}


export default SignupWidget;
// abdulbahajaj@gmail.com