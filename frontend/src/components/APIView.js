import React, { Component } from 'react';
import ComponentRouter from './ComponentRouter';
import { Link } from 'react-router-dom';
import APPManager from './APPManager'
class CallViewer extends Component{
    constructor(props){
        super(props);
        // var fetch=this.fetch();
        this.state={};
        this.state['call']=[];
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
        this.state.loaded=false;
    }
    componentDidMount(){
        this.update();
    }
    update(){
        if(this.state.loaded===true){return null;}
        var callID=this.props.callID;
        var state={};
        state['call']=APPManager.Retrievers.getAPICall(callID);
        if(state['call']===null){state['call']={}}
        else{
            if(typeof state['call'].response=='object'){
                if(!(typeof state['call'].response=='undefined')){
                    state['call'].response=JSON.stringify(state['call'].response,null,4);
                }
            }
            if(typeof state['call'].request=='object'){
                if(!(typeof state['call'].request=='undefined')){
                    state['call'].request=JSON.stringify(state['call'].request,null,4);
                }
            }
            if(!(typeof state['call'].date=='undefined')){
                var date=state.call.date;
                state.call.time=''
                state.call.time+=JSON.stringify(date.year) + '/';
                state.call.time+=JSON.stringify(date.month) + '/';
                state.call.time+=JSON.stringify(date.day) + ' ';
                state.call.time+=JSON.stringify(date.hour) + ':';
                state.call.time+=JSON.stringify(date.minute) ;

            }
            state.loaded=true;
        }
        this.setState(state);
    }
    render(){
        return (
            <div>
                <div className='toolBar'>
                    <div className='title'>
                        API call
                    </div>
                </div>
                <div className='formwrapper'>
                    <div className='form'>

                        <div className='row'>
                            <div className='title'>Request ID</div>
                            <div className='field'>
                                <input type='text' className='text' value={this.state.call.id}/>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='title'>Endpoint</div>
                            <div className='field'>
                                <input type='text' className='text' value={this.state.call.endpoint}/>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='title'>Time</div>
                            <div className='field'>
                                <input type='text' className='text' value={this.state.call.time}/>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='title'>Request body</div>
                            <div className='field'>
                                <textarea className='textarea big' value={this.state.call.request}></textarea>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='title'>Response body</div>
                            <div className='field'>
                                <textarea className='textarea big' value={this.state.call.response}></textarea>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}
class APIKeyRetriever extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state['password']='';
        this.state.APIKey=''
        this.state.publicAPIKey=APPManager.Retrievers.getPrivateAPIKey();
        if(this.state.publicAPIKey==null){
            this.state.publicAPIKey='';
        }
        this.handleInputChange=this.handleInputChange.bind(this)
        this.submit=this.submit.bind(this);
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    update(){
        var publicAPIKey=APPManager.Retrievers.getPrivateAPIKey();
        if( publicAPIKey==null){
            publicAPIKey='';
        }
        this.setState({publicAPIKey: publicAPIKey});
    }
    submit(){
        var password=this.state.password;
        var processRequest=function(response){
            var data=response.data;
            this.setState({APIKey:data.APIKey});
        }
        processRequest=processRequest.bind(this)
        APPManager.Company.getAPIKey({password:password}).then(processRequest);
    }
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
    render(){
        return (
            <div className='formwrapper'>
                <div className='toolBar'>
                    <div className='title'>
                        Get your api key
                    </div>
                </div>
                <div className='form'>
                    <div className='row'>
                        <div className='title'>
                            public API key
                        </div>
                        <div className='field'>
                            <input type='text' className='text'
                            value={this.state.publicAPIKey}/>
                        </div>

                    </div>
                    <div className='row'>
                        <div className='title'>
                            Password
                        </div>
                        <div className='field'>
                            <input type='password' 
                            className='text'
                            name='password'
                            value={this.state.password}
                            onChange={this.handleInputChange}/>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='field'>
                            <div className='button big'
                            onClick={this.submit}>
                                GET API KEYS
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>
                            private API Key
                        </div>
                        <div className='field'>
                            <input type='text' className='text'
                            value={this.state.APIKey}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
class LogList extends Component{
    constructor(props){
        super(props);
        this.state={}
        this.state['calls']=[];
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    update(){
        var state={}
        state['calls']=APPManager.Retrievers.getAPICalls();
        if(state['calls']===null){return null;}
        this.setState(state);
    }
    loadMoreButton(){
        if(this.state.calls.length < 23){return}
        return (
            <div className='loadMore' onClick={APPManager.loadAPICalls}>
                Load more
            </div>
        )
    }
    buildList(){
        var endpointWidth='215px';
        var statusWidth='200px';
        var requestIDWidth='240px';
        var requestTimeWidth='180px';
        var table=[];
        table.push(
            <div className='Element'>
                <div className='column' style={{width: endpointWidth}}>
                    ENDPOINT
                </div>
                <div className='column' style={{width: statusWidth}}>
                    STATUS
                </div>
                <div className='column' style={{width: requestIDWidth}}>
                    ID
                </div>
                <div className='column' style={{width: requestTimeWidth}}>
                    TIME
                </div>
            </div>
        )
        table.push(this.state.calls.map(function(call){
            var statusClasses='column '
            if(call.status=='SUCCESS'){
                statusClasses+='green'
            }else{
                statusClasses+='red'
            }
            return (
                <Link to={'call/'+call.id} className='Element'>
                    <div className='column' style={{width: endpointWidth}}>
                        {call.endpoint}
                    </div>
                    <div className={statusClasses} style={{width: statusWidth}}>
                        {call.status}
                    </div>
                    <div className='column' style={{width: requestIDWidth}}>
                        {call.id}
                    </div>
                    <div className='column' style={{width: requestTimeWidth}}>
                        
                        {call.date.year}/{call.date.month}/{call.date.day} {call.date.hour}:{call.date.minute}
                    </div>
                </Link>
            )
        }));
        return table;
    }
    render(){
        return (
            <div className='list'>
                <div className='toolBar'>
                    <div className='title' >
                        API calls log
                    </div>
                    <Link to='apikeys' className='Button'>
                        <i className="symbol fa fa-key" aria-hidden="true"></i>
                        API Keys
                    </Link>
                </div>
                <div className='ElementsWrapper'>
                    {this.buildList()}
                    {this.loadMoreButton()}
                </div>
            </div>
        );
    }
}
class APIView extends Component{
    constructor(props){
        super(props)
        var routes=[
            { type: LogList, path: '/api/' },
            { type: APIKeyRetriever, path: '/api/apikeys' },
            { type: CallViewer, path: '/api/call/:callID' },
        ]
        this.state={routes: routes};
    }
    render(){
        return(
            <div>
                <ComponentRouter routes={this.state.routes} />
            </div>
        )
    }
}
export default APIView
//public: 77c327864c5b4fab867c52a86c3bcb6c
//private: 6236ddcebbff46beb388bb9f1b19561c