import { createStore } from 'redux';
import AjaxWrapper from './AjaxWrapper';
//Helpers
const Combine=(originalDict,changed)=>{
    var newDict={};
    for(var name in originalDict ){
        newDict[name]=originalDict[name];
    }
    for(var name in changed){
        newDict[name]=changed[name];
    }
    return newDict;
}
const SetIfDefined=( testThis, otherwise )=>{
    if( typeof testThis=='undefined' ){return otherwise;}
    return testThis;
}
//Action names
const ACTIONS={
    LoadCompany: 'loadCompany',
    StartLoadingMode: 'startLoadingMode',
    StopLoadingMode: 'stopLoadingMode',
    SetAlertMessage: 'setAlertMessage',
    LoadCustomers: 'loadCustomers',
    AddCustomer: 'addCustomer',
    CustomerNotFound: 'customerNotFound',
    UpdateCustomerHistory: 'updateCustomerHistory',
    LoadAPICalls: 'loadAPICalls',
    ToggleSearch: 'toggleSearch',
}
function createUniqueList(list,getID){
    var uniqueList=[];
    var exists={};
    for (var cursor=0; cursor < list.length; cursor++){
        var item=list[cursor];
        var id=getID(item);
        if(exists[id]){continue;}
        exists[id]=true;
        uniqueList.push(item);
    }
    return uniqueList;
}

function addHistoryToCustomer(state,history,gateWayID){
    var customers=state.company.customers;
    for(var cursor in customers){
        var customer=customers[cursor];
        if(customer.gateWayID==gateWayID){
            customer.history=history;
            break;
        }
    }
    return state
}
const MainReducer=(state={},action)=>{
    switch(action.type){
        case ACTIONS.ToggleSearch:
            state.properties.searchIsOn= !state.properties.searchIsOn;
            return state;
        case ACTIONS.LoadCompany:
            state.company=action.company;
            return state
        case ACTIONS.StartLoadingMode:
            state.properties.loading=true;
            return state
        case ACTIONS.StopLoadingMode:
            state.properties.loading=false;
            return state
        case ACTIONS.SetAlertMessage:
            state.properties.alertMessage.text=action.text;
            state.properties.alertMessage.buttons=action.buttons;
            return state
        case ACTIONS.LoadCustomers:
            state.properties.customerListStep=action.step;
            state.company.customers=state.company.customers.concat(action.customers)
            state.company.customers=createUniqueList(state.company.customers,function(customer){return customer.gateWayID});
            return state
        case ACTIONS.AddCustomer:
            state.company.customers.push(action.customer)
            return state
        case ACTIONS.CustomerNotFound:
            state.properties.customerNotFound.push(action.gateWayID);
            return state;
        case ACTIONS.UpdateCustomerHistory:
            state=addHistoryToCustomer(state,action.history,action.gateWayID);
            return state;
        case ACTIONS.LoadAPICalls:
            state.properties.apiCallsListStep=action.step;
            state.company.apiCalls=state.company.apiCalls.concat(action.apiCalls)
            return state;
        default:
            return state;
    }
}

//Data store
let StateStore=createStore(MainReducer,{
    company: {
        customers: [],
        apiCalls: [],
    },
    properties: {
        customerListStep: 24,
        apiCallsListStep: 23,
        loading: false,
        customerNotFound: [],
        searchIsOn: false,
        alertMessage: {
            text: '',
            buttons: [],
        }
    }
});
const requestMakerFactory=function(obj,objName){
    if(typeof objName=='undefined'){objName='';}
    const requestFactory=function(func,path){
        const getParamNames=function(func) {
            var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
            var ARGUMENT_NAMES = /([^\s,]+)/g;
            var fnStr = func.toString().replace(STRIP_COMMENTS, '');
            var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
            if(result === null){
                result = [];
            }
            return result;
        }
        const displayError=(message)=>{
            APPManager.stopLoadingMode();
            APPManager.setAlertMessage(message,[
                {text: 'okey',func: 'close', type: 'green'}
            ])
        }
        // var parameters=getParamNames(func);
        path=path.toLowerCase();
        return function(data){
            APPManager.startLoadingMode();
            var responseWrapper=AjaxWrapper(path,data);
            responseWrapper.then(function(response){
                APPManager.setCompanyInfo(response.data.company);
                APPManager.stopLoadingMode();
                // APPManager.update();
            });
            responseWrapper.catch(function(error){
                var response=error.response;
                if(typeof response=='undefined'){
                    displayError('Unknown error has occured');
                    return;
                }
                var data=response.data;
                if(typeof data=='undefined'){
                    displayError('Unknown error has occured');
                    return;
                }
                var message=data.message;
                if(typeof message=='undefined'){
                    displayError('Unknown error has occured');
                    return;
                }
                displayError(message); 
            });

            return responseWrapper;
        }
    }
    if(!(typeof obj == 'object')){return obj;}
    for(var name in obj){
        var actualObject=obj[name];
        var objType=typeof actualObject;
        if(objType=='function' && obj.RequestsWrapper===true){
            obj[name]=requestFactory(actualObject,objName+'/'+name);
        }
        else if(objType=='object'){
            obj[name]=requestMakerFactory(actualObject,name);
        }
    }
    return obj;
}
const clone=function(obj){
    return JSON.parse(JSON.stringify(obj));
}
//Functions that will execute an action
var APPManager={
    endPoint: "https://subfrog.com",
    StateStore: StateStore,
    appLock: false,
    isLoading: false,
    lock: function(){
        if(this.appLock){
            return false;
        }
        this.appLock=true;
        return true;
    },
    unlock: function(){
        this.appLock=false;
    },
    Retrievers: {
        getCompany: function(){
            var company=APPManager.getState().company;
            if(typeof company=='undefined'){return null;}                
            return company;
        },
        getSettings: function(){
            var company=this.getCompany();
            if(company===null){return null;}
            var response={};
            response['endpointURL']=company.endpointURL;
            
            var gateWay=company.gateWay;
            if(typeof gateWay=='undefined'){return null;}
            response['preChargeAmount']=gateWay.preChargeAmount;
            if(typeof response['preChargeAmount']=='undefined'){return null;}
            if(typeof response['endpointURL']=='undefined'){return null;}

            return response;
        },
        isConnected(){
            if(APPManager.isLoading){return true;}
            var company=this.getCompany();
            if(company===null){return true}
            var isConnected=company.isConnected;
            if(typeof isConnected=='undefined'){return true;}
            return isConnected;
        },
        getPrivateAPIKey: function(){
            var company=this.getCompany();
            if(company==null){return null;}
            var publicAPIKey=company.publicAPIKey;
            if(typeof publicAPIKey=='undefined'){
                return null;
            }
            return publicAPIKey;
        },
        getCompanyName: function(){
            var company=APPManager.Retrievers.getCompany();
            if(company===null){return null;}
            var name=company.name;
            if(typeof name=='undefined'){return null;}
            return name;
        },
        // getTemplate: function(){
        //     var company=this.getCompany();
        //     if(company===null){return null;}
        //     var template=company.template;
        //     if(typeof template == 'undefined'){
        //         return null;
        //     }
        //     return template;
        // },
        listUnits: function(){
            var company=this.getCompany();
            if(company===null){return null;}
            var units=company.units;
            if(typeof units=='undefined'){return null;}
            return units;
        },
        getUnit: function(unitID,customer){
            var units=this.listUnits();
            if(typeof customer != 'undefined'){
                units=customer.units;
            }
            if(units===null){return null;}
            for(var counter in units){
                var unit=units[counter];
                if(unit.id==unitID){
                    return unit;
                }
            }
            return null;
        },
        getGateWay(){
            var company=this.getCompany();
            if(company===null){return null;}
            var gateWay=company.gateWay;
            if(typeof gateWay=='undefined'){return null;}
            return gateWay;
        },
        listPlans: function(){
            var gateWay=this.getGateWay()
            if(gateWay==null){return null;}
            var plans=gateWay.plans;
            if(typeof plans=='undefined'){return null;}
            return plans
        },
        getPreChargeAmount(){
            var gateWay=this.getGateWay();
            if(gateWay==null){return null;}
            var preChargeAmount=gateWay.preChargeAmount;
            if(typeof preChargeAmount=='undefined'){
                return null;
            }
            return preChargeAmount;
        },
        listWebhooks(unit){
            var webhooks=unit.webhooks
            if(typeof webhooks=='undefined'){return null;}
            return clone(webhooks);
        },
        listCustomers(){
            var company=this.getCompany();
            if(company===null){return null;}
            var customers=company.customers;
            if(typeof customers=='undefined'){return null;}
            return customers;
        },
        getCustomer(gateWayID){
            var customers=this.listCustomers();
            if(!(customers===null)){
                var customer=customers.find(function(customer){
                    if(customer.gateWayID==gateWayID){
                        return true;
                    }
                    return false;
                });
                if(typeof customer != 'undefined'){return customer;}
            }
            customer=APPManager.addCustomer(gateWayID);
            return null;
        },
        getCustomerListStep(){
            var properties=APPManager.getState().properties;
            var step=properties.customerListStep;
            return step;
        },
        // getCustomerTemplate(customer){
        //     var template=customer.template;
        //     if(typeof template=='undefined'){
        //         customer=this.getCustomer(customer);
        //         if(customer===null){return null;}
        //         if(typeof customer.template=='undefined')
        //         {return null;}
        //         return customer.template;
        //     }
        //     return template;
        // },
        getCustomerUnits(customer){
            var units=customer.units;
            if(typeof units=='undefined'){
                customer=this.getCustomer(customer);
                if(customer===null){return null;}
                if(typeof customer.units=='undefined'){
                    return null;
                }
                return customer.units;
            }
            return units;
        },
        getCustomerUnit(customer,unitID){
            var units=this.getCustomerUnits;
            if(units===null){return null;}
            for(var cursor in units){
                var unit=units[cursor];
                if(unit.id==unitID){
                    return unit;
                }
            }
            return null
        },
        // getCustomerWebhooks(customer){
        //     var template=this.getCustomerTemplate(customer);
        //     if(template===null){return null;}
        //     var webhooks=template.webhooks;
        //     if(typeof webhooks=='undefined'){return null;}
        //     return clone(webhooks);
        // },
        getCustomerHistory(gateWayID,customer){
            if(typeof customer=='undefined'){
                var customer=this.getCustomer(gateWayID);
                if(customer==null){return null;}
            }
            if(typeof customer.history=='undefined'){
                if(!APPManager.lock()){
                    return null;
                }
                var request=APPManager.Customer.history({gateWayID:gateWayID});
                request.then(function(request){
                    var history=request.data;
                    var action={};
                    action.type=ACTIONS.UpdateCustomerHistory;
                    action.history=history;
                    action.gateWayID=customer.gateWayID;
                    APPManager.unlock();
                    APPManager.StateStore.dispatch(action);
                });
                request.catch(function(){
                    APPManager.unlock();
                });
                return null;
            }
            if(customer===null){return null;}
            var history=customer.history;
            if(typeof history=='undefined'){return null;}
            return history;
        },
        getCustomerNotFound(){
            var properties=APPManager.getState().properties;
            return properties.customerNotFound;
        },
        getHistory(){
            var company=this.getCompany()
            if(company===null){return null;}
            var history=company.history;
            if(typeof history=='undefined'){return null;}
            return history;
        },
        getAPICalls(){
            var company=this.getCompany();
            if(company===null){return null;}
            var apiCalls=company.apiCalls;
            if(typeof apiCalls=='undefined'){return null;}
            return apiCalls
        },
        getAPICallsListStep(){
            var properties=APPManager.getState().properties;
            var step=properties.apiCallsListStep;
            return step;
        },
        getAPICall(callID){
            var apiCalls=this.getAPICalls();
            if(apiCalls===null){return null};
            var foundTargetException={};
            var call=null
            try{
                apiCalls.forEach(function(element, index) {
                    if(element.id==callID){
                        call=element;
                        throw foundTargetException;
                    }
                });
            }catch(foundTargetException){}
            return call;
        },
        getProperties(){
            var state=APPManager.getState();
            if(typeof state != 'object'){return null;}
            var properties=state.properties;
            if(typeof properties=='undefined'){return null;}
            return properties;
        },
        getAlertMessage(){
            var properties=this.getProperties();
            if(properties==null){return null;}
            var alertMessage=properties.alertMessage;
            if(typeof alertMessage=='undefined'){return null;}
            return alertMessage;
        }
    },
    detectors:{
        isSearchOn(){
            var properties=APPManager.getState().properties;
            return properties.searchIsOn;
        },
        isRegistered(){
            var company=APPManager.Retrievers.getCompany();
            if(company===null){return true;}
            var registered=company.registered;
            if(typeof registered=='undefined'){return true;}
            return registered;
        },
        isMessage(){
            var alertMessage=APPManager.Retrievers.getAlertMessage();
            if(alertMessage===null){return false;}
            if(alertMessage.text==''){
                return false;
            }else{
                return true;
            }
        },
        isLoading(){
            var properties=APPManager.Retrievers.getProperties();
            if(properties===null){return false;}
            var loading=properties.loading;
            if(typeof loading=='undefined'){return false;}
            return loading;
        }
    },
    getState: function(){
        return this.StateStore.getState();
    },
    toggleSearch(){
        var action={};
        action.type=ACTIONS.ToggleSearch;
        APPManager.StateStore.dispatch(action);
    },
    startLoadingMode: function(){
        this.StateStore.dispatch({
            type: ACTIONS.StartLoadingMode,
        });
    },
    stopLoadingMode: function(){
        this.StateStore.dispatch({
            type: ACTIONS.StopLoadingMode,
        });
    },
    setAlertMessage: function(text='',buttons=[]){
        this.StateStore.dispatch({
            type: ACTIONS.SetAlertMessage,
            text: text,
            buttons: buttons,
        });
    },
    setCompanyInfo(info){
        if(typeof info == 'undefined'){return;}
        var action={};
        action.type=ACTIONS.LoadCompany;
        action.company=info;
        this.StateStore.dispatch(action);
    },
    loadCustomers(){
        var step=APPManager.Retrievers.getCustomerListStep();
        APPManager.Customer.listCustomers({listStep:step}).then(function(response){
            var data=response.data;
            var action={};
            action.type=ACTIONS.LoadCustomers;
            action.customers=data.customers;
            action.step=data.currentStep;
            APPManager.StateStore.dispatch(action);
        });
    },
    loadAPICalls(){
        var step=APPManager.Retrievers.getAPICallsListStep();
        APPManager.Company.getAPICalls({listStep:step}).then(function(response){
            var data=response.data;
            var action={};
            action.type=ACTIONS.LoadAPICalls;
            action.apiCalls=data.apiCalls;
            action.step=data.currentStep;
            APPManager.StateStore.dispatch(action);
        });
    },
    isInNotFoundList(gateWayID){
        var notFoundList=APPManager.Retrievers.getCustomerNotFound();
        for(var cursor=0;cursor<notFoundList.length;cursor++){
            if(notFoundList[cursor]==gateWayID){
                return true;
            }
        }
        return false;
    },
    addCustomer:function(gateWayID){
        if(!APPManager.lock()){
            if(this.isInNotFoundList(gateWayID)){
                return false;
            }
            return null;
        }
    
        if(this.isInNotFoundList(gateWayID)){
            return false;

        }
        var request=APPManager.Customer.get({gateWayID:gateWayID});
        request.then(function(response){
            var data=response.data;
            var action={};
            action.type=ACTIONS.AddCustomer;
            action.customer=data;
            APPManager.StateStore.dispatch(action);
            APPManager.unlock();
        });
        request.catch(function(error){
            APPManager.unlock();
            var action={};
            action.type=ACTIONS.CustomerNotFound;
            action.gateWayID=gateWayID;
            APPManager.StateStore.dispatch(action);
            var notFoundList=APPManager.Retrievers.getCustomerNotFound();
        });
        return true;
    },
    update: function(){
        if(window.location.pathname=='/'){
            return;
        }
        var request=APPManager.Company.get();
        request.then((function(response){
            this.isLoaded=true;
            this.companyUpdateRequest=undefined;
         }).bind(this));
        request.catch(function(error){       
            var pathname='/';
            if(window.location.pathname!=pathname){
                window.location='/';
            }
            this.companyUpdateRequest=undefined;
        });
        this.companyUpdateRequest=request;
        return request;
    },
    createEmptyCompany: function(){
        this.Company.registerEmptyCompany().then(function(){
            window.location='/units/create/';
        })
    },
    Company: {
        RequestsWrapper: true,
        get: function(){},
        getAPIKey: function(password){},
        getGateWayCredentialLink: function(){},
        edit: function(name,currency,endpointURL){},
        resetAPI: function(password){},
        register: function(placeholder,email,password){},
        logout: function(){},
        registerEmptyCompany: function(){},
        getAPICalls: function(listStep){},
        updateSettings: function(settings){}
    },
    Unit: {
        RequestsWrapper: true,
        create: function(unitID,unitName,unitDescription,pricing){},
        remove: function(unitID){},
        edit: function(unitID,unitName,unitDescription,atEnd,resetEachInterval,pricing,updateCustomersWithCustomPricing,grandFatherCustomers){},
        generateMockWebhook: function(){},
    },
    Webhook: {
        RequestsWrapper: true,
        edit: function(webhooks){},
    },
    Member: {
        RequestsWrapper: true,
        add: function(email,password,code){},
        login: function(companyName,email,password){},
        edit: function(email){},
        editPassword: function(currentPassword,newPassword){},
        getInvitationLink: function(permission,password){},
        changePermission: function(memberID,permission){},
        remove: function(memberID){},
    },
    Customer: {
        RequestsWrapper: true,
        appyUsage: function(subscriptionID){},
        update: function(gateWayID){},
        // bulkUpdate: function(templateID,password){},
        setCustomPricing: function(unitID,unitName,unitDescription,atEnd,resetEachInterval,pricing){},
        setCustomWebhooks: function(webhooks){},
        get: function(gateWayID){},
        listCustomers: function(listStep){},
        history: function(gateWayID){},
    },
    Quantity: {
        RequestsWrapper: true,
        add: function(gateWayID,unitID,quantity){},
        set: function(gateWayID,unitID,quantity){}
    },
    GateWay:{
        RequestsWrapper: true,
        edit: function(preChargeAmount){},
    },
    companyUpdateRequest: undefined,
}
APPManager=requestMakerFactory(APPManager);
APPManager.update();
export default APPManager;








