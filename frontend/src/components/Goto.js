import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import APPManager from './APPManager'

class Goto extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.search=''
		this.state.visible=false;
		this.handleInputChange=this.handleInputChange.bind(this);
		this.update=this.update.bind(this);
		APPManager.StateStore.subscribe(this.update);
	}
	update(){
		if(APPManager.detectors.isSearchOn()){
			this.setState({visible: true});
		}else{
			this.setState({visible: false})
		}
	}
	getLink(){
		return "/customers/view/"+this.state.search;
	}
	getGoButton(){
		if(this.state.search==''){return '';}
		return (
			<Link className='SearchButton' to={this.getLink()}>
        		<i className="fa fa-search" aria-hidden="true"></i>
			</Link>
		)
	}
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({search: value});
    }
	componentDidMount(){
	    this.searchBox.focus();
	}
	componentDidUpdate(){
	    this.searchBox.focus();
	}
	render(){
		return (
	        <div  className={'AlertWrapper '+(this.state.visible?'visible':'')}>
	        	<div className="AlertShadow" onClick={APPManager.toggleSearch}>&nbsp;</div>
				<div className='LookUp' >
					<form method="GET" action={this.getLink()}>
						<input autofocus type='text' 
						ref={(input) => { this.searchBox = input; }} 
						placeholder="Enter a gateway ID" className='searchBox'
						onChange={this.handleInputChange}
						/>
						{this.getGoButton()}
					</form>
				</div>
        	</div>
		)
	}
}
export default Goto;