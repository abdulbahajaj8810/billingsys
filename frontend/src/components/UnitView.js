import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import { Link } from 'react-router-dom'
import {
    ToolBar,
    Button as ToolBarButton
} from './ToolBar'
import AjaxWrapper from './AjaxWrapper'
import ComponentRouter from './ComponentRouter'
import APPManager from './APPManager'
import { createStore } from 'redux';
const ACTIONS={
    SETUNIT: 'SetUnit',
    UPDATEPRICING: 'UpdataPricing',
    UPDATEWEBHOOKS: 'UpdateWebhooks',
}
const MainReducer=(state={},action)=>{
    switch(action.type){
        case ACTIONS.SETUNIT:
            state.unit=action.unit;
            return state;
        case ACTIONS.UPDATEPRICING:
            state.unit.pricing=action.pricing;
            return state;
        case ACTIONS.UPDATEWEBHOOKS:
            state.unit.webhooks=action.webhooks;
            console.log('MainReducer.UPDATEWEBHOOKS:state: ',state.unit.webhooks);
            return state;
        default:
            return state;
    }
}
let UnitEditorStateStore=createStore(MainReducer,{
    unit: {},
});
// var Events={
//     list: {},
//     subscribe: function(name,func){
//         if(typeof this.list[name]=='undefined'){
//             this.list[name]=[];
//         }
//         this.list[name].push(func);
//     },
//     dispatch(name){
//         // for(var cur in this.list[name]){
//         //     this.list[name][cur]();
//         // }
//     },
// }
const Retriever={
    state: function(){
        return UnitEditorStateStore.getState();
    },
    unit: function(){
        var state=this.state();
        var unit=state.unit;
        if(typeof unit=='undefined'){return null;}
        return unit;
    },
    webhooks: function(){
        var unit=this.unit();
        if(unit==null){return null;}
        var webhooks=unit.webhooks;
        if(typeof webhooks=='undefined'){return null}
    },
    pricing: function(){
        var unit=this.unit();
        if(unit==null){return null;}
        var pricing=unit.pricing;
        if(typeof pricing=='undefined'){return null}
    },
}
var generateID=function(){
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 30; i++)
       text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}
class WebhookConstant extends Component{
    constructor(props){
        super(props);
        this.state={constant: this.props.constant}
        this.handleInputChange=this.handleInputChange.bind(this);
        this.delete=this.delete.bind(this);
    }
    emptyConstant(){
        return {name:'',value:''}
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        var constant=this.state.constant;
        constant[name]=value;
        this.setState({constant:constant});
        if(name=='name'){
            this.props.updator();
        }
        // Events.dispatch('WebhooksLogicEditor.updateStateStore');
    }
    delete(){
        this.props.deleter(this.props.refKey);
    }
    render(){
        return(
            <div className='field long'>
                <input type='text'
                    className='text'
                    placeholder='constant name'
                    name='name'
                    value={this.state.constant.name}
                    onChange={this.handleInputChange}
                    />
                &nbsp;=&nbsp;
                <input type='text'
                    className='text'
                    placeholder='constant value'
                    name='value'
                    value={this.state.constant.value}
                    onChange={this.handleInputChange}
                    />
                &nbsp;
                    <i className="deleteSymbol fa fa-trash"
                        aria-hidden="true"
                        onClick={this.delete}></i>
            </div>
        )
    }
}
class WebhookRule extends Component{
    constructor(props){
        super(props);
        this.state={
            webhook: this.props.webhook,
            keys: {}
        }
        this.delete=this.delete.bind(this);
        this.addConstant=this.addConstant.bind(this);
        this.update=this.update.bind(this);
        this.deleteConstant=this.deleteConstant.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
    }
    delete(){
        this.props.deleter(this.props.webhook.refKey);
    }
    addConstant(){
        var webhook=this.state.webhook;
        webhook.constants.push({name:'', value: '',key: generateID()})
        this.setState({webhook:webhook})
    }
    update(){
        this.setState({});
    }
    deleteConstant(refKey){
        var constants=this.state.webhook.constants;
        for(var counter in constants){
            var constant=constants[counter];
            if(constant.key==refKey){
                constants.splice(counter,1);
                this.setState({});
                return;
            }
        }
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(name=='quantity'){
            value=parseInt(value);
            if(isNaN(value)){
                value=0;
            }
        }
        var webhook=this.state.webhook;
        webhook[name]=value;
        this.setState({webhook:webhook});
        // Events.dispatch('WebhooksLogicEditor.updateStateStore');
    }
    getConstants(){
        var output = [];
        for (var counter in this.state.webhook.constants){
            var constant = this.state.webhook.constants[counter];
            output.push(
                <WebhookConstant
                    key={constant.key}
                    constant={constant}
                    updator={this.update}
                    refKey={constant.key}
                    deleter={this.deleteConstant} />
            );
        }
        return output;
    }
    render(){
        if(typeof this.state.webhook.trialMode == 'undefined'){this.state.webhook.trialMode = false; }
        if(this.props.trialMode != this.state.webhook.trialMode){return <div></div>;}
        if(!this.props.trialMode && (this.props.selectedPlan != this.state.webhook.plan)){return <div></div>;}
        return (
            <div className='row'>
                <div className='title'>
                    <i className="deleteSymbol fa fa-trash"
                        aria-hidden="true"
                        onClick={this.delete}>
                    </i>
                </div>
                <div className='field long'>
                    <b>send a webhook when quantity reaches &nbsp;</b>
                    <input name='quantity' value={this.state.webhook.quantity} onChange={this.handleInputChange} type='text' className='text'/>
                </div>
                <div className='title'>
                    constants
                </div>
                {this.getConstants()}
                <div className='field long'>
                    <div className='button' onClick={this.addConstant}>Add constant</div>
                </div>
            </div>
        )
    }
}
class WebhooksLogicEditor extends Component{
    constructor(props){
        super(props);
        this.state={
            webhooks: [],
            selectedPlan: '',
            trialMode: true,
            selectedTrialModeVal: generateID()
        }
        this.handleInputChange=this.handleInputChange.bind(this);
        this.addWebhook=this.addWebhook.bind(this);
        this.deleteWebhook=this.deleteWebhook.bind(this);
        this.update=this.update.bind(this);
        this.trialModeToggle=this.trialModeToggle.bind(this);
        this.updateStateStore=this.updateStateStore.bind(this);
        // Events.subscribe('WebhooksLogicEditor.updateStateStore',this.updateStateStore);
        UnitEditorStateStore.subscribe(this.update);
    }
    generateWebhookID(){
       var text = "";
       var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       for (var i = 0; i < 30; i++)
           text += possible.charAt(Math.floor(Math.random() * possible.length));
       return text;
    }
    componentDidMount(){
        this.update();
    }
    update(){
        var state={};

        state['unit']=Retriever.unit();
        if(state.unit===null){state.unit={};}
        state['webhooks']=state.unit.webhooks;
        if(state.webhooks===null){state.webhooks=[];}

        for(var counter in state.webhooks){
            var webhook=state.webhooks[counter];
            webhook.refKey=generateID();
        }
        this.setState(state);
    }
    updateStateStore(){
        var action={};
        action.type=ACTIONS.UPDATEWEBHOOKS;
        action.webhooks=this.state.webhooks;
        UnitEditorStateStore.dispatch(action);
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(value==this.state.selectedTrialModeVal){
            this.setState({
                trialMode: true,
                selectedPlan: value,
            });
        }else{
            this.setState({selectedPlan: value,trialMode: false});
        }
    }
    emptyWebhook(){
        return {
            refKey: this.generateWebhookID(),
            plan: (this.state.trialMode) ? '' : this.state.selectedPlan,
            unit: undefined,
            quantity: 0,
            constants: [],
            trialMode: this.state.trialMode,
        }
    }
    addWebhook(){
        console.log("WebhooksLogicEditor: addWebhook: ",this.state.webhooks);
        var webhooks=this.state.webhooks;
        webhooks.push(this.emptyWebhook());
        this.setState({webhooks:webhooks});
        this.updateStateStore();
    }
    deleteWebhook(refKey){
        var webhooks=this.state.webhooks
        for(var counter in webhooks){
            var webhook=webhooks[counter];
            if(webhook.refKey==refKey){
                webhooks.splice(counter,1);
                this.setState({webhooks:webhooks});
                return;
            }
        }
        this.updateStateStore();
    }
    setWebhooksList(){
        return this.state.webhooks.map((function(webhook){
            return (
                <WebhookRule
                    webhook={webhook}
                    key={webhook.refKey}
                    keyRef={webhook.refKey}
                    deleter={this.deleteWebhook}
                    trialMode={this.state.trialMode}
                    selectedPlan={this.state.selectedPlan}
                    units={this.state.units}/>
            )
        }).bind(this));
    }
    trialModeToggle(){
        this.setState({trialMode: !this.state.trialMode});
    }
    getAddWebhookButton(){
        var button=(
            <div className='row'>
                <div className='field'>
                    <div onClick={this.addWebhook}
                        className='button big'>
                        Add webhook
                    </div>
                </div>
            </div>
        )
        if(this.state.trialMode) {
            return button;
        }
        for(var counter in this.props.plans) {
            var plan = this.props.plans[counter];
            if (this.props.selectedPlan == plan.id) {
                return button;
            }
        }
    }
    getOptionBar(){
        return (
            <span>
                <div className='title'>
                    If customer is subscribed to
                </div>
                <div className='field'>
                    <select value={this.state.selectedPlan} onChange={this.handleInputChange}>
                        <option value={this.state.selectedTrialModeVal}>On trial</option>
                        {this.props.plans.map(function(plan){
                            return (
                                <option value={plan.id}>
                                    {plan.id}
                                </option>
                            )
                        })};
                    </select>
                </div>
            </span>
        );
    }
    render(){
        return (
            <div>
                {(function(){
                    if(this.props.custom){
                        return (
                            <div className='row'>
                                <div className='formTitle'>
                                    GATEWAY-ID: {this.props.gateWayID}
                                </div>
                            </div>
                        )
                    }
                }).bind(this)()}
                <div className='row'>
                    {this.getOptionBar()}
                </div>
                {this.setWebhooksList()}
                {this.getAddWebhookButton()}
            </div>

        )
    }
}
//Unit editor
class Testor extends Component{
    constructor(props){
        super(props);
        this.state={quantity: 0};
        this.handleInputChange=this.handleInputChange.bind(this);
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(name=='quantity'){
            value=parseInt(value);
            if(isNaN(value)){value=0;}
        }
        this.setState({quantity: value});
    }
    getRule(below){
        var lowest=false;
        for( var counter in this.props.pricing.rules){
            var rule=this.props.pricing.rules[counter]
            if(below >= rule.above){
                if(lowest==false)
                    lowest=rule
                else if(rule.above > lowest.above)
                    lowest=rule
            }
        }
        return lowest;
    }
    volumeCalculator(){
        if(this.state.quantity==0){return 0;}
        if(typeof this.state.quantity=='undefined'){return 0;}
        var rule=this.getRule(this.state.quantity);
        if(rule){
            var cost=rule.cost;
        }
        else{
            return 0;
        }
        if(rule.each==0){return 0;}
        return parseFloat(parseFloat(cost)*(parseFloat(this.state.quantity)/parseFloat(rule.each)))
    }
    stairstepCalculator(){
        if(this.state.quantity==0) return 0
        var cost=0
        var quantity=this.state.quantity
        while(true){
            var rule=this.getRule(quantity)
            if(!rule){
                break;
            }
            cost+=parseFloat((quantity-rule.above+1))*parseInt(rule.cost)
            quantity=rule.above-1
        }
        return parseFloat(cost)
    }
    packageCalculator(){
        var rule=this.getRule(this.state.quantity)
        if(!rule) return 0
        return parseFloat(rule.cost)
    }
    calculator(){
        var cost=0.0;
        if(this.props.pricing.model=='volume')
            cost=this.volumeCalculator()
        if(this.props.pricing.model=='stairstep')
            cost=this.stairstepCalculator()
        if(this.props.pricing.model=='package')
            cost=this.packageCalculator()
        cost=parseFloat(cost).toFixed(2);
        cost=cost/100.0;
        return cost
    }
    render(){
        return(
            <div className='row'>
                <div className='title'>Test</div>
                <div className='field'>
                    <input
                        type='text'
                        className='testinput'
                        value={this.state.quantity}
                        name='quantity'
                        onChange={this.handleInputChange}
                        placeholder='Quantity'/>
                    <div className='rowtext'> {this.props.unit.id}(s) will cost <b>{this.calculator()}</b> dollars</div>
                </div>
            </div>
        )
    }
}
class Rule extends Component{
    constructor(props){
        super(props);
        this.state={
            rule: props.rule,
        }
        this.handleInputChange=this.handleInputChange.bind(this)
        this.handleInputBlur=this.handleInputBlur.bind(this)
        this.delete=this.delete.bind(this)
    }
    delete(){
        this.props.deleter(this.props.refKey);
    }
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(value=='false'){value=false;}
        if(value=='true'){value=true;}
        value=parseInt(value)
        if(isNaN( value )){value=0;}
        var rule=this.state.rule;
        rule[name]=value;
        this.setState({rule: rule});
    }
    handleInputBlur(event){
        var rule=this.state.rule
        const target = event.target;
        const name = target.name;
        if(name=='above'){
            this.props.sorter();
        }
    }
    render(){
        return(
            <div className='bracket'>
                if <b>{this.props.unit.id}</b> quantity reaches &nbsp;
                <input
                    type='text'
                    className='brackettext'
                    name='above'
                    value={this.state.rule.above}
                    onChange={this.handleInputChange}
                    onBlur={this.handleInputBlur}
                    placeholder='Quantity'
                    />
                charge&nbsp;
                <input
                    type='text'
                    className='brackettext'
                    name='cost'
                    value={this.state.rule.cost}
                    onChange={this.handleInputChange}
                    onBlur={this.handleInputBlur}
                    placeholder='cost in cents'
                    />
                {
                ((function(){
                    if(this.props.model!='volume'){return '';}
                    return (
                        <span>
                            for each&nbsp;
                            <input
                                type='text'
                                className='brackettext'
                                name='each'
                                value={this.state.rule.each}
                                onChange={this.handleInputChange}
                                onBlur={this.handleInputBlur}
                                placeholder='Quantity'
                                />
                           <b>{this.props.unit.id}</b>
                        </span>
                    )
                    }).bind(this))()
                }
                <i
                    className="deleteButton fa fa-trash"
                    aria-hidden="true"
                    onClick={this.delete}
                    >
                </i>
        </div>
        )
    }
}
class PricingLogicEditor extends Component{
    constructor(props){
        super(props);
        this.state={
            pricing: undefined,
            selectedPlan: undefined,
            selectedPricing: [],
            update: 0,
            counter: 0,
            onTrialSelecter: generateID(),
        };
        this.state.selectedPlan=this.state.onTrialSelecter;
        this.handleInputChange=this.handleInputChange.bind(this);
        this.changePlan=this.changePlan.bind(this);
        this.addRule=this.addRule.bind(this);
        this.deleteRule=this.deleteRule.bind(this);
        this.sortRules=this.sortRules.bind(this);
        this.update=this.update.bind(this);
        UnitEditorStateStore.subscribe(this.update);
        
    }
    componentDidMount(){
        this.update();
    }
    update(){
        var unit=Retriever.unit();
        for(var counter in unit.pricing){
            var pricing=unit.pricing[counter]
            var rules=pricing.rules;
            for(var counter2 in rules ){
                var rule=rules[counter2]
                rule.key=this.generateRuleID();
            }
        }
        this.setState({
            unit: unit,
            pricing: unit.pricing,
        });
    }
    deleteRule(key){
        var selectedPricing=this.state.selectedPricing
        for(var counter in selectedPricing.rules){
            var rule=selectedPricing.rules[counter];
            if(rule.key==key){
                selectedPricing.rules.splice(counter,1);
                this.setState({selectedPricing:selectedPricing})
                return;
            }
        }
    }
    generateRuleID(){
       var text = "";
       var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       for (var i = 0; i < 30; i++)
           text += possible.charAt(Math.floor(Math.random() * possible.length));
       return text;
    }
    emptyPricing(planID){
        return {
            model: 'volume',
            trialMode: false,
            plan: planID,
            rules: []
        }
    }
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(value=='false'){value=false;}
        if(value=='true'){value=true;}
        var selectedPricing=this.state.selectedPricing;
        selectedPricing[name]=value;
        this.setState({selectedPricing: selectedPricing});
    }
    changePlan(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({selectedPlan: value});
        // if(this.state.counter==0){
        //     this.props.updatePricing(this.state.pricing);
        //     this.state.counter+=1;
        // }
    }
    addRule(){
        var selectedPricing=this.state.selectedPricing;
        selectedPricing.rules.push({
            above: 0,
            each: 1,
            cost: 0,
            key: this.generateRuleID()
        });
        this.setState({selectedPricing: selectedPricing});
    }
    sortRules(){
        if(typeof this.state.selectedPricing=='undefined'){ return;}
        var sorted=this.state.selectedPricing.rules.sort(function(rule1,rule2){
            return (parseInt( rule1.above )-parseInt( rule2.above ));
        });
        var selectedPricing=this.state.selectedPricing;
        selectedPricing.rules=sorted;
        this.setState({selectedPricing:selectedPricing});
    }
    setPricingEditor(){
        var found=false;
        if(typeof this.state.pricing=='undefined'){return '';}
        if(typeof this.state.selectedPlan == 'undefined'){return '';}
        for( var counter in this.state.pricing ){
            var pricing=this.state.pricing[counter];
            if(pricing.plan==this.state.selectedPlan){
                this.state.selectedPricing=pricing;
                found=true
                break;
            }else if(pricing.trialMode && this.state.onTrialSelecter==this.state.selectedPlan){
                this.state.selectedPricing=pricing;
                found=true
                break;
            }
        }
        if(!found){
            this.state.selectedPricing=this.emptyPricing(this.state.selectedPlan);
            this.state.pricing.push(this.state.selectedPricing);
        }
        if(typeof this.state.selectedPricing == 'undefined'){return '';}
        return(
            <div>
                <div className='row'>
                    <div className='title'>Pricing model</div>
                    <div className='field'>
                        <select
                        name='model'
                        onChange={this.handleInputChange}
                        value={this.state.selectedPricing.model}>
                            <option >Select a pricing model</option>
                            <option value="volume">Volume</option>
                            <option value="stairstep">Stairstep</option>
                            <option value="package">Package</option>
                        </select>
                    </div>
                </div>
                {((function(){
                    if(['volume','stairstep','package'].indexOf(this.state.selectedPricing.model)==-1 ){
                        return '';
                    }
                    return (
                        <span>
                            <div className='row brackets'>
                                <div className='title'>Rules</div>
                                <div className='field'>
                                    {
                                        this.state.selectedPricing.rules.map((function(rule){
                                            return (
                                                <Rule
                                                    unit={this.state.unit}
                                                    key={rule.key}
                                                    refKey={rule.key}
                                                    sorter={this.sortRules}
                                                    deleter={this.deleteRule}
                                                    model={this.state.selectedPricing.model}
                                                    rule={rule}
                                                />
                                            )
                                        }).bind(this))
                                    }
                                    <div className='addbracketbutton' onClick={this.addRule}>
                                        ADD RULE
                                    </div>
                                </div>
                            </div>
                            <Testor unit={this.state.unit} pricing={this.state.selectedPricing}/>
                        </span>
                    )
                }).bind(this))()}
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className='row'>
                    <div className='title'>If customer is subscribed to</div>
                    <div className='field'>
                        <select
                        value={this.state.selectedPlan}
                        name='selectedPlan'
                        onChange={this.changePlan}>
                            <option key={this.state.onTrialSelecter} 
                                value={this.state.onTrialSelecter}>
                                On trial
                            </option>
                            {this.props.plans.map(function(plan){
                                return <option key={plan.id} value={plan.id}>{plan.id}</option>
                            })}
                        </select>
                    </div>
                </div>
                {this.setPricingEditor()}
            </div>
        )
    }
}
class UnitEditor extends Component{
    constructor(props){
        super(props);
        //Binding event handles
        this.state={};
        this.state.unit=this.emptyUnit()
        this.state.plans=[];
        this.state.grandfatheringPolicy='grandfatherAll';
        this.state.logicEditor='pricing'

        this.submit=this.submit.bind(this);
        this.update=this.update.bind(this);
        this.launchDeleteAlert=this.launchDeleteAlert.bind(this);
        this.deleteUnit=this.deleteUnit.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        APPManager.StateStore.subscribe(this.update);
    }
    componentDidMount(){
        this.update();
    }
    emptyUnit(){
        return {
            id: '',
            name: '',
            description: '',
            webhooks: [],
            invoiceGenerator: {
                atEnd: false,
            },
            quantity: {
                resetEachInterval: true,
                value: ''
            },
            pricing: [
                {
                    model: 'volume',
                    trialMode: true,
                    plan: null,
                    rules: []
                }
            ]
        }
    }
    updateStateStore(unit){
        var action={};
        action.type=ACTIONS.SETUNIT;
        action.unit=unit;
        UnitEditorStateStore.dispatch(action);
    }
    update(){ 
        if(this.props.demo===true){
            this.setState({
                plans: this.props.plans,
                unit: this.props.unit,
            })
            this.updateStateStore(this.props.unit);
            return;
        }
        //loading plans
        var plans=APPManager.Retrievers.listPlans();

        if(plans===null){plans=[];}
        this.setState({plans: plans});
        if(!(this.props.create===false)){return;}
        
        //loading unit info
        var unitID=this.props.unitID;
        if(typeof unitID=='undefined' || unitID===null){return;}

        //loading customer info
        var customer;
        if(this.props.custom){
            var gateWayID=this.props.gateWayID;
            if(typeof gateWayID=='undefined'){return;}
            customer=APPManager.Retrievers.getCustomer(gateWayID);
            if(customer==null){return;}
        }

        //Loading unit
        var unit=APPManager.Retrievers.getUnit(unitID,customer);
        if(unit===null){
            unit=this.emptyUnit();
        }
        this.setState({unit: unit});
        this.updateStateStore(unit);
    }
    handleInputChange(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        var unit=this.state.unit;
        if(value=='false'){value=false;}
        if(value=='true'){value=true;}
        if(name=='grandfatheringPolicy'){
            this.setState({grandfatheringPolicy: value});
            return;
        }
        if(name=='resetEachInterval'){
            this.state.unit.quantity.resetEachInterval=value;
        }else if(name=='atEnd'){
            this.state.unit.invoiceGenerator.atEnd=value;
        }
        else{
            unit[name]=value;
        }
        this.setState({unit: unit});
    }
    gatherInputs(){
        var unit=Retriever.unit();
        console.log(unit);
        var inputs={
            unitID: this.state.unit.id,
            unitName: this.state.unit.name,
            webhooks: unit.webhooks,
            unitDescription: this.state.unit.description,
            pricing: unit.pricing,
            resetEachInterval: this.state.unit.quantity.resetEachInterval,
            atEnd: this.state.unit.invoiceGenerator.atEnd,
        }
        if(!this.props.create && !this.props.custom){
            inputs['grandfatheringPolicy']=this.state.grandfatheringPolicy;
        }
        inputs=JSON.parse(JSON.stringify(inputs))
        return inputs
    }
    validityCheck(info){
        var valid=true;
        valid &= !(typeof info.unitID=='undefined');
        valid &= !(info.unitID=='');
        if(!valid){
            return "Enter a valid unit ID";
        }
        valid &= !(typeof info.unitName=='undefined');
        valid &= !(info.unitName=='');
        if(!valid){
            return "Enter a valid unit name";
        }
        return false;
    }
    submit(){
        var parameters=this.gatherInputs();
        var errorMessage=this.validityCheck(parameters);
        if(errorMessage){
            APPManager.setAlertMessage(errorMessage,[
                {text: 'okey',func: 'close', type: 'green'}
            ]);
            return;
        }
        var sender=APPManager.Unit.edit;
        if(this.props.create===true){
            sender=APPManager.Unit.create;
        }
        else if(this.props.custom){
            parameters['gateWayID']=this.props.gateWayID;
        }
        sender(parameters);
    }
    submissionButton(){
        if(this.props.demo===true){return '';}
        var buttonValue="UPDATE";
        if(this.props.create){
            buttonValue="CREATE";
        }else if(this.props.custom){
            buttonValue="CUSTOMIZE"
        }

        return <div className='submit' onClick={this.submit}>{buttonValue}</div>;
    }
    deleteButton(){
        if(this.props.demo===true){return '';}
        var button='';
        if(!this.props.create){
            button=<div className='submit red' onClick={this.launchDeleteAlert}>DELETE</div>;
        }
        return button;
    }
    launchDeleteAlert(){
        APPManager.setAlertMessage('Are you sure that you want to delete '+this.state.unit.id,[
            {text: 'no!',type: 'green', func:'close'},
            {text: 'yes',type: 'red', func: this.deleteUnit},
        ]);
    }
    deleteUnit(){
        var unitID=this.state.unit.id;
        APPManager.Unit.remove({unitID: unitID}).then((function(){
            this.props.history.push('/units');
        }).bind(this))
        APPManager.setAlertMessage('',[]);
    }
    getUnitID(){
        if(this.props.create){
            return (
                <input className='text'
                    name='id'
                    onChange={this.handleInputChange}
                    value={this.state.unit.id}
                    type='text'
                    placeholder='You will use this in API calls'/>
            )
        }
        return (
                <input className='text'
                    name='id'
                    onChange={this.handleInputChange}
                    value={this.state.unit.id}
                    type='text'
                    placeholder='You will use this in API calls' disabled/>
            )
    }
    grandfatheringPolicy(){
        if(this.props.custom || this.props.create){
            return '';
        }
        return (
            <div className='form'>
                <div className='row'>
                    <div className='formTitle'>
                        Grandfathering policy
                    </div>
                </div>
                <div className='row'>
                    <div className='title'>What subset of customers do you want to grandfather</div>
                    <div className='field'>
                        <select name='grandfatheringPolicy' 
                            value={this.state.grandfatheringPolicy}
                            onChange={this.handleInputChange}>
                            <option value="grandfatherAll">
                                Grandfather all customers 
                            </option>
                            <option value="grandfatherCustom">
                                Grandfather only customers with custom pricing
                            </option>
                            <option value="grandfatherNone">
                                Don't grandfather any customer
                            </option>
                        </select>
                    </div>
                </div>
            </div>

        )
    }
    setLogicEditor(){
        var selectPricingLogic='';
        var selectWebhooksLogic='';
        var logicEditor='';
        var gotoPricingLogic=function(){}
        var gotoWebhookLogic=function(){}
        if(this.state.logicEditor=='webhooks'){
            selectPricingLogic='';
            selectWebhooksLogic='selected';
            gotoPricingLogic=(function(){
                this.setState({logicEditor: 'pricing'})  ;
            }).bind(this);
            logicEditor=(<WebhooksLogicEditor
                plans={this.state.plans}/>)
        }else{
            selectPricingLogic='selected';
            selectWebhooksLogic='';
            gotoWebhookLogic=(function(){
                this.setState({logicEditor: 'webhooks'});  
            }).bind(this);
            logicEditor=(<PricingLogicEditor
                plans={this.state.plans}/>)
        }
        return (
            <div>
                <div className='row darkBorder'>
                    <div className='formTitle'>
                        <div className={'tabSelector '+selectPricingLogic} 
                            onClick={gotoPricingLogic}>   
                            Pricing logic
                        </div>
                        <div className={'tabSelector '+selectWebhooksLogic} 
                            onClick={gotoWebhookLogic} >
                            Webhooks logic
                        </div>
                    </div>
                </div>
                {logicEditor}
            </div>
        )
    }
    render(){
        return (
            <div className='formwrapper'>
                <div className='form'>
                    {(function(){
                        if(this.props.custom){
                            return (
                                <div className='row'>
                                    <div className='formTitle'>
                                        GATEWAY-ID: {this.props.gateWayID}
                                    </div>
                                </div>
                            )
                        }
                    }).bind(this)()}
                    <div className='row'>
                        <div className='formTitle'>
                            Unit settings
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>Unit ID</div>
                        <div className='field'>
                            {this.getUnitID()}
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>Unit name</div>
                        <div className='field'>
                            <input
                                name='name'
                                onChange={this.handleInputChange}
                                value={this.state.unit.name}
                                className='text'
                                type='text'/>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>Unit description</div>
                        <div className='field'>
                            <textarea name='description'
                                onChange={this.handleInputChange}
                                className='textarea'
                                value={this.state.unit.description}
                                placeholder="For internal use">
                                {this.state.unit.description}
                            </textarea>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>Reset quantity each</div>
                        <div className='field'>
                            <select name='resetEachInterval'
                                onChange={this.handleInputChange}
                                value={this.state.unit.quantity.resetEachInterval}>
                                <option value={false}>Never reset quantity</option>
                                <option value={true}>Reset quantity to 0 each billing interval</option>
                            </select>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='title'>Billing settings </div>
                        <div className='field'>
                            <select
                                name='atEnd'
                                onChange={this.handleInputChange}
                                value={this.state.unit.invoiceGenerator.atEnd}>
                                <option value={true}>Bill at the end of each billing cycle</option>
                                <option value={false}>Bill at the begining of each billing cycle</option>
                            </select>
                        </div>
                    </div>
                </div>

                {this.grandfatheringPolicy()}

                <div className='form'>
                    {this.setLogicEditor()}
                </div>
                <br/>

                {this.submissionButton()}
                {this.deleteButton()}
            </div>
        )
    }
}
const clone=function(obj){
    const isList=function(obj){
        if(obj.constructor==Array){return true}
        return false;
    }
    const isDictionary=function(obj){
        if(obj.constructor==Object){return true;}
        return false;
    }
    if(isList(obj)){
        var newObj=[];
    }else if(isDictionary(obj)){
        var newObj={};
    }else{return obj;}
    for(var name in obj){
        var c=clone(obj[name]);
        if(typeof c=='undefined'||typeof name=='undefined'){continue};
        newObj[name]=c;
    }
    return newObj;
}
//List
class DeleteUnit extends Component{
    constructor(props){
        super(props);
        this.delete=this.delete.bind(this);
    }
    render(){
        return (
            <div className='conformation'>
                Are you sure that you want to delete this unit?
                <Link className='button green' to='/units'>
                    NO
                </Link>
                <Link className='button red' to='/units' onClick={this.delete}>YES</Link>

            </div>
        )
    }
}
class Element extends Component{
    constructor(props){
        super(props);
    }
    render(){
        /*
            <Link to={'/units/'+this.props.data.id+"/editpricing"} className='button' >
                <i className="symbol fa fa-pencil-square-o" aria-hidden="true"></i>
                EDIT
            </Link>
            <div onClick={this.launchDeleteAlert} className='button red' >
                <i className="symbol fa fa-trash-o" aria-hidden="true"></i>
                DELETE
            </div>
        */
        return (
            <Link to={'/units/'+this.props.data.id+"/edit"} className="Element large">
                <div className='column' style={{width: this.props.nameWidth}}>
                    {this.props.data.name}
                </div>
                <div className='buttons'>
                </div>
            </Link>
        )
    }
}
class List extends Component{
    constructor(props){
        super(props);
        this.state={elements: []}
        this.update=this.update.bind(this);
        APPManager.StateStore.subscribe(this.update);
        this.state.isMounted=false;
    }
    update(){
        var units=APPManager.Retrievers.listUnits();
        if(units===null){units=[];}
        if(units.length==0){
            if(APPManager.detectors.isLoading() && typeof APPManager.companyUpdateRequest!='undefined'){
                APPManager.companyUpdateRequest.then((function(){
                    this.update();
                }).bind(this));
            }else{
                window.location='/units/create';
            }
        }
        this.setState({elements: units});
    }
    buildList(){
        var list=[];
        var nameWidth='665px';
        this.state.elements.map(function(data,key){
            list.push(<Element key={data.id} data={data} nameWidth={nameWidth} />);
        });
        return list;
    }
    componentDidMount(){
        this.update();
    }
    render(){
        var toolBarButtons=[
            {text: 'CREATE UNIT', link: '/units/create/', symbol: 'fa-plus-square-o'},
        ]
        return (
            <div className='list'>
                <ToolBar buttons={toolBarButtons}/>
                <div className='ElementsWrapper'>
                    {this.buildList()}
                </div>
            </div>
        )
    }
}
//Wraps everything
class UnitView extends Component{
    constructor(props){
        super(props)
        var routes=[
            { type: List, path: '/units/' },
            { type: UnitEditor, path: '/units/create/', props: {create: true, custom: false } },

            //
            { type: UnitEditor, path: '/units/:gateWayID/:unitID/edit', props: {create: false,custom: true } },
            { type: UnitEditor, path: '/units/:unitID/edit', props: {create: false,custom: false, history: this.props.history } },


            { type: DeleteUnit, path: '/units/:unitID/delete' },

        ]
        this.state={routes: routes};
    }
    render(){
        return(
            <div className='UnitView'>
                <ComponentRouter routes={this.state.routes} />
            </div>
        )
    }
}
export { UnitView,UnitEditor }







