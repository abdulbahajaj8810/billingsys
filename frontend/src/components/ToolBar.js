import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'

class Button extends Component{
    render(){
        return (
            <Link to={this.props.link} className='Button'>
                <i className={'fa symbol '+this.props.symbol} aria-hidden="true"></i>
                
                {this.props.text}
            </Link>
        )
    }
}

class ToolBar extends Component{
    render(){
        return (
            <div className='toolBar'>

                {
                    this.props.buttons.map(function(button){
                        return <Button link={button['link']} text={button['text']} symbol={button['symbol']} />
                    })
                }
                {this.props.content}
            </div>
        )
    }
}
export { ToolBar }
