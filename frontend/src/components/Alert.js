import React, { Component } from 'react';
import APPManager from './APPManager'
// {
//     func: function(){},
//     text: 'bahajaj',
//     type: 'black',
// }
class Button extends Component{
	constructor(props){
        super(props);
    }
	render(){
        return (
            <div onClick={this.props.func}
            className={'button '+ this.props.type}>
                {this.props.text.toUpperCase()}
            </div>
        )
    }
}
class MessageBox extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.message={
            text: '',
            buttons: []
        };
        this.state.buttons=[];
        this.setMessage=this.setMessage.bind(this);
        APPManager.StateStore.subscribe(this.setMessage);
    }
    componentDidMount(){
        this.setMessage();
    }
    setMessage(){
        var state={};
        var alertMessage=APPManager.Retrievers.getAlertMessage();
        if(alertMessage===null){alertMessage={
            text: '',
            buttons: []
        };}
        var state={};
        state['message']=alertMessage
        this.setState(state);
    }
    setButtons(){
        var buttonsData=this.state.message.buttons;
        var buttons=[]
        for(var counter in buttonsData){
            var button=buttonsData[counter];
            if(button.func=='close'){
                button.func=this.props.toggle;
            }
            buttons.push(<Button text={button.text} type={button.type} func={button.func} />)
        }
        this.state.buttons=buttons;
    }
    render(){
        this.setButtons();
        return (
            <div className={'AlertMessage '+ (APPManager.detectors.isMessage()?'visible':'')} >
                <div className='textWrapper'>
                    {this.state.message.text}
                </div>
                <div className='buttonsWrapper'>
                    {this.state.buttons}
                </div>
            </div>
        )
    }

}
class Loading extends Component{

    render(){
        return (
            <div className={'Loading '+(APPManager.detectors.isLoading()?'visible':'')}>
                <i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
                <br/>
                <br/>
            </div>
        )
    }
}
class Alert extends Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.visible=false;
        this.update=this.update.bind(this);
    	APPManager.StateStore.subscribe(this.update);
        this.toggle=this.toggle.bind(this);
    }
    componentDidMount(){
        this.update();
    }
    update(setState=true){
        var isMessage=APPManager.detectors.isMessage();
        var isLoading=APPManager.detectors.isLoading();
        var visibility=isMessage || isLoading;
        this.setState({visible: visibility});
    }
    toggle(){
        var visibility=!this.state.visible;
        if(!visibility){
            if(APPManager.detectors.isMessage()){
                APPManager.setAlertMessage('',[]);
            }
        }
        this.setState({visible: visibility});
    }
    render(){
        return (
	        <div className={'AlertWrapper '+(this.state.visible?'visible':'')}>
	        	<div className="AlertShadow">&nbsp;</div>
                <MessageBox toggle={this.toggle}/>
                <Loading/>
        	</div>
        )
    }
}
export default Alert;