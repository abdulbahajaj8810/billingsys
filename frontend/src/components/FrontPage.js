import React, { Component } from 'react';
import APPManager from './APPManager';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import { Link } from 'react-router-dom'
import ComponentRouter from './ComponentRouter'
class Pricing extends Component{
	render(){
		return <div></div>
	}
}
class APIDemo extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.state.requestType='set'
		this.state.endPoint='endPoint'
		this.state.parameters=[{name: 'lalal', 'value': 'abbaba'}];
		this.state.response='response'
		this.state.text=''
		this.state.showResponse=false;
		this.updateRequestType=this.updateRequestType.bind(this);
		this.showResponse=this.showResponse.bind(this);
		this.updateRequestType();
	}
    updateRequestType(event) {
		var requests={};
		requests['set']={};
		requests['set']['requestType']='set';
		requests['set']['endPoint']='/api/quantity/set';
		requests['set']['parameters']=[
			{name: 'unitID', value: 'operator'},
			{name: 'quantity', value: '5'},
			{name: 'gateWayID', value: '{customer gateway ID}'},
			{name: 'APIKey', value: '{Your api key}'},
		];
		requests['set']['response']='{"status": "success","message": "Your request has been successful","code": "SuccessfulRequest"}';
    	requests['set']['response']=JSON.stringify(JSON.parse(requests['set']['response']),null,4)
    	requests['set']['description']=(
    		<div>
    			Sets the given customer's usage quantity to the given quantity
    			<br/>
	    		(customer's usage=given usage)
    		</div>
    	); 
		requests['add']={};
		requests['add']['requestType']='add';
		requests['add']['endPoint']='/api/quantity/add';
		requests['add']['parameters']=[
			{name: 'unitID', value: 'apiCall'},
			{name: 'quantity', value: '1'},
			{name: 'gateWayID', value: '{customer gateway ID}'},
			{name: 'APIKey', value: '{Your api key}'},
		];
		requests['add']['response']='{"status": "success","message": "Your request has been successful","code": "SuccessfulRequest"}';
    	requests['add']['response']=JSON.stringify(JSON.parse(requests['add']['response']),null,4)
    	requests['add']['description']=(
    		<div>
				Adds the given quantity to the given customer's usage
				<br/>
				(customer's usage += given usage)    		
			</div>
    	); 

		requests['getHistory']={};
		requests['getHistory']['requestType']='getHistory';
		requests['getHistory']['endPoint']='/api/quantity/history';
		requests['getHistory']['parameters']=[
			{name: 'gateWayID', value: 'cus_B5bR9ryRGp0tcv'},
			{name: 'APIKey', value: 'bdd57526ac8748349939aa7ebc64326e'},
		];
		requests['getHistory']['response']='{"status": "success","message": "Your request has been successful","code": "SuccessfulRequest"}';
    	requests['getHistory']['response']=JSON.stringify(JSON.parse(requests['getHistory']['response']),null,4)
    	
		var requestType={}
    	var value=''
    	var setState=false;
        if(typeof event=='undefined'){
			requestType=requests['set'];
        }else{
	        const target = event.target;
	        value=target.type === 'checkbox' ? target.checked : target.value;
	        const name=target.name;
			requestType=requests[value];
			setState=true;
		}

		requestType['text']=APPManager.endPoint+requestType.endPoint;
		requestType.parameters.map(function(parameter){
			requestType['text']+="\n\t"+parameter.name+"="+parameter.value;
		});
		requestType['showResponse']=false;

    	if(setState){
			this.setState(requestType);
    	}else{
        	this.state.requestType=requestType.requestType;
        	this.state.endPoint=requestType.endPoint;
        	this.state.parameters=requestType.parameters;
        	this.state.response=requestType.response;
        	this.state.description=requestType.description;
	    	this.state.text=requestType.text;
    	}
    }
    showResponse(){
    	this.setState({showResponse: true});
    }
	render(){
		return (	
			<div>
				<div className='title'>
				 An easy API that is built by developers for developers
				</div>
				<div className='requestSelection'>
					<select onChange={this.updateRequestType}
					value={this.state.requestType}>
						<option value='set'>
							Set customer's usage quantity
						</option>
						<option value='add'>
							Add to customer's usage quantity
						</option>
					</select>
				</div>
				<div className='description'>
					{this.state.description}
				</div>
				<div className='EndPoint'>
					Endpoint<br/>
					<textarea type='text' value={this.state.text}>
						

					</textarea>
				</div>
				<div className="ShowResponse" onClick={this.showResponse}>
					show response
				</div>
				{(function(){
					if(!this.state.showResponse){
						return ""
					}
					return (
						<div className='Response'>
							<textarea value={this.state.response}></textarea>
						</div>
					)
				}).bind(this)()}
			</div>
		)

	}
}
class Examples extends Component{
	render(){
		return (
			<div className='Examples section'>
				<div className='left'>
					<APIDemo/>
				</div>
				<div className='right'>
					<div className='title'>
					&nbsp;Use cases
					</div>
					
					<div className='UseCase bubble first'>
						Charge customers per API call
					</div>
					<div className='UseCase bubble'>
						Charge customers per operator per month
					</div>
					<div className='UseCase bubble'>
						Charge customers for exceeding their monthly quota 
					</div>
					<div className='UseCase bubble'>
						Charge customers per GB used per month
					</div>
					<div className='UseCase bubble'>
						Charge customers per server usage per month
					</div>
					<div className='UseCase bubble last'>
						Charge customers per page view
					</div>
				</div>
			</div>
		)
	}
}
class Demo extends Component{
	render(){
		return (
			<div className='Demo'>
				Demo
			</div>
		)
	}
}
class Features extends Component{
	render(){
		return(
			<div className='Features section'>
				<div className='title'>Features</div><br/>
				<div className='Feature bubble first'>
					Fully integrated with stripe.
					<div className='explanation'>
						After you build your billing system
						using stripe all you have to do is to tell us what your pricing is using our
						user interface and then tell us the quantity that your customers have used using our API.
						Customers are identified by their stripe ID. This makes SubFrog more secure since
						all of your customer's data is secured by stripe. In addition, this enable you to stop
						using SubFrog anytime you want with no effort.
					</div>
				</div>
				<div className='Feature bubble'>
					Expressive pricing editor
					<div className='explanation'>
						Our pricing editor enables you to create any pricing that you might want. 
						For example, you can have different pricing depending on the plan that customers are subscribed to and/or the quantity that they have used.
					</div>
				</div>
				<div className='Feature bubble'>
					Change your pricing in less than a minute
					<div className='explanation'>
						You can easily change your pricing using our user interface without changing a line of code.
						Your new pricing will apply to all new customers the moment you press save.
					</div>
				</div>
				<div className='Feature bubble'>
					Custom pricing
					<div className='explanation'>
						Want to give customers custom pricing? You can do that in less than minute using our user interface and without changing a line of code.
					</div>
				</div>
				<div className='Feature bubble'>
					Price estimation
					<div className='explanation'>
						Change your pricing without having to change your UI. Our Javascript library retrieves your latest pricing model and
						gives you price estimation for each quantity and/or plan.

					</div>
				</div>
				<div className='Feature bubble'>
					Grandfather customers
					
					<div className='explanation'>
						Keep your promises. Changes to pricing will not impact existing customers, unless you choose to. 
					</div>
				</div>
				<div className='Feature bubble'>
					MicroPayments
					<div className='explanation'>
						We enable micropayments by precharging customers automatically an amount of your choosing
						when their balance is zero. The amount precharged is stored in stripe as credit.
					</div>
				</div>

				<div className='Feature bubble'>
					Access customers' usage history, current rate and many more
					<div className='explanation'>
						Transparency is important. Our javascript library helps you be more transparent by giving you your customers' usage history, 
						the rate that they are being charged at, the amount that they have to pay in the next billing cycle and the quantity that they have 
						used in the current billing cycle.
					</div>
				</div>
				<div className='Feature bubble last'>
					Stay in the loop through webhooks
					<div className='explanation'>
						SubFrog sends you webhooks when customer's usage reaches a quantity of your choosing.
						This enables you, for example, to know when one of your customers exceeds their usage.
					</div>
				</div>
			</div>
		)
	}
}
class About extends Component{
	constructor(props){
		super(props);
		this.createEmptyCompany=this.createEmptyCompany.bind(this);
	}
	createEmptyCompany(){
		APPManager.Company.registerEmptyCompany().then(function(){
	        window.location='/units/create/';
  		});
	}
	render(){
		return (
			<div className='About section'>
				<div className='title'>About</div><br/>
				<div className='textDescription'>
					Charge your customers based on usage. 
					SubFrog provides you with the tools you need to build a metered billing system.
					All you have to do is to tell us what your pricing is using our UI and tell us 
					what your customers usage is through our API. We will then charge your customers using Stripe.
				</div>

				<div className='GetInButton'  onClick={this.createEmptyCompany}>
					GET IN 
					<i className="symbol fa fa-arrow-right" aria-hidden="true"></i>
				</div>
			</div>
		)
	}
}
class Header extends Component{
	render(){
		return (
			<div className='Header'>
				<div className='Logo'>
					SubFrog
				</div>
				<Link to="/" className='Button'>
					<i className="symbol fa fa-bars" aria-hidden="true"></i>
					Overview 
				</Link>
				<div className='Button'>
					<i className="symbol fa fa-credit-card-alt" aria-hidden="true"></i>
					Pricing
				</div>
				<div className='right'>
					<div className='Button'>
						<i className="symbol fa fa-code" aria-hidden="true"></i>
						Documentation
					</div>
					<Link to="/login" className='Button'>
						<i className="symbol fa fa-sign-in" aria-hidden="true"></i>
						Login
					</Link>
				</div>
			</div>
		)
	}
}
class Login extends Component{
	constructor(props){
		super(props);
		this.submit=this.submit.bind(this);
		this.handleInputChange=this.handleInputChange.bind(this);
		this.state={};
		this.state.email='';
		this.state.password='';
	}
	submit(){
		var data={};
		data['email']=this.state.email;
		data['password']=this.state.password;
		APPManager.Member.login(data).then(function(){
			window.location='/decide'
		})
	}
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
	render(){
		return (
			<div>
				<Header/>
				<div className='Login'>
					<div className='field'>
						<input onChange={this.handleInputChange}
							value={this.state.email}
							name="email" 
							type="email" 
							placeholder="Email address"/>
					</div>
					<div className='field'>
						<input onChange={this.handleInputChange}
							value={this.state.password}
							name="password" 
							type="password" 
							placeholder="Password"/>
					</div>
					<div className='field'>
						<div className='submit' onClick={this.submit}>LOGIN</div>
					</div>
					<div className='field'>
						<div className='link'>Forgot password</div>
					</div>
				</div>
			</div>
		)
	}
}
class Overview extends Component{
	render(){
		return (
			<div>
				<Header/>
				<About/>
				<Features/>
				<Examples/>
			</div>
		)
	}
}
class FrontPage extends Component{
	constructor(props){
		super(props);
	    var routes=[
	        { type: Overview, path: '/' },
	        { type: Login, path: '/login', props: {create: true} },
	        // { type: UnitEditor, path: '/units/:unitID/editpricing', props: {create: false} },
	        // { type: WebhookEditor, path: '/units/:unitID/editwebhooks' },
	        // { type: DeleteUnit, path: '/units/:unitID/delete' },
	    ]        
	    this.state={routes: routes};
	}
	render(){
        return(
            <div className='FrontPage'>
                <ComponentRouter routes={this.state.routes} />
            </div>
        )
	}
}
export default FrontPage