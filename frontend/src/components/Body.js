import React, { Component } from 'react';
import {UnitView} from './UnitView';
import CustomerView from './CustomerView';
import APIView from './APIView';
import ReportView from './ReportView';
import SettingsView from './SettingsView';
import APPManager from './APPManager';
class Body extends Component {
    render(){
        var content="";
        if( this.props.view=='units' ){
            content=<UnitView history={this.props.history} />;
        }else if ( this.props.view=='customers' ){
            content=<CustomerView history={this.props.history} />;
        }else if ( this.props.view=='api' ){
            content=<APIView/>;
        }else if ( this.props.view=='report' ){
            content=<ReportView/>;
        }else if ( this.props.view=='settings' ){
            content=<SettingsView/>;
        }
        else if(this.props.view=='decide'){
            APPManager.companyUpdateRequest.then((function(){
                var history=APPManager.Retrievers.listCustomers();
                if(!(history==null)){
                    if(Object.keys(history).length>0){
                        this.props.history.push('/dashboard')
                        return;
                    }
                }
                var customers=APPManager.Retrievers.listCustomers();
                if(!(customers==null)){
                    if(customers.length>0){
                        this.props.history.push('/customers');
                        return;
                    }
                }
                var units=APPManager.Retrievers.listUnits();
                if(!(units==null)){
                    if(units.length>0){
                        this.props.history.push('/units');
                        return;
                    }
                }
                this.props.history.push('units/create');
            }).bind(this));
        }
        return <div className='Body'>{content}</div>
    }
}
export default Body;
