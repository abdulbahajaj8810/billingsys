import axios from 'axios';
/*
open -a Google\ Chrome --args --disable-web-security --user-data-dir

celery -A billingSys worker -l info

*/
const getCookie=(cname)=>{
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++){
        var c = ca[i];
        while (c.charAt(0) == ' '){
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0){
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
const csrfmiddlewaretoken=getCookie('csrftoken');
const AjaxWrapper=(subpath,data) => {
    if(typeof data=='undefined'){data={}}
    var path='http://localhost:8000/api/'+subpath;
    data.csrfmiddlewaretoken=csrfmiddlewaretoken;
    var request=axios({
        headers: {'X-CSRFToken':csrfmiddlewaretoken},
        method: 'post',
        url: path,
        data: data,
    });
    return request;
}
export default AjaxWrapper;
