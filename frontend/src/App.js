import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Body from './components/Body';
import NavMenu from './components/NavMenu';
import Register from './components/Register';
import Login from './components/Login';
import FrontPage from './components/FrontPage2'
import Alert from './components/Alert'
import SignupWidget from './components/SignupWidget'
import Goto from './components/Goto'
import '../node_modules/react-vis/dist/style.css';

import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
class Wrapper extends Component{
    render(){
        const pageName=this.props.match.params.pageName;
        return(
            <div>
                <Goto/>
                <SignupWidget/>
                <Alert/>
                <div className='wrapper' >
                    <NavMenu />
                    <Body view={pageName} history={this.props.history}/>
                </div>
            </div>
        )
    }
}
class App extends Component{
    constructor(props){
        super(props)
        this.state={}
        this.state.user={};
        this.state.user.permission=undefined;
        this.state.user.isLoggedIn=false;
    }
    render(){
        return (
            <Router>
                <Switch>
                    <Route path='/register/' component={Register} ></Route>
                    <Route path='/login' component={FrontPage} ></Route>
                    <Route path='/:pageName/' component={Wrapper} ></Route>
                    <Route path='/' component={FrontPage} ></Route>
                </Switch>
            </Router>
        );
    }
}
// <Route path='/:name/' component={DashBoard}></Route>
// <Route path='/:name/' component={Customers}></Route>
// <Route path='/:name/' component={Units}></Route>
// <Route path='/:name/' component={API}></Route>
// <Route path='/:name/' component={Settings}></Route>
export default App;
