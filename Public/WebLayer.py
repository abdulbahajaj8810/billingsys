from django.core.handlers.wsgi import WSGIRequest
import inspect,json
from models.Company import Company
from models.Member import Member
from django.http import HttpResponse
from django.template import Context as DjangoContext, Template as DjangoTemplate
from Public.Responses import *
from django.shortcuts import redirect

class DataBaseRetriever(object):
	__MAP={}
	__accessPoints={}
	__accessPoint=None
	def __init__(self,inputs):
		self.__request=request
		self.__inputs=inputs
		self.__cache={}
		self.retrieve=self.__retrieve
		self.getAccessPoint=self.__getAccessPoint
		for accessPointName in self.__accessPoints:
			accessPointValue=self.retriever(name=accessPointName)
			if accessPointValue is not None:
				self.__accessPoint=accessPointName
				break
	def __getAccessPoint(self):
		return self.__accessPoint
	def __input(self,name):
		return self.__inputs.get(name,None)
	def __retrieve(self,name):
		retriever=self.__MAP(name,None)

		cachedValue=self.getCache(name=name)
		if cachedValue is not None: return cachedValue

		if retriever is None:
			return None
		retrievedValue=retriever(self=self)
		return retrievedValue
	def __addCache(self,name,value):
		if name is None: return
		if value is None: return
		self.__cache[name]=value
	def __getCache(self,name=None):
		if name is not None:
			return self.__cache.get(name,None)
		return self.__cache
	@classmethod
	def addFunc(cls,name,accessPoint=True):
		def dec(func):
			def wrapper(self):
				value=func(self=self)
				self.__addCache(name=name,value=value)
				return value
			cls.__MAP[name]=wrapper
			if accessPoint:
				cls.__accessPoints[name]=wrapper
			return None
		return dec

Retriever.addFunc(name='member',accessPoint=True)
def getMember(self):
    if not self.__request.user.is_authenticated():
    	return None
    else:
        username=self.__request.user.username
        member=Member.getByID(id=username)
    return member
Retriever.addFunc(name='company',accessPoint=True)
def getCompany(self):
	member=self.__retrieve(name='member')
	if isinstance(member,Member):
		company=member.company
        return company
	APIKey=self.__input('APIKey')	
	if APIKey is None: return None
	company=Company.objects(APIKey=APIKey)
	if len(company)!=1:
		return None
	company=company[0]
	if not company.authenticate(APIKey=APIKey): return None
	return company
Retriever.addFunc(name='unitManager')
def getUnitManager(self):
    company=self.__retrieve(name='company')
    if company is None: return None
    unitManager=company.getDefaultPricing()
    return unitManager
Retriever.addFunc(name='unit')
def getUnit(self):
    unitID=self.__input('unitID')
    if unitID is None: return None
    unitManager=self.__retrieve(name='unitManager')
    if unitManager is None: return None
    unit=unitManager.get(id=unitID)
    return unit
Retriever.addFunc(name='webhookManager')
def getWebhookManager(self):
    unit=self.__retrieve(name='unit')
    if unit is None: return None
    webhookManager=unit.getWebhookManager()
    return webhookManager
Retriever.addFunc(name='gateWayConnector')
def getGateWayConnector(self):
    company=self.__retrieve(name='company')
    if company is None: return None
    gateWayConnector=company.GateWayConnector
    gateWayConnector.initialize()
    return gateWayConnector
Retriever.addFunc(name='logsList')
def listLogs(self):
    step=self.__input('listStep')
    if step is None: step=0
    company=self.__retrieve(name='company')
    if company is None: return None
    customer=self.__retrieve(name='customer')
    listAll=False
    if customer is not None:
    	listAll=True
    logs=Logger.list(
        company=company,
        step=step,
        customer=customer,
        all=listAll)
    return logs
Retriever.addFunc(name='historyList')
def listHistory(self):
    customer=self.__retrieve(name='customer')
    company=self.__retrieve(name='company')
    if customer is None and company is None: 
    	return None
    history=HistoryLogger.list(customer=customer,company=company)
    if history is None: return {}
    return history
Retriever.addFunc(name='customer')
def getCustomer(self):
    gateWayID=self.__input('gateWayID')
    if gateWayID is None: return None
    company=self.__retrieve(name='company')
    if company is None: return None
    customer=company.getCustomer(gateWayID=gateWayID)
    if customer is None: return None
    return customer
Retriever.addFunc(name='customerList')
def listCustomers(self):
    step=self.__input('listStep')
    if step is None: return None
    try:
	    step=int(step)
    except:
    	return None
    company=self.__retrieve(name='company')
    if company is None: return None
    customerList=company.listCustomers(step=step)
    return customerList


def isAuthenticated(retriever):
	if retriever.accessThrough is None: DontHavePermissionResponse.get()
	if retriever.accessThrough=='member':
	    member=retriever.member
	    if member is None: return DontHavePermissionResponse.get()
	elif retriever.accessThrough=='api':
	    company=retriever.company
	    if company is None:
	        return DontHavePermissionResponse.get()
	else:
	    return UnAuthenticatedRequestResponse.get()
def getResponse(request,authenticated,cls=None,self=None):
    inputs=getInputHolder(request=request)
    retriever=Retrievers.get(request=request)




    if authenticated is not False:




    expectedInputs=getExpectedInputs(
        retriever=retriever,
        request=request,
        cls=cls,
        self=self,
    )
    try:
        pass
    except TypeError:
        output=MissingArgumentsResponse.get()
    print 'expectedInputs: ',expectedInputs
    output=func(**expectedInputs)
    log(inputs=inputs,output=output,retriever=retriever,request=request)
    print 'type of output: ', output
    output=addReleventInfo(output=output,retriever=retriever)
    return output
def removeIrreleventInfo(output):            
    try:
        del output['includeCompanyInfo']
    except: pass
def argsToDict(args):
    result={}
    firstElement=args[0]
    if isinstance(firstElement,WSGIRequest):
        result['request']=firstElement
    else:
        if isinstance(firstElement,type):
            result['cls']=firstElement
        else:
            result['self']=firstElement
        for element in args:
            if isinstance(element,WSGIRequest):
                result['request']=element
    return result
def parseInput(*args,**kwargs):
	args=argsToDict(args=args)
	ignoreWebLayer=kwargs.get('byPass',False)
	request=args.get('request',None)
	cls=args.get('cls',None)
	self=args.get('self',None)
	isHttp=kwargs.get('isHttp',False)
	excludeArgs=kwargs.get('excludeArgs',False)
	return (self,cls,request,ignoreWebLayer,isHttp,excludeArgs)


def getFuncInputs(func):
	return inspect.getargspec(func).args

def getFileContent(file):
	if file is None: return None
    with open(file) as fileContent:
        fileContent=fileContent.read()
    return fileContent


def getRequestInputs(request):
    inputHolder={}
    method=request.method.lower()
    if method=='get':
        inputHolder=request.GET
    elif method=='post':
        inputHolder=request.POST
    if len(inputHolder) == 0:
        inputHolder=request.body
        if len(inputHolder) > 0:
            inputHolder=json.loads(inputHolder)
        else:
            inputHolder={}
    return inputHolder

def getFuncArgs(func,request,authenticated,
	APIAccessible,file,cls,self):
	
	funcArgs={}
	permissible=False

	requestInputs=getRequestInputs(request=request)

	DBRetriever=DataBaseRetriever(inputs=requestInputs)
	accessPoint=DBRetriever.getAccessPoint()

	if accessPoint == 'member':
		permissible=True
	elif accessPoint == 'company' and APIAccessible is True:
		permissible=True
	else:
		return ({},False)

    funcInputs=getFuncInputs(func=func)

    for name in funcInputs:
    	if name=='request':
    		funcArgs[name]=request
    		continue
    	if name=='cls':
    		funcArgs[name]=cls
    		continue
    	if name=='self':
    		funcArgs[name]=self
    		continue
    	if name=='template':
    		content=getFileContent(path=file)
    		if not isinstance(content,str): continue
    		funcArgs[name]=DjangoTemplate(fileContent)
    		continue
    	if name=='file':
    		content=getFileContent(path=file)
    		funcArgs[name]=DjangoTemplate(fileContent)
    		continue
    	dbValue=DBRetriever.retrieve(name=name)
    	if dbValue is not None:
    		funcArgs[name]=dbValue
    		continue
    	requestInputValue=requestInputs.get(name,None)
    	if requestInputValue is not None:
    		funcArgs[name]=dbValue
    		continue

	return (funcArgs,permissible)


#Connects the python program with the web
def WebLayer(
    method=None,#Dictates the method that the view must be accessed with
    authenticated=True,#Dictates that the access to the api requires authentication
    APIAccessible=False,#dictates whether or not a view can be accessed through the API
    file=None):
    def decorator(func):
        def wrapper(*args,**kwargs):

        	self,cls,request,ignoreWebLayer,isHttp,excludeArgs=parseInput(*args,**kwargs):

            if ignoreWebLayer is True:
                if excludeArgs is True:
                    output=func(**kwargs)
                else:
                    output=func(*args,**kwargs)
                if isHttp:
                    output=makeHttp(output)
                return output

            if method is not None:
                if not (request.method.upper()==method.upper()):
					context={}
					context['wrongMethod']=request.method.upper()
					context['properMethod']=method.upper()
					return WrongRequestMethodResponse.get(context=context)

			funcArgs,permissible=getFuncArgs(func=func,
				authenticated=authenticated,
				APIAccessible=APIAccessible,
				file=file,
				cls=cls,
				self=self)

			if permissible is not True:
				return DontHavePermissionResponse.get()

			output=func(**funcArgs)

            output=removeIrreleventInfo(output=output)
            output=makeHttp(output=output)
            return output
      

        wrapper.__name__=func.__name__
        return wrapper
    return decorator
