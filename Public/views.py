from models.Company import Company
from models.Customer import CompanyCustomer
from django.http import HttpResponse
import inspect,json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as Authlogout
from models.Logger import Logger
from django.shortcuts import redirect
from django.template import Context as DjangoContext, Template as DjangoTemplate
from Public.Responses import *
from Public.ViewDecorators import accessible,viewDec
from models.Tasks.ProcessRequest import processAPIRequest
from models.Tasks.GenerateInvoices import generateInvoices
from models.Member import Member
from models.Tasks.WebhookSender import sendWebhook
@accessible.include(className='member')
class MemberView(object):
    @staticmethod
    @viewDec(method='get')
    def edit(member,email):
        member.edit(email=email)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get')
    def editPassword(member,currentPassword,newPassword):
        state=member.editPassword(currentPassword=currentPassword,newPassword=newPassword)
        if state is False:
            return InfoDidntMatchRecordResponse.get(context={'entity':'password'})
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get')
    def remove(member,memberID):
        state=member.removeMember(memberID=memberID)
        if state==-1:
            return DoesntExistResponse.get(context={'entityName': 'Member'})
        if state is False:
            return DontHavePermissionResponse.get()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get',authenticated=False)
    def add(email,password,code):
        state=Member.Invitation.createMember(
            email=email,
            password=password,
            code=code
        )
        if state==-2:
            return ExpiredResponse.get(context={'entityName': 'The entered code'})
        if state==-3:
            return CustomResponse.get(context={
                "message": 'There already exists an account with the given email address'
            })
        if state==-4:
            return DoesntExistResponse.get(context={
                'entityName': 'Entered code'
            })
        if isinstance(state,Member):
            member=state.json()
            member=json.dumps(member)
            member=HttpResponse(member)
            return member
    @staticmethod
    @viewDec(method='get',authenticated=False)
    def logout(request):
        Authlogout(request)
        return SuccessfulRequestResponse.get()
@accessible.include()
class Unit(object):
    @classmethod
    @viewDec(method='post')
    def create(cls,company,unitManager,unitID,unitName,unitDescription,resetEachInterval=False,
        atEnd=False,pricing=[],webhooks=[]):
        try:
            unitID=str(unitID)
            unit=unitManager.add(id=unitID,name=unitName,description=unitDescription)
        except:
            return AlreadyExistResponse.get(context={'EntityName': 'A unit with the given unit ID'})

        response=cls.edit(
            unitID=unitID,
            company=company,
            byPass=True,
            unit=unit,
            pricing=pricing,
            resetEachInterval=resetEachInterval,
            atEnd=atEnd,
            unitManager=unitManager,
            webhooks=webhooks,
        )
        return response
    @staticmethod
    @viewDec(method='post')
    def edit(unitID,unit,company,unitManager,grandfatheringPolicy='grandfatherAll',
        unitName=None,unitDescription=None,atEnd=None,
        resetEachInterval=None,pricing=[],webhooks=[],customer=None):
        if customer is not None:
            unitManager=customer.getUnitManager()
            unit=unitManager.getOrAdd(unit=unit)
            unit.custom=True

        unit.edit(name=unitName,description=unitDescription)
        unit.PricingManager.set(pricings=pricing)
        if resetEachInterval=='true': resetEachInterval=True
        elif resetEachInterval=='false': resetEachInterval=False
        if atEnd=='true': atEnd=True
        elif atEnd=='false': atEnd=False
        unit.InvoiceGenerator.edit(atEnd=atEnd)
        unit.Quantity.edit(resetEachInterval=resetEachInterval)
        unit.WebhookManager.setWebhooks(webhooksInput=webhooks)
        if customer is None:
            if grandfatheringPolicy=='grandfatherAll':
                grandFatherAllCustomers=True
                grandfatherCustomersWithCustomPricing=True
            elif grandfatheringPolicy=='grandfatherCustom':
                grandFatherAllCustomers=False
                grandfatherCustomersWithCustomPricing=True
            elif grandfatheringPolicy=='grandfatherNone':
                grandFatherAllCustomers=False
                grandfatherCustomersWithCustomPricing=False
            CompanyCustomer.applyGrandfatheringPolicy(company=company,
                grandfatherCustomersWithCustomPricing=grandfatherCustomersWithCustomPricing,
                grandFatherAllCustomers=grandFatherAllCustomers,unit=unit)
            company.save()
        else:
            customer.save()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='post')
    def remove(company,unitManager,unitID):
        unitID=str(unitID)
        unitManager.remove(id=unitID)
        company.save()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='post')
    def generateMockWebhook(company):
        endpointURL=company.getEndPoint()
        mockData={}
        mockData['plan']='mockPlan'
        mockData['quantity']=99999
        mockData['unit']='mockUnitID'
        mockData['gateWayID']='mockGateWayID'
        mockData['constants']={}
        mockData['constants']['mockConstantName1']='mockConstantValue1'
        mockData['constants']['mockConstantName2']='mockConstantValue2'
        mockData['constants']['mockConstantName3']='mockConstantValue3'

        sendWebhook(endpoint=endpointURL,data=mockData)
        return HttpResponse('ss')
@accessible.include(className='company')
class CompanyView(object):
    @staticmethod
    @viewDec(method='post')
    def getAPIKey(member,password):
        APIKey=member.getAPIKey(password=password)
        if APIKey is False:
            return WrongResponse.get(context={'entityName': 'password'})
        response={
            'APIKey': APIKey
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @viewDec(method='get')
    def testAPIKey(company,APIKey):
        status=company.authenticate(APIKey=APIKey)
        response={
            'status': status
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @viewDec(method='get')
    def addGateWayCredentials(company,gateWayConnector,GET):
        # try:
        gateWayConnector.Credentials.authorize(GETParameters=GET,company=company)
        # except:
        #     return AnErrorOccuredResponse.get()
        company.save()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get',authenticated=False)
    def isConnected(gateWayConnector):
        response={
            'isConnected': gateWayConnector.isConnected()
        }
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @viewDec(method='post')
    def edit(company,name=None,currency=None,endpointURL=None):
        company.edit(name=name,currency=currency,endpointURL=endpointURL)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='post')
    def get(company,member=None,httpResponse=True):
        return SuccessfulRequestResponse.get()
        # response=company.json()
        # response['']
        # response['authenticatedUser']={}
        # response['authenticatedUser']['loggedIn']=False
        # if member is not None:
        #     response['authenticatedUser']['loggedIn']=True
        # if httpResponse is True:
        #     response=json.dumps(response)
        #     response=HttpResponse(response)
        # return response
    @staticmethod
    @viewDec(method='get')
    def resetAPI(member,password):
        state=member.resetAPI(password=password)
        if state is None:
            return WrongResponse.get(context={'entityName': 'password'})
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='post',authenticated=False)
    def register(request,placeholder,email,password):
        state=Company.register(
            placeholder=placeholder,
            email=email,
            password=password,
        )
        if state==-1:
            return AnErrorOccuredResponse.get()
        elif state==-2:
            return CustomResponse.get(context={
                "message": 'There already exists an account with the given email address'
            })
        # return SuccessfulRequestResponse.get()
        return login(
            byPass=True,
            request=request,
            email=email,
            password=password
        )
    @staticmethod
    @viewDec(method='post')
    def getAPICalls(logsList):
        currentStep=logsList[1]
        logsList=logsList[0]
        result=[]
        for log in logsList:
            result.append(log.json())
        response={'apiCalls': result,
            'currentStep': currentStep}
        response=json.dumps(response)
        response=HttpResponse(response)
        return response
    @staticmethod
    @viewDec(method='post')
    def updateSettings(company,endpointURL=None,preChargeAmount=None):
        print endpointURL
        if endpointURL is not None:
            endpointURL=str(endpointURL)
            company.endpointURL=endpointURL
        if preChargeAmount is not None:
            gateWayConnector=company.getGateWayConnector()
            gateWayConnector.setPreChargeAmount(preChargeAmount=preChargeAmount)
        company.save()
        company=Company.objects(id=company.id)
        print company[0].endpointURL
        return SuccessfulRequestResponse.get()
    @staticmethod
    def registerEmptyCompany(request,*args,**kwargs):
        # print request
        # print args
        # print kwargs
        state=Company.createEmpty()
        if state==-1:
            return AlreadyExistResponse.get(context={'EntityName': 'company'})
        if state==-2:
            return AnErrorOccuredResponse.get()
        placeholder=state.name
        return login(
            byPass=True,
            isHttp=True,
            request=request,
            email=placeholder,
            password=placeholder
        )
@accessible.include()
class Customer(object):
    @staticmethod
    @viewDec(method='post')
    def listCustomers(company,customerList):
        output=[]
        currentStep=customerList[1]
        customerList=customerList[0]
        for customer in customerList:
            customer=customer.json()
            output.append(customer)
        output={'customers': output, 'currentStep': currentStep}
        output=json.dumps(output)
        output=HttpResponse(output)
        return output
    @staticmethod
    @viewDec(method='post')
    def update(customer):
        customer.updatePricing()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get',authenticated=False)
    def sync(customer):
        customer.sync()
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='post')
    def get(customer=None,historyList=None):
        if customer is None:
            return NotFoundResponse.get(context={'EntityName': 'Customer'})
        customer=customer.json()
        customer['history']=historyList
        customer=json.dumps(customer)
        return HttpResponse(customer)
    @staticmethod
    @viewDec(method='post')
    def history(customer,historyList,httpResponse=True):
        if httpResponse:
            historyList=json.dumps(historyList)
            historyList=HttpResponse(historyList)
        return historyList
@accessible.include()
class GateWay(object):
    @staticmethod
    @viewDec(method='post')
    def edit(gateWayConnector,preChargeAmount=0):
        print 'preChargeAmount',preChargeAmount
        try:
            preChargeAmount=int(preChargeAmount)
        except:
            return CustomResponse.get(context={
                'message': "The precharge amount has to be an integer"
            })
        gateWayConnector.setPreChargeAmount(preChargeAmount=preChargeAmount)
        return SuccessfulRequestResponse.get()
    @staticmethod
    @viewDec(method='get')
    def connect(company,gateWayConnector):
        # return HttpResponse(type(gateWayConnector))
        link=gateWayConnector.Credentials.buildLink()
        company.save()
        return redirect(link)
class WebhookHandler(object):
    @staticmethod
    def verifyEvent(): return False
    @classmethod
    def handle(cls): return HttpResponse("")
@accessible.include()
class StripeWebhookHandler(WebhookHandler):
    @staticmethod
    def verifyEvent(): return True
    @staticmethod
    def handle(request):
        if not StripeWebhookHandler.verifyEvent():
            return HttpResponse(status=401)
        gateWayID="cus_B5bR9ryRGp0tcv"
        company=Company.objects(APIKey='09a8e155992f4d80ab75bb63c0b11609')[0]
        companyID=str(company.id)
        subscriptionID='sub_BJrWSBKVgASfAU'
        generateInvoices.delay(companyID=companyID,
            gateWayID=gateWayID,subscriptionID=subscriptionID)
        return HttpResponse(status=200)
@accessible.include(pattern='api/member/login')
@csrf_exempt
@viewDec(method='post',authenticated=False)
def login(request,email,password):
    state=Member.login(
        request=request,
        email=email,
        password=password
    )
    if state==-1:
        return NotFoundResponse.get(
            redirect='/',
            context={'EntityName': 'Member'})
    if state==-2:
        return DoNotMatchResponse.get(
            redirect='/',
            context={'EntitiesName': 'Company name, email and password'})
    if state is True:
        return SuccessfulRequestResponse.get(
            redirect='/decide',
            context={'EntitiesName': 'Company name, email and password'})



@accessible.include()
class Log(object):
    @staticmethod
    @viewDec(method='get',APIAccessible=True)
    def verify(self,company,id):
        print 's1'
        result=Logger.verify(company=company,id=id)
        print 's2'
        if result:
            return HttpResponse(CustomSuccessResponse.get(context={'custom': 'True'}))
        return HttpResponse(CustomSuccessResponse.get(context={'custom': 'False'}))




@accessible.include()
@csrf_exempt
def change(request):
    # if request.method.upper() != 'POST':
    #     context={}
    #     context['wrongMethod']=request.method.upper()
    #     context['properMethod']='POST'
    #     return WrongRequestMethodResponse.get(context=context)
    processAPIRequest.delay(request.GET)
    return HttpResponse(json.dumps({
        'status': 'queued',
        'code': 'Processing',
        'message': "You request has been queued and is being processed"
    }))








# @accessible.include(className='js')
# class JSLibrary(object):
#     jsLibraryPath="Public/jsLibrary/"
#     @staticmethod
#     @viewDec(authenticated=False,file=jsLibraryPath+'main.js')
#     def load(request,publicKey,template):
#         company=Company.get(publicAPIKey=publicKey)
#         if company is False:
#             return WrongResponse.get(context={'entityName': 'public API key'})
#         unitTemplateManager=company.UnitTemplateManager
#         pricingTemplate=unitTemplateManager.getDefault()
#         if pricingTemplate is None:
#             units=[]
#         else:
#             unitManager=pricingTemplate.UnitManager
#             units=unitManager.json()
#         data={
#             'units': units,
#         }
#         data=json.dumps(data)
#         template=template.render(DjangoContext({
#             'PublicAPIKey': publicKey,
#             'data': data
#         }))
        
#         template.replace('\n','')
#         template.replace('\t','')
#         return HttpResponse(template)
#     @viewDec(authenticated=False,file=jsLibraryPath+'customer.js')
#     def customer(request,publicKey,gateWayID,template):
#         company=Company.get(publicAPIKey=publicKey)
#         if company is False:
#             return WrongResponse.get(context={'entityName': 'public API key'})
#         customer=CompanyCustomer.get(gateWayID=gateWayID,company=publicKey)
#         customer=json.dumps(customer.json())
#         template=template.render(DjangoContext({
#             'customer': customer
#         }))
#         template.replace('\n','')
#         template.replace('\t','')
#         return HttpResponse(template)
# @accessible.include()
# class StripeWebhookHandler(object):
#     @staticmethod
#     def getEvent(request):
#         endpoint_secret="whsec_jWJuihmZlcil14nwVE2M26ih90VCPSMJ"
#         payload=request.body
#         sig_header=request.META['HTTP_STRIPE_SIGNATURE']
#         event=None
#         try:
#             event=stripe.Webhook.construct_event(
#                 payload, sig_header, endpoint_secret
#             )
#         except ValueError as e:
#             return False
#         except stripe.error.SignatureVerificationError as e:
#             return False
#         return event
#     @classmethod
#     def handle(cls,request):
#         event=cls.getEvent(request=request)
#         if event is False: return HttpResponse(status=400)
#         user_id=event.user_id
#         company=Company.get(stripe_user_id=user_id)
#         if company is False: return HttpResponse(status=400)
#         if event.type=='customer.updated':
#             cls.customerUpdate(event=event,company=company)
#         return HttpResponse(status=200)
#     @staticmethod
#     def customerUpdate(event,company):
#         gateWayID=event.data.object.id
#         userID=event.user_id
#         customer=company.getCustomer(gateWayID=gateWayID)
#         customer.sync()
#     def customerCreated(event,company):
#         gateWayID=event.data.object.id
#         company.addCustomer(gateWayID=gateWayID)
#     def applyUsage(event,company):
#         gateWayID=event.data.object.customer
#         lines=event.data.object.lines.data
#         subscriptionIDs=[]
#         for subscription in lines:
#             subscriptionID=subscription.id
#             if subscriptionID[0:3]=='sub':
#                 company.apply(
#                     subscriptionID=subscriptionID,
#                     gateWayID=gateWayID
#                 )
