var SubFrog=(new function(){
    data=JSON.parse('{{data|safe}}');
    const endpoint='localhost:8000/api/jslibrary/';
    const publickey="{{PublicAPIKey}}";
    const cachedCustomers=[];
    const messages={
        customerDoesntExist: "You don't have a customer with the given gateway ID",
    };
    const helpers=(new function(){
        this.request=function(path){
            function RequestWrapper(){
                var endpoint="http://localhost:8000/api/js/"
                this.scriptTag=document.createElement("script");
                this.request=undefined;
                var thenFunctions=[];
                this.then=function(functionToRun){
                    thenFunctions.push(functionToRun);
                }
                var runThenFunctions=function(){
                    for(var counter in thenFunctions){
                        var func=thenFunctions[counter];
                        func();
                    }
                }
                this.scriptTag.onload=runThenFunctions;
                this.send=function(path){
                    this.scriptTag.src=endpoint+path+"&publickey="+publickey;
                    var header=document.getElementsByTagName("head")[0];
                    header.appendChild(this.scriptTag);
                }
            }
            var requestWrapper=new RequestWrapper();
            requestWrapper.send(path);
            return requestWrapper;
        };
        this.cost=function(units_arg,unitID_arg,plan_arg,quantity_arg,trial_arg){
            const [units,unitID,plan,quantity,trial]=[units_arg,unitID_arg,plan_arg,quantity_arg,trial_arg];
            const pricing=helpers.Retrievers.getPricing(unitID,plan,trial,units);
            if(pricing===null){return null;}
            var volumeCalculator=function(){
                if(quantity==0){return 0;}
                var rule=helpers.Retrievers.getRule(quantity,pricing);
                if(rule){
                    var cost=rule.cost;
                }
                else{
                    return 0;
                }
                if(rule.each==0){return 0;}
                return parseInt(parseInt(cost)*(parseInt(quantity)/parseInt(rule.each)));
            }
            var stairstepCalculator=function(){
                if(quantity==0) return 0;
                var cost=0;
                var quantityHolder=quantity;
                while(true){
                    var rule=helpers.Retrievers.getRule(quantityHolder,pricing);
                    if(rule===null){
                        break;
                    }
                    cost+=parseInt(parseInt((quantityHolder-rule.above+1))*parseInt(rule.cost));
                    quantityHolder=rule.above-1;
                }
                return cost
            }
            var packageCalculator=function(){
                var rule=helpers.Retrievers.getRule(quantity,pricing);
                if(!rule) return 0
                return rule.cost
            }
            var getCost=function(){
                if(typeof quantity=='undefined'){
                    return null;
                }
                if(pricing.model=='volume')
                    return volumeCalculator();
                if(pricing.model=='stairstep')
                    return stairstepCalculator();
                if(pricing.model=='package')
                    return packageCalculator();
                return null;
            }
            return getCost();
        }
        this.Retrievers=(new function(){
            this.getData=function(){
                if(typeof data==='undefined'){return null;}
                return data;
            };
            this.listUnits=function(){
                var data=this.getData()
                if(data===null){return null;}
                var units=data.units;
                if(typeof units==='undefined'){return null;}
                return units;
            };
            this.getUnit=function(unitID,units){
                if(typeof units=='undefined'){
                    units=this.listUnits();
                }

                if(units===null){return null;}
                for(var counter in units){
                    var unit=units[counter];
                    if(unit.id==unitID){return unit;}
                }
                return null;
            };
            this.listPricing=function(unitID,units){
                var unit=this.getUnit(unitID,units);
                if(unit===null){return null;}
                var pricing=unit.pricing;
                if(typeof pricing==='undefined'){return null;}
                return pricing;
            };
            this.getPricing=function(unitID,plan,trial,units){
                var pricingList=this.listPricing(unitID,units);
                if(pricingList==null){return null;}
                if(typeof plan=='undefined'){plan=null;}
                if(typeof trial=='undefined'||!trial){trial=null;}
                for(var counter in pricingList){
                    var pricing=pricingList[counter];
                    if(plan!=null && pricing.plan==plan){
                        return pricing;

                    }
                    if(trial!=null && pricing.trialMode){
                        return pricing
                    }
                }
                return null;
            }
            this.getRule=function(below,pricing){
                var lowest=null;
                for(var counter in pricing.rules){
                    var rule=pricing.rules[counter]
                    if(below >= rule.above){
                        if(lowest==null)
                            lowest=rule
                        else if(rule.above > lowest.above)
                            lowest=rule
                    }
                }
                return lowest;
            }
            this.getCustomer=function(gateWayID,callBack){
                for(var counter in cachedCustomers){
                    var customer=cachedCustomers[counter];
                    if(customer.gateWayID==gateWayID){
                        callBack(customer);
                        return;
                    }
                }
                helpers.request("customer?gatewayid="+gateWayID).then(function(){
                    SubFrog.customer(gateWayID,callBack);
                });
                // if(request===true){
                //     console.error(messages.customerDoesntExist);
                //     callBack(null);
                //     return;
                // }
            }
            this.getQuantity=function(units,unitID){
                var unit=helpers.Retrievers.getUnit(unitID,units);
                if(unit==null){return null;}
                var quantity=unit.quantity;
                if(typeof quantity=='undefined'){return null;}
                var quantityValue=quantity.value;
                if(typeof quantityValue=='undefined'){return null;}
                return quantityValue;
            }
        });
    }); 
    this.addToCache=function(customer){
        function customerTemplate(customer){
            console.log("loaded customer: ", customer)
            this.gateWayID=customer.gateWayID;
            const template=customer.template;
            const pricingList=customer.pricing;
            console.log('template',template);
            this.outstanding=function(unitID){
                var unitPricing=null;
                for(index in pricingList){
                    var pricing=pricingList[index];
                    if(pricing.unitID==unitID){
                        unitPricing=pricing;
                        break;
                    }
                }
                if(unitPricing===null){
                    console.error("Unit doesn't exist");
                    return null;
                }
                var units=template.units;
                var plan=unitPricing.pricing.plan;
                var trial=unitPricing.pricing.trialMode;
                var quantity=helpers.Retrievers.getQuantity(units,unitID);
                var total=helpers.cost(units,unitID,plan,quantity,trial);
                return new (function(total,quantity){
                    this.quantity=quantity;
                    this.total=total;
                    if(quantity==0){
                        this.rate=0;
                    }else{
                        this.rate=this.total/quantity;
                    }
                })(total,quantity);
            }

        }
        cachedCustomers.push(new customerTemplate(customer));
    }
    this.customer=function(gateWayID,callBack){
        helpers.Retrievers.getCustomer(gateWayID,callBack);
    };
    this.pricing=function(unitID,plan,quantity,trial){
        var units=data.units;
        return helpers.cost(units,unitID,plan,quantity,trial);
    };

});
console.log(SubFrog.customer("cus_8vP146gFe80t11",function(customer){
    console.log('customer: ',customer);
    console.log('outstanding', customer.outstanding("mama"));
}));
// console.log(SubFrog.customer());
// SubFrog.initialize();
// SubFrog.pricing()

// var SubFrog={
//     key: undefined,
//     start: function(key){
//         this.key=key;
//     }
//     customer: function(customer,callBack){
//         var template=function(customer){
//             var pricing=function(pricing){
//                 var rule=function(rule){
//                     this.object='rule';
//                     this.above=rule.above;
//                     this.each=rule.each;
//                     this.cost=rule.cost;
//                 }
//                 this.object='pricing';
//                 this.model=pricing.model;
//                 this.rules=[];
//                 this.current=pricing.current;
//                 this.next=pricing.next;
//                 for(var oneRule in pricing.rules){
//                     this.rules.push(new rule(oneRule));
//                 }
//             }
//             pricing.prototype.costOf=function(unit,plan,quantity){}
//             var thisPeriod=function(thisPeriod){
//                 this.object='thisPeriod';
//                 this.unit=thisPeriod;
//                 this.quantity=thisPeriod;
//                 this.total=thisPeriod;
//             }
//             this.object='customer';
//             this.currency=customer.currency;
//             this.thisPeriod=new thisPeriod(customer.thisPeriod);
//             this.pricing=new pricing(customer.pricing);    
//         }
//         var result=new template(data);
//         if(typeof callBack != 'undefined'){callBack(result);}
//     },
//     pricing: function(){
//         var rule=function(rule){
//             this.object='rule';
//             this.above=rule.above;
//             this.each=rule.each;
//             this.cost=rule.cost;
//         }
//         this.object='pricing';
//         this.model=pricing.model;
//         this.rules=[];
//         for(var oneRule in pricing.rules){
//             this.rules.push(new rule(oneRule));
//         }
//     },
// };
// SubFrog.pricing.prototype.costOf=function(unit,plan,quantity){}
