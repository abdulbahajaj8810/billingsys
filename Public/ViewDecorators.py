import json,inspect
from models.Company import Company
from models.Member import Member
from models.Customer import CompanyCustomer,HistoryLogger
from models.Logger import Logger
from django.http import HttpResponse
from django.conf.urls import url
from django.core.handlers.wsgi import WSGIRequest
from django.template import Context as DjangoContext, Template as DjangoTemplate
from Public.Responses import *
from django.shortcuts import redirect

def viewDec( method=None,#Dictates the method that the view must be accessed with
        authenticated=True,#Dictates that the access to the api requires authentication
        APIAccessible=False,#dictates whether or not a view can be accessed through the API
        logIO=False, #dictates whether or not the request/response should be logged
        file=None #gives the view access to a file
    ):
    def decorator(func):
        class Retrievers(object):
            @classmethod
            def get(cls,request):
                return cls.Retriever(request=request)
            class RetrievedTree(object):
                def __init__(self,):
                    self.elements={}
                def append(self,name,obj):
                    self.elements[name]=obj
                def get(self,name):
                    return self.elements.get(name,None)
                def toDict(self):
                    return self.elements
            class Retriever(object):
                class CacheTemplate(object):
                    def __init__(self):
                        self.cachedObjects={}
                    def add(self,name,obj):
                        self.cachedObjects[name]=obj
                    def get(self,name):
                        return self.cachedObjects.get(name,None)
                    def getAll(self):
                        return self.cachedObjects
                def __init__(self,request):
                    self.parent=Retrievers
                    self.request=request
                    self.inputHolder=getInputHolder(request=request)
                    self.member=None
                    self.member=self.getMember()
                    self.company=None
                    self.company=self.getCompany()
                    self.map={}
                    self.map['member']=self.getMember
                    self.map['company']=self.getCompany
                    self.map['webhookManager']=self.getWebhookManager
                    self.map['unitManager']=self.getUnitManager
                    self.map['unit']=self.getUnit
                    self.map['gateWayConnector']=self.getGateWayConnector
                    self.map['customer']=self.getCustomer
                    self.map['customerList']=self.listCustomers
                    self.map['logsList']=self.listLogs
                    self.map['historyList']=self.listHistory
                    self.cache=self.CacheTemplate()
                #Document
                def getMember(self):
                    if self.member is not None: return self.member
                    if not self.request.user.is_authenticated():
                        member=None
                    else:
                        username=self.request.user.username
                        member=Member.getByID(id=username)
                    return member
                #Document
                def getCompany(self):
                    self.accessThrough=None
                    if self.member is not None:
                        company=self.member.company
                        self.accessThrough='member'
                        return company
                    if not (APIAccessible is True): return None
                    APIKey=self.inputHolder.get('APIKey',None)
                    if APIKey is None: return None
                    company=Company.objects(APIKey=APIKey)
                    if len(company)!=1:
                        return None
                    company=company[0]
                    if not company.authenticate(APIKey=APIKey): return None
                    self.accessThrough='api'
                    return company
                #Embedded
                def getUnitManager(self):
                    company=self.getCompany()
                    if company is None: return None
                    unitManager=company.getDefaultPricing()
                    retrievedTree=self.parent.RetrievedTree()
                    retrievedTree.append(name='unitManager',obj=unitManager)
                    return retrievedTree
                #Embedded
                def getUnit(self):
                    id=self.inputHolder.get('unitID',None)
                    if id is None: return None
                    retrievedTree=self.getUnitManager()
                    unitManager=retrievedTree.get(name='unitManager')
                    if unitManager is None: return None
                    unit=unitManager.get(id=id)
                    retrievedTree.append(name='unit',obj=unit)
                    return retrievedTree
                #Document
                def getWebhookManager(self):
                    unit=self.getUnit()
                    if unit is None: return None
                    webhookManager=unit.getWebhookManager()
                    retrievedTree=self.parent.RetrievedTree()
                    retrievedTree.append(name='webhookManager',obj=webhookManager)
                    retrievedTree.append(name='unit',obj=unit)
                    return retrievedTree
                #Document
                def getGateWayConnector(self):
                    company=self.company
                    if company is None: return None
                    gateWayConnector=company.GateWayConnector
                    gateWayConnector.initialize()
                    return gateWayConnector
                def listLogs(self):
                    step=self.inputHolder.get('listStep',None)
                    if step is None: step=0
                    company=self.company
                    if company is None: return None

                    customer=self.getCustomer()
                    all=False
                    if customer is not None:
                        all=True
                    logs=Logger.list(
                        company=company,
                        step=step,
                        customer=customer,
                        all=all)
                    return logs
                def listHistory(self):
                    customer=self.getCustomer()
                    company=None
                    if customer is None:
                        company=self.company
                        if company is None: return None
                    history=HistoryLogger.list(customer=customer,company=company)
                    if history is None: return {}
                    return history
                #Document
                def getCustomer(self):
                    customer=self.cache.get('customer') 
                    if customer is not None: return customer
                    gateWayID=self.inputHolder.get('gateWayID',None)
                    if gateWayID is None: return None
                    company=self.company
                    if company is None: return None
                    customer=company.getCustomer(gateWayID=gateWayID)
                    if customer is None: return None
                    return customer
                def listCustomers(self):
                    step=self.inputHolder.get('listStep',None)
                    if step is None: return None
                    step=int(step)
                    company=self.company
                    if company is None: return None
                    list=company.listCustomers(step=step)
                    return list
                def retrieve(self,name):
                    retriever=self.map.get(name,None)
                    if retriever is None:
                        return None
                    retrievedValue=retriever()
                    self.cache.add(name,retrievedValue)
                    return retrievedValue
        def log(inputs,output,retriever,request):
            if logIO is not True: return
            inputs=toDict(inputs)
            del inputs['APIKey']
            outputClone=toDict(output)
            outputClone=json.loads(outputClone)
            Logger.create(
                retriever=retriever,
                request=inputs,
                response=outputClone,
                endpoint=buildPath(request=request)
            )
        def getExpectedInputs(request,retriever,cls=None,self=None):
            expectedInputsNames=getExpectedInputsNames()
            DBInputs=retrieveFromDB(
                retriever=retriever,
                request=request,
                authenticated=authenticated,
                expectedInputsNames=expectedInputsNames
            )
            inputHolderInputs=retrieveFromInputHolder(
                request=request,
                expectedInputsNames=expectedInputsNames
            )
            expectedInputs=combineInputs(
                dominant=DBInputs,
                inferior=inputHolderInputs,
                expectedInputsNames=expectedInputsNames
            )
            if file is not None:
                getFile(
                    expectedInputs=expectedInputs,
                    expectedInputsNames=expectedInputsNames
                )
            if 'request' in expectedInputsNames:
                expectedInputs['request']=request
            if 'cls' in expectedInputsNames:
                expectedInputs['cls']=cls
            elif 'self' in expectedInputsNames:
                expectedInputs['self']=self
            return expectedInputs
        def getFile(expectedInputs,expectedInputsNames):
            with open(file) as fileContent:
                fileContent=fileContent.read()
            if 'file' in expectedInputsNames:
                expectedInputs['file']=fileContent
            elif 'template' in expectedInputsNames:
                fileContent=DjangoTemplate(fileContent)
                expectedInputs['template']=fileContent
        def combineInputs(dominant,inferior,expectedInputsNames):
            if len(dominant) == 0: return inferior
            if len(inferior) == 0: return dominant
            expectedInputs={}
            for name in expectedInputsNames:
                dominantValue=dominant.get(name,None)
                if dominantValue is not None:
                    expectedInputs[name]=dominantValue
                    continue
                inferiorValue=inferior.get(name,None)
                if inferiorValue is not None:
                    expectedInputs[name]=inferiorValue
            return expectedInputs
        def retrieveFromDB(request,authenticated,expectedInputsNames,retriever):
            if authenticated is False and APIAccessible is not True: return {}
            expectedInputs={}
            for name in expectedInputsNames:
                if expectedInputs.get(name,None) is not None: continue
                value=retriever.retrieve(name=name)
                if isinstance(value,Retrievers.RetrievedTree):
                    value=value.toDict()
                    for name in value:
                        expectedInputs[name]=value[name]
                elif value is not None:
                    expectedInputs[name]=value
            return expectedInputs
        def retrieveFromInputHolder(request,expectedInputsNames):
            inputHolder=getInputHolder(request=request)
            expectedInputs={}
            for name in expectedInputsNames:
                value=inputHolder.get(name,None)
                if value is not None:
                    try:
                        value=json.loads(value)
                    except: pass
                    expectedInputs[name]=value
            if 'GET' in expectedInputsNames:
                expectedInputs['GET']=request.GET
            if 'POST' in expectedInputsNames:
                expectedInputs['POST']=request.POST
            return expectedInputs
        def getInputHolder(request):
            inputHolder={}
            method=request.method.lower()
            if method=='get':
                inputHolder=request.GET
            elif method=='post':
                inputHolder=request.POST
            if len(inputHolder) == 0:
                inputHolder=request.body
                if len(inputHolder) > 0:
                    inputHolder=json.loads(inputHolder)
                else:
                    inputHolder={}
            return inputHolder
        def getExpectedInputsNames():
            return inspect.getargspec(func).args
        def buildPath(request):
            return request.META.get('PATH_INFO')
        def toDict(inputHolder):
            response={}
            if isinstance(inputHolder,HttpResponse):
                for name in inputHolder:
                    return name
            for name in inputHolder:
                if not isinstance(name,str) and not isinstance(name,unicode):
                    continue
                value=inputHolder.get(name,None)
                if value is None: continue
                response[name]=value
            return response
        def addReleventInfo(output,retriever):
            if(isinstance(output,HttpResponse)):
                return output
            if(isinstance(output,dict)):
                if output.get('includeCompanyInfo',False):
                    company=retriever.company
                    if company is not None:
                        output['company']=company.json()
                        apiCalls=Logger.list(company=company,json=True)[0]
                        customers=company.listCustomers(json=True)[0]
                        output['company']['customers']=customers
                        output['company']['apiCalls']=apiCalls
                        output['company']['history']=HistoryLogger.list(company=company)

                try:
                    del output['includeCompanyInfo']
                except: pass
            return output
        def getOutput(request,cls=None,self=None):
            inputs=getInputHolder(request=request)
            retriever=Retrievers.get(request=request)
            if authenticated is not False:
                if retriever.accessThrough is None: DontHavePermissionResponse.get()
                if retriever.accessThrough=='member':
                    member=retriever.member
                    if member is None: return DontHavePermissionResponse.get()
                elif retriever.accessThrough=='api':
                    company=retriever.company
                    if company is None:
                        return DontHavePermissionResponse.get()
                else:
                    return UnAuthenticatedRequestResponse.get()
            expectedInputs=getExpectedInputs(
                retriever=retriever,
                request=request,
                cls=cls,
                self=self,
            )
            try:
                pass
            except TypeError:
                output=MissingArgumentsResponse.get()
            print 'expectedInputs: ',expectedInputs
            output=func(**expectedInputs)
            log(inputs=inputs,output=output,retriever=retriever,request=request)
            print 'type of output: ', output
            output=addReleventInfo(output=output,retriever=retriever)
            return output
        def translateArgs(args):
            result={}
            firstElement=args[0]
            if isinstance(firstElement,WSGIRequest):
                result['request']=firstElement
            else:
                if isinstance(firstElement,type):
                    result['cls']=firstElement
                else:
                    result['self']=firstElement
                for element in args:
                    if isinstance(element,WSGIRequest):
                        result['request']=element
            return result
        def makeHttp(output):
            redirectPath=False
            if type(output) is dict:
                redirectPath=output.get('redirect',False)
                try:
                    del output['redirect']
                except: pass
                if not redirectPath:
                    output=json.dumps(output)

            if redirectPath:
                redirectPath+='?'
                for name in output:
                    if name=='company': continue
                    value=output[name]
                    if type(value) is dict:
                        value=json.dumps(value)
                    elif type(value) is int:
                        value=str(value)
                    elif type(value) is bool:
                        continue
                    redirectPath+=name+'='+value+'&'
                redirectPath=redirectPath[:-1]
                output=redirect(redirectPath,lala='whowho')
            elif type(output) is not HttpResponse:
                output=HttpResponse(output)
            return output
        def removeIrreleventInfo(output):            
            try:
                del output['includeCompanyInfo']
            except: pass
            return output
        def wrapper(*args,**kwargs):
            byPass=kwargs.get('byPass',False)
            output=None
            if byPass is True:
                isHttp=kwargs.get('isHttp',False)
                del kwargs['byPass']
                if kwargs.get('excludeArgs',False) is True:
                    del kwargs['excludeArgs']
                    output=func(**kwargs)
                else:
                    output=func(*args,**kwargs)
                if isHttp:
                    del kwargs['isHttp']
                    output=makeHttp(output)
            else:
                args=translateArgs(args=args)
                request=args.get('request',None)
                if method is not None:
                    if not (request.method.upper()==method.upper()):
                            context={}
                            context['wrongMethod']=request.method.upper()
                            context['properMethod']=method.upper()
                            output=WrongRequestMethodResponse.get(context=context)
                if output is None:
                    cls=args.get('cls',None)
                    self=args.get('self',None)
                    output=getOutput(request=request,cls=cls,self=self)

                output=removeIrreleventInfo(output=output)
                output=makeHttp(output=output)
            return output
        wrapper.__name__=func.__name__
        return wrapper
    return decorator














class accessible(object):
    urlPatterns=[]
    @staticmethod
    def isFunction(obj):
        if isinstance(obj,classmethod):
            return True
        if isinstance(obj,staticmethod):
            return True
        if hasattr(obj,'__call__'):
            return True
        return False
    @classmethod
    def append(cls,pattern,functionObj,path):
        # functionObj.endpointPath=path
        urlObj=url(pattern,functionObj)
        cls.urlPatterns.append(urlObj)
    @classmethod
    def addClass(cls,classObj,api,className=None):
        if className is None:
            className=classObj.__name__.lower()
        elements=classObj.__dict__
        for functionName in elements:
            functionName=functionName
            functionObj=getattr(classObj,functionName)
            functionName=functionName.lower()
            if not cls.isFunction(functionObj): continue
            if api is True:
                path='api/'+className+'/'+functionName
            else:
                path=className+'/'+functionName
            pattern=r'^'+path+'$'
            cls.append(pattern=pattern,functionObj=functionObj,path=path)
    @classmethod
    def addFunction(cls,functionObj,api,pattern=None):
        functionName=functionObj.__name__
        functionName=functionName.lower()
        if api is True:
            path='api/'+functionName
        else:
            path=functionName
        if pattern is None:
            pattern=r'^'+path+'$'
        cls.append(pattern=pattern,functionObj=functionObj,path=path)
    @classmethod
    def include(cls,className=None,pattern=None,api=True):
        def dec(obj):
            if isinstance(obj,type):
                cls.addClass(classObj=obj,className=className,api=api)
            elif cls.isFunction(obj):
                cls.addFunction(functionObj=obj,pattern=pattern,api=api)
            return obj
        return dec

