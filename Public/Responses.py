import inspect,json
from django.http import HttpResponse
class BaseResponse(object):
    status=None
    code=None
    message=None
    includeCompanyInfo=False
    @classmethod
    def getMessage(cls,context):
        message=cls.message
        for varName in context:
            varValue=context[varName]
            if isinstance(varValue,list):
                varValue=','.join(varValue)
            message=message.replace('{'+varName+'}',varValue)
        return message
    @classmethod
    def construct(cls,context={}):
        return {
            'message': cls.getMessage(context=context),
            'code': cls.code,
            'status': cls.status
        }
    @classmethod
    def get(cls,context={},isHttpResponse=False,redirect=False):
        response=cls.construct(context=context)
        # response=json.dumps(response)
        status=200
        if cls.status=='failed': status=400
        # if (not cls.includeCompanyInfo or isHttpResponse) and not redirect:
        #     return HttpResponse(response,status=status)
        return {
            'response': response,
            'status': status,
            'includeCompanyInfo': cls.includeCompanyInfo,
            'redirect': redirect,
        }
class CustomSuccessResponse(BaseResponse):
    status='success'
    code='SUCCESS'
    message="{custom}"
class MissingArgumentsResponse(BaseResponse):
    status='failed'
    code='MissingArguments'
    message='One or more argument(s) are missing'
class SpecificMissingAruumentResponse(BaseResponse):
    status='failed'
    code='MissingArguments'
    message='Argument {argument} is missing'
class UnAuthenticatedRequestResponse(BaseResponse):
    status='failed'
    code='UnAuthenticatedRequest'
    message='You have to be authenticated to perform this request'
class WrongRequestMethodResponse(BaseResponse):
    status='failed'
    code='WrongRequestMethod'
    message='This request has to be sent using {properMethod}. However, it is sent using {wrongMethod}'
class NotFoundResponse(BaseResponse):
    status='failed'
    code='NotFound'
    message='{EntityName} is not found'
class DoNotMatchResponse(BaseResponse):
    status='failed'
    code='DoNotMatch'
    message="{EntitiesName} Didn't match"
class SuccessfulRequestResponse(BaseResponse):
    status='success'
    code='SuccessfulRequest'
    message="Your request has been successful"
    includeCompanyInfo=True
class AlreadyExistResponse(BaseResponse):
    status='failed'
    code='AlreadyExist'
    message="{EntityName} already exist"
class DoesntExistResponse(BaseResponse):
    status='failed'
    code='DoesntExist'
    message="{entityName} doesn't exist"
class AnErrorOccuredResponse(BaseResponse):
    status='failed'
    code='AnErrorOccured'
    message='An unexpected error has occured. Our team has been notified and is working on it!'
class InfoDidntMatchRecordResponse(BaseResponse):
    status='failed'
    code='AnErrorOccured'
    message="The entered {entity} didn't match our records"
class AlreadyLoggedInResponse(BaseResponse):
    status='failed'
    code='YouAreAlreadyLoggedIn'
    message='You are already logged in. logout out first in order to log in as another user'
class NotAllowedResponse(BaseResponse):
    status='failed'
    code='NotAllowed'
    message='The entered {enteredEntity} is not allowed. The allowed {enteredEntity}s are {allowedEntities}'
class WrongResponse(BaseResponse):
    status='failed'
    code='Wrong'
    message='Wrong {entityName}'
class ExpiredResponse(BaseResponse):
    status='failed'
    code='Expired'
    message='{entityName} is expired'
class CustomResponse(BaseResponse):
    status='failed'
    code='Error'
    message='{message}'
class DontHavePermissionResponse(BaseResponse):
    status='failed'
    code='NoPermission'
    message="You don't have the permission to perform this action"
class NotValidResponse(BaseResponse):
    status='failed'
    code='NotValid'
    message="Not valid {entityName}"
class ProcessingRequest(BaseResponse):
    status='queued'
    code='Processing'
    message="You request has been queued and is being processed"
