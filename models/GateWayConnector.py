from mongoengine import *
import stripe,requests,uuid,math
from models.StripeConnector import StripeConnector,StripeCustomer
import json
class CustomerManager(object):
    company=None
    type=None
    def __init__(self,company,type=None):
        self.type=type
        self.company=company
        if self.type=='stripe':
            self.path=StripeCustomer
    def create(self,id):
        customer=self.path.create(id=id,company=self.company)
        customer.initialize(root=self.company.GateWayConnector.getConnector())
        customer.update()
        return customer
    def get(self,id):
        gateWayConnector=self.company.GateWayConnector.getConnector()
        customer=self.path.getOrCreate(gateWayID=id,
            company=self.company,
            gateWayConnector=gateWayConnector)
        if customer is None: return customer
        return customer
    def list(self):
        return self.path.list(company=self.company)
    def updateAll(self):
        self.path.updateAll(company=self.company)
class Credentials(object):
    __Connector=False
    @classmethod
    def create(cls):
        return cls()
    def __init__(self,connector):
        self.__Connector=connector
    def buildLink(self):
        return self.__Connector.Credentials.buildLink()
    def authorize(self,GETParameters,company):
        return self.__Connector.Credentials.authorize(
            GETParameters=GETParameters,
            company=company,
        )
class PlanManager(object):
    __Connector=None
    def __init__(self,connector):
        self.__Connector=connector
    @classmethod
    def create(cls):
        return cls()
    def update(self):
        self.__Connector.PlanManager.update()
    def json(self):
        return self.__Connector.PlanManager.json()
    def list(self):
        return self.__Connector.PlanManager.list()
    def add(self,id,name,amount,interval,interval_count=1,trial_period_days=0):
        return self.__Connector.PlanManager.add(
            id=id,
            name=name,
            amount=amount,
            interval=interval,
            interval_count=interval_count,
            trial_period_days=trial_period_days
        )
class GateWayConnector(DynamicEmbeddedDocument):
    stripeGateWayConnector=EmbeddedDocumentField('StripeConnector')
    PlanManager=None
    Credentials=None
    CustomerManager=CustomerManager
    connectorMapper={}
    connectorMapper['stripe']=StripeConnector
    def __repr__(self):
        return json.dumps(self.json())
    @classmethod
    def create(cls,connector,currency):
        gateWayConnector=cls()
        if not gateWayConnector.setConnector(
            type='stripe',
            currency=currency
        ): return False
        # gateWayConnector.Credentials=Credentials.create()
        # gateWayConnector.PlanManager=PlanManager.create()

        return gateWayConnector
    def initialize(self):
        connector=self.getConnector()
        self.PlanManager=PlanManager(connector=connector)
        self.Credentials=Credentials(connector=connector)
    def getConnector(self):
        if self.stripeGateWayConnector is not None:
            self.stripeGateWayConnector.initialize()
            return self.stripeGateWayConnector
        return False
    def setConnector(self,type,currency):
        connector=self.connectorMapper.get(type,None)
        if connector is None: return None
        connector=connector.create(
            currency=currency
        )
        if type == 'stripe':            
            self.stripeGateWayConnector=connector
            return True
        return None
    def isConnected(self):
        connector=self.getConnector()
        return connector.isConnected()
    def setCurrency(self,currency):
        connector=self.getConnector()
        if connector is False or connector is None:
            return
        connector.setCurrency(currency=currency)
    def getCurrency(self):
        connector=self.getConnector()
        if connector is False or connector is None:
            return
        return connector.getCurrency()
    def update(self):
        connector=self.getConnector()
        if connector is False or connector is None:
            return
        connector.update()
    def json(self):     
        connector=self.getConnector()
        if connector is False or connector is None:
            return {}
        connector.update(plan=True)
        return connector.json()
    def setPreChargeAmount(self,preChargeAmount):
        connector=self.getConnector()
        if connector is False or connector is None:
            return
        return connector.setPreChargeAmount(preChargeAmount=preChargeAmount)
    def getPreChargeAmount(self):
        connector=self.getConnector()
        if connector is False or connector is None:
            return 0
        return connector.getPreChargeAmount()
# from models.Customer import CompanyCustomer

