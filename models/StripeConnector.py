import stripe,requests,uuid,math
from mongoengine import *

class ErrorObject(Exception):
    type=None
    __allowedErrors={
        'FailedAuth':[
            "Auth has been denied",
            "Auth request doesn't exist",
            "Read and write scope wasn't granted",
            "Couldn't connect to stripe",
            "Account is not connected to stripe",
        ],
        "DeletingError":[
            "Plan doesn't exist"
        ],
        "MissingResources":[
            "Subscription not found"
        ],
        "paymentFailure":[
            "Can't pay an upcoming invoice",
            "There are no items to pay for"
        ]
    }
    def __init__(self,err=None,message=None,type=None):
        if err:
            message=err.message
            try:
                type=err.type
            except:
                type=None
        else:
            if not message or not type:
                raise Exception("Failed to build error. Message or type is missing")
            if not self.__allowedErrors.get(type,False):
                raise Exception("Error type is not allowed. Given: "+type)
            if not message in self.__allowedErrors.get(type,[]):
                raise Exception("Error message is not allowed. Given: "+message)
        super(ErrorObject, self).__init__(message)
        self.type=type

class Plan(DynamicEmbeddedDocument):
    id=StringField()
    name=StringField()
    amount=IntField()
    currency=StringField()
    interval=StringField()
    interval_count=IntField()
    trial_period_days=IntField()
    @classmethod
    def create(cls,stripeObj):
        plan=cls(stripeObj=stripeObj)
        plan.id=stripeObj.id
        plan.name=stripeObj.name
        plan.amount=stripeObj.amount
        plan.currency=stripeObj.currency
        plan.interval=stripeObj.interval
        plan.interval_count=stripeObj.interval_count
        plan.trial_period_days=stripeObj.trial_period_days
        return plan
    def json(self):
        response={}
        response['id']=self.id
        response['name']=self.name
        response['amount']=self.amount
        response['currency']=self.currency
        response['interval']=self.interval
        response['interval_count']=self.interval_count
        response['trial_period_days']=self.trial_period_days
        return response
class PlanManager(DynamicEmbeddedDocument):
    __root=None
    __plans=EmbeddedDocumentListField('Plan')
    def initialize(self,root):
        self.__root=root
    @classmethod
    def create(cls):
        return cls()
    def add(self,id,name,amount,interval,interval_count=1,trial_period_days=0):
        try:
            stripePlan=stripe.Plan.create(
                id=id,name=name,amount=amount,currency=self.__root.getCurrency(),
                interval=interval,interval_count=interval_count,trial_period_days=trial_period_days,
                stripe_account=self.__root.getStripe_user_id()
            )
        except Exception as err:
            raise ErrorObject(err=err)
        plan=Plan.create(stripePlan)
        self.__plans.append(plan)
        return plan
    def list(self):
        return self.__plans
    def update(self,nextID=None):
        if self.__root.getStripe_user_id() is None:
            self.__plans=[]
            return
        try:
            if nextID:
                response=stripe.Plan.list(
                    limit=100,
                    starting_after=nextID,
                    stripe_account=self.__root.getStripe_user_id()
                )
            else:
                self.__plans=[]
                response=stripe.Plan.list(
                    limit=100,
                    stripe_account=self.__root.getStripe_user_id()
                )
        except Exception as err:
            raise ErrorObject(err=err)
        stripePlans=response.data
        for stripePlan in stripePlans:
            plan=Plan.create(stripeObj=stripePlan)
            self.__plans.append(plan)
        if response.has_more:
            nextID=stripePlans[len(stripePlans)-1].id
            del stripePlans
            self.update(nextID=nextID)
    def json(self):
        response=[]
        for plan in self.__plans:
            response.append(plan.json())
        return response
class Credentials(DynamicEmbeddedDocument):
    __root=None
    __states=ListField(StringField())
    @classmethod
    def create(cls):
        return cls()
    def initialize(self,root):
        self.__root=root
    def buildLink(self):
        state=uuid.uuid4().hex
        link="https://connect.stripe.com/oauth/authorize?response_type=code"
        link+="&client_id="+self.__root.getClientID()
        link+="&scope=read_write"
        link+="&state="+state
        self.__states.append(state)
        return link
    def verifyState(self,state):
        for index in range(0,len(self.__states)):
            if self.__states[index]==state:
                del self.__states[index]
                return True
        return False
    def authorize(self,GETParameters,company):
        if GETParameters.get('error',False)=='access_denied':
            raise ErrorObject(type='FailedAuth',message="Stripe auth has been denied")
        state=GETParameters.get('state')
        if not self.verifyState(state=state):
            raise ErrorObject(type="FailedAuth",message="Auth request doesn't exist")
        authCode=GETParameters.get('code')
        endpoint='https://connect.stripe.com/oauth/token'
        data={'client_secret':self.__root.getSecretKey(),'code':authCode,'grant_type':'authorization_code'}
        response=requests.post(url=endpoint,data=data)
        response=response.json()
        if not (response.get('scope','')=='read_write'):
            raise ErrorObject(type="FailedAuth",message="Read and write scope wasn't granted")
        stripe_user_id=response.get('stripe_user_id',False)
        refresh_token=response.get('refresh_token',False)
        if not stripe_user_id or not refresh_token:
            raise ErrorObject(type="FailedAuth",message="Couldn't connect to stripe")
        self.__root.setStripe_user_id(stripe_user_id=stripe_user_id)
        self.__root.setrefresh_token(refresh_token=refresh_token)
        from models.Customer import CompanyCustomer
        CompanyCustomer.syncAll(company=company)
        return {"success":"connection has been established"}

#customer
# class Credit(DynamicEmbeddedDocument):
#     __Customer=None
#     __root=None
#     __credit=IntField()
#     @classmethod
#     def create(cls):
#         return cls()
#     def update(self,credit):
#         self.__credit=credit*-1
#     def initialize(self,customer,root):
#         self.__Customer=customer
#         self.__root=root
#     #Resource intensive
#     def add(self,amount,charge=True):
#         customer=self.__Customer.getStripeObject()
#         try:
#             if charge:
#                 self.__Customer.charge(amount=amount,useCredit=False)
#             customer.account_balance-=amount
#             self.__credit+=amount
#             customer.save()
#             self.__Customer.save()
#         except Exception as err:
#             raise ErrorObject(err=err)
#     #Resource intensive
#     def use(self,amount):
#         customer=self.__Customer.getStripeObject()
#         customer.account_balance+=amount
#         self.__credit-=amount
#         try:
#             customer.save()
#             self.__Customer.save()
#         except Exception as err:
#             raise ErrorObject(err=err)
#     def get(self):
#         return self.__credit
#     def precharge(self,chargingAmount):
#         diff=self.__credit-chargingAmount
#         if diff > 0: return None
#         defaultPrecharge=self.__root.getPreChargeAmount()
#         if(defaultPrecharge==0): return None
#         defaultPrecharge=float(defaultPrecharge)
#         diff=-1*diff
#         diff=float(diff)
#         multiplyBy=diff/defaultPrecharge
#         multiplyBy=math.ceil(multiplyBy)
#         toPreCharge=float(multiplyBy*defaultPrecharge)
#         self.add(amount=toPreCharge)
class CustomerPlan(DynamicEmbeddedDocument):
    _id=StringField()
    name=StringField()
    amount=IntField()
    currency=StringField()
    interval=StringField()
    interval_count=IntField()
    trial_period_days=IntField()
    quantity=IntField()
    @classmethod
    def create(cls,stripeObj,quantity=1):
        plan=cls()
        plan._id=stripeObj.id
        plan.name=stripeObj.name
        plan.amount=stripeObj.amount
        plan.currency=stripeObj.currency
        plan.interval=stripeObj.interval
        plan.interval_count=stripeObj.interval_count
        plan.trial_period_days=stripeObj.trial_period_days
        plan.quantity=quantity
        return plan
    def json(self):
        response={}
        response['id']=self._id
        response['name']=self.name
        response['amount']=self.amount
        response['currency']=self.currency
        response['interval']=self.interval
        response['interval_count']=self.interval_count
        response['trial_period_days']=self.trial_period_days
        response['quantity']=self.quantity
        return response
class CustomerPlanManager(DynamicEmbeddedDocument):
    __plans=EmbeddedDocumentListField('CustomerPlan')
    @classmethod
    def create(cls):
        return cls()
    def update(self,stripeSubscription):
        if stripeSubscription.plan is not None:
            self.__add(stripePlan=stripeSubscription.plan)
        else:
            for item in stripeSubscription['items'].data:
                self.__add(stripePlan=item.plan,quantity=item.quantity)
    def __add(self,stripePlan,quantity=1):
        plan=CustomerPlan.create(stripeObj=stripePlan,quantity=quantity)
        self.__plans.append(plan)
        return plan
    def list(self):
        return self.__plans
    def get(self,id):
        for plan in self.__plans:
            if plan._id==id:
                return plan
        return False
    def json(self):
        response=[]
        for plan in self.__plans:
            response.append(plan.json())
        return response
class Subscription(DynamicEmbeddedDocument):
    __root=None
    __Customer=None
    _id=StringField()
    cancel_at_period_end=IntField()
    canceled_at=IntField()
    created=IntField()
    current_period_end=IntField()
    current_period_start=IntField()
    ended_at=IntField()
    status=StringField()
    trial_end=IntField()
    trial_start=IntField()
    PlanManager=EmbeddedDocumentField('CustomerPlanManager')
    stripeObj=None
    def getID(self):
        return self._id
    def isOnTrial(self):
        if self.status=='trialing':
            return True
        return False
    def initialize(self,root,customer):
        self.__root=root
        self.__Customer=customer
    #Resource intensive
    def getStripeObj(self):
        return stripe.Subscription.retrieve(
            id=self._id,
            stripe_account=self.__root.getStripe_user_id()
        )
    @classmethod
    def create(cls,stripeSubscription):
        subscription=cls()
        subscription.stripeObj=stripeSubscription
        subscription._id=stripeSubscription.id
        subscription.cancel_at_period_end=stripeSubscription.cancel_at_period_end
        subscription.canceled_at=stripeSubscription.canceled_at
        subscription.created=stripeSubscription.created
        subscription.current_period_end=stripeSubscription.current_period_end
        subscription.current_period_start=stripeSubscription.current_period_start
        subscription.ended_at=stripeSubscription.ended_at
        subscription.status=stripeSubscription.status
        subscription.trial_end=stripeSubscription.trial_end
        subscription.trial_start=stripeSubscription.trial_start
        subscription.PlanManager=CustomerPlanManager.create()
        subscription.PlanManager.update(stripeSubscription=stripeSubscription)
        return subscription
    def has(self,id):
        if self.PlanManager.get(id=id):
            return True
        return False
    #Resource intensive
    def addItem(self,amount):
        try:
            stripe.InvoiceItem.create(
                customer=self.__Customer.gateWayID,
                amount=amount,
                currency=self.__root.getConnector().getCurrency(),
                subscription=self._id,
                stripe_account=self.__root.getConnector().getStripe_user_id()
            )
        except Exception as err:
            raise ErrorObject(err=err)
    def json(self):
        response={}
        response['id']=self._id
        response['cancel_at_period_end']=self.cancel_at_period_end
        response['canceled_at']=self.canceled_at
        response['created']=self.created
        response['current_period_end']=self.current_period_end
        response['current_period_start']=self.current_period_start
        response['ended_at']=self.ended_at
        response['status']=self.status
        response['trial_end']=self.trial_end
        response['trial_start']=self.trial_start
        response['plans']=self.PlanManager.json()
        return response
    def isFirstPeriod(self):
        start=self.created
        current=self.current_period_start
        if int(start-current)==0:
            return True
        return False
class SubscriptionManager(DynamicEmbeddedDocument):
    __subscriptions=EmbeddedDocumentListField('Subscription')
    __root=None
    __Customer=None
    @classmethod
    def create(cls):
        return cls()
    def initialize(self,root,customer):
        self.__root=root
        self.__Customer=customer
        for subscription in self.__subscriptions:
            subscription.initialize(root=root,customer=customer)
    def update(self,stripeSubscriptionsObject):
        del self.__subscriptions
        self.__subscriptions=[]
        stripeSubscriptions=stripeSubscriptionsObject.data
        for stripeSubscription in stripeSubscriptions:
            subscription=Subscription.create(stripeSubscription=stripeSubscription)
            subscription.initialize(root=self.__root,customer=self.__Customer)
            self.__subscriptions.append(subscription)
        if stripeSubscriptionsObject.has_more:
            lastSubscriptionID=stripeSubscriptions[len(stripeSubscriptions)-1].id
            del stripeSubscriptions
            self.fetch(starting_after=lastSubscriptionID)
    def fetch(self,starting_after):
        try:
            response=stripe.Subscription.list(
                limit=100,
                stripe_account=self.__root.getStripe_user_id(),
                starting_after=starting_after
            )
        except Exception as err:
            raise ErrorObject(err=err)
        stripeSubscriptions=response.data
        for stripeSubscription in stripeSubscriptions:
            subscription=Subscription.create(stripeSubscription=stripeSubscription)
            subscription.initialize(root=self.__root,customer=self.__Customer)
            self.__subscriptions.append(subscription)
        if response.has_more:
            lastSubscriptionID=stripeSubscriptions[len(stripeSubscriptions)-1].id
            del stripeSubscriptions
            self.fetch(starting_after=lastSubscriptionID)
    def list(self):
        return self.__subscriptions
    def hasPlan(self,id):
        for subscription in self.__subscriptions:
            if subscription.has(id=id):
                return True
        return False
    def get(self,id=None,plan=None):
        if id:
            for subscription in self.__subscriptions:
                if subscription._id==id:
                    return subscription
        if plan:
            for subscription in self.__subscriptions:
                if subscription.has(id=plan):
                    return subscription
        return False
    def json(self):
        response=[]
        for subscription in self.__subscriptions:
            response.append(subscription.json())
        return response
class StripeCustomer(DynamicDocument):
    __root=None
    gateWayID=StringField()
    company=ReferenceField('Company')
    email=StringField()
    SubscriptionManager=EmbeddedDocumentField('SubscriptionManager')
    @classmethod
    def create(cls,company,gateWayID):
        if cls.get(gateWayID=gateWayID,company=company) is not None: return
        customer=cls()
        customer.company=company
        customer.gateWayID=gateWayID
        customer.SubscriptionManager=SubscriptionManager.create()
        return customer
    @classmethod
    def updateAll(cls,company):
        connector=company.GateWayConnector.getConnector()
        if connector.getStripe_user_id() is None:
            return []
        nextID=None
        while nextID is not False:
            try:
                response=stripe.Customer.list(
                    limit=100,
                    starting_after=nextID,
                    stripe_account=connector.getStripe_user_id()
                )
                stripeCustomers=response.data
                for stripeCustomer in stripeCustomers:
                    customer=cls.getOrCreate(gateWayID=stripeCustomer.id,company=company)
                    customer.update(stripeCustomerObject=stripeCustomer)
                if response.has_more:
                    nextID=stripeCustomers[len(stripeCustomers)-1].id
                    del stripeCustomers
                    del response
                else:
                    nextID=False
            except Exception as err:
                raise ErrorObject(err=err)

    @classmethod
    def list(cls,company):
        return cls.objects(company=company)
    @classmethod
    def deleteAll(cls,company):
        cls.objects(company=company).delete()
    @classmethod
    def get(cls,company,gateWayID):
        query=cls.objects(company=company,gateWayID=gateWayID)
        if len(query) != 1:
            return None
        return query[0]
    @classmethod
    def getOrCreate(cls,company,gateWayID,gateWayConnector):
        customer=cls.get(company=company,gateWayID=gateWayID)
        if customer is None:
            customer=cls.create(company=company,gateWayID=gateWayID)
            customer.initialize(root=gateWayConnector)
            customer.update()
        else:
            customer.initialize(root=gateWayConnector)
        return customer
    def initialize(self,root):
        self.__root=root
        self.SubscriptionManager.initialize(root=root,customer=self)
    # def charge(self,amount,useCredit=True):
    #     if amount >= 1:
    #         while amount > 99999999:
    #             self.charge(amount=99999999,useCredit=useCredit)
    #             amount-=99999999
    #         if amount < 50:
    #             self.Credit.add(amount=50-amount,charge=False)
    #             amount=50
    #     try:
    #         stripe.Charge.create(
    #             amount=amount,
    #             currency=self.__root.getCurrency(),
    #             customer=self.gateWayID,
    #             stripe_account=self.__root.getStripe_user_id()
    #         )
    #         # if useCredit:
    #         #     self.Credit.use(amount)
    #     except Exception as err:
    #         raise ErrorObject(err=err)
    #     return {"success":"customer has been charged "+str(amount)+self.__root.getCurrency()}
    def update(self,stripeCustomerObject=None):
        if stripeCustomerObject is None:
            try:
                customer=stripe.Customer.retrieve(
                    id=self.gateWayID,
                    stripe_account=self.__root.getStripe_user_id()
                )
            except Exception as err:
                raise ErrorObject(err=err)
        else: 
            if stripeCustomerObject.id!=self.gateWayID: return
            customer=stripeCustomerObject
        self.email=customer.email
        self.SubscriptionManager.update(stripeSubscriptionsObject=customer.subscriptions)
        self.save()
    def json(self):
        response={}
        response['id']=self.gateWayID
        response['email']=self.email
        response['subscriptionManager']=self.SubscriptionManager.json()
        return response
    def getStripeObject(self):
        return stripe.Customer.retrieve(
            id=self.gateWayID,
            stripe_account=self.__root.getStripe_user_id()
        )

#Connectors
class StripeConnector(DynamicEmbeddedDocument):
    PlanManager=EmbeddedDocumentField('PlanManager')
    Credentials=EmbeddedDocumentField('Credentials')
    __clientID='ca_5UjlDNH9uQorbvTGq3Y6KI6E0MRMRXqg'
    __secretKey='sk_test_VRCz3YmqpKveRVUY6ZROENbv'
    stripe_user_id=StringField(default=None)
    __refresh_token=StringField(default=None)
    __currency=StringField()
    initialized=False
    stripe.api_key=__secretKey
    preChargeAmount=IntField(default=0)
    def setPreChargeAmount(self,preChargeAmount):
        self.preChargeAmount=preChargeAmount
        self.save()
    def getPreChargeAmount(self):
        return self.preChargeAmount
    def setCurrency(self,currency):
        self.__currency=currency
        try:
            self.save()
        except: pass
    def json(self):
        response={}
        response['plans']=self.PlanManager.json()
        response['preChargeAmount']=self.preChargeAmount
        response['currency']=self.getCurrency()
        return response
    def getCurrency(self):
        return self.__currency
    def getClientID(self):
        return self.__clientID
    def getSecretKey(self):
        return self.__secretKey
    def initialize(self):
        if self.initialized: return
        self.initialized=True
        self.Credentials.initialize(root=self)
        self.PlanManager.initialize(root=self)
    def update(self,plan=False,stripe_user_id=False,refresh_token=False):
        if plan:
            self.PlanManager.update()
        if stripe_user_id:
            self.stripe_user_id=stripe_user_id
        if refresh_token:
            self.__refresh_token=refresh_token
    def setStripe_user_id(self,stripe_user_id):
        self.stripe_user_id=stripe_user_id
    def getStripe_user_id(self):
        return self.stripe_user_id
    def setrefresh_token(self,refresh_token):
        self.__refresh_token=refresh_token
    def getrefresh_token(self):
        return self.__refresh_token
    def isConnected(self):
        stripeUserID=self.getStripe_user_id()
        if stripeUserID is None or len(stripeUserID)==0:
            return False
        return True
    @classmethod
    def create(cls,currency):
        stripePE=cls()
        stripePE.Credentials=Credentials.create()
        stripePE.PlanManager=PlanManager.create()
        stripePE.setCurrency(currency=currency)
        return stripePE


