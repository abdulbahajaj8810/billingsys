from mongoengine import *
from models.GateWayConnector import GateWayConnector
from models.UnitManager import UnitManager
import uuid
class Company(DynamicDocument):
    APIKey=StringField()
    publicAPIKey=StringField()
    name=StringField()
    endpointURL=StringField(default='')
    registered=BooleanField(default=False)
    GateWayConnector=EmbeddedDocumentField('GateWayConnector')
    UnitManager=EmbeddedDocumentField('UnitManager')
    def getEndPoint(self):
        return self.endpointURL
    @classmethod
    def create(cls,name,email,password,currency):
        company=cls(name=name)
        company.publicAPIKey=uuid.uuid4().hex
        company.save()
        company.GateWayConnector=GateWayConnector.create(
            connector='stripe',
            currency=currency
        )
        Member.create(
            company=company,
            email=email,
            password=password
        )
        company.UnitManager=UnitManager.create()
        company.setAPIKey()
        company.save()
        return company
    @classmethod
    def get(cls,name=None,id=None,stripe_user_id=None,publicAPIKey=None,privateAPIKey=None):
        if publicAPIKey is not None:
            company=cls.objects(publicAPIKey=publicAPIKey)
        elif id is not None:
            company=cls.objects(id=id)
        elif name is not None:
            company=cls.objects(name=name)
        elif privateAPIKey is not None:
            company=cls.objects(APIKey=privateAPIKey)
        elif stripe_user_id is not None:
            company=cls.objects(
                GateWayConnector__stripeGateWayConnector__stripe_user_id=stripe_user_id
            )
        else:
            return False
        if len(company)==1:
            return company[0]
        return False
    @classmethod
    def __integrityCheck(self,name):
        companies=Company.objects(name=name)
        if len( companies ) > 0:
            return False
        return True
    def edit(self,name=None,currency=None,endpointURL=None):
        if name is not None:
            if not Company.__integrityCheck(name=name):
                return False
            self.name=name
        if currency is not None:
            gateWayConnector=company.getGateWayConnector()
            gateWayConnector.setCurrency(currency=currency)
        if endpointURL is not None:
            self.endpointURL=endpointURL
        self.save()
        return True
    def getAPIKey(self):
        return self.APIKey
    def setAPIKey(self):
        APIKey=uuid.uuid4().hex
        self.APIKey=APIKey
        # self.save()
    def authenticate(self,APIKey):
        if self.APIKey==APIKey:
            return True
        return False
    def remove(self):
        for member in Member.objects(company=self):
            member.remove()
        # if self.UnitTemplateManager is not None:
        #     self.UnitTemplateManager.remove()
        self.delete()
    def getCustomer(self,gateWayID):
        print 'Company.getCustomer'
        customer=CompanyCustomer.objects(company=self,gateWayID=gateWayID)
        if len(customer)!=1: return None
        customer=customer[0]
        customer.initialize()
        return customer
    def listCustomers(self,step=0,json=False):
        listSize=24
        step+=listSize
        result=CompanyCustomer.objects(company=self)[ step-listSize : step ]
        if json:
            temp=[]
            for customer in result:
                temp.append(customer.json())
            result=temp
        return (result,step)
    def getGateWayConnector(self):
        return self.GateWayConnector
    def addCustomer(self,gateWayID,UnitManager=None):
        if not self.getGateWayConnector().isConnected():
            return -1
        if UnitManager is None:
            UnitManager=self.getDefaultPricing()
        customer=CompanyCustomer.create(
            gateWayID=gateWayID,
            company=self,
        )
        if customer is False:
            return -2
        return True
    def getMember(self,memberID):
        member=Member.objects(id=memberID,company=self)
        if not len(member)==1: return None
        member=member[0]
        return member
    def listMembers(self):
        return Member.objects(company=self)
    def apply(self,subscriptionID,gateWayID):
        customer=self.getCustomer(gateWayID=gateWayID)
        return customer.apply(subscriptionID=subscriptionID)
    def json(self):
        response={}
        response['name']=self.name
        response['publicAPIKey']=self.publicAPIKey
        response['registered']=self.registered
        response['endpointURL']=self.endpointURL
        response['units']=self.getDefaultPricing().json()
        response['currency']=self.GateWayConnector.getCurrency()
        response['customers']=[]
        response['isConnected']=self.GateWayConnector.isConnected()
        response['gateWay']=self.GateWayConnector.json()
        return response
    def usageHistory(self):
        companyUsage=CompanyUsage.getCompany(company=self)
        return companyUsage.json()
    @classmethod
    def register(cls,placeholder,email,password):
        company=cls.get(name=placeholder)
        if company is False: return -1 
        member=Member.get(password=placeholder,company=company)
        if not isinstance(member,Member): return -1
        company.name=''
        member.email=email
        if not Member.integrityCheck(member=member):
            return -2
        member.editPassword(currentPassword=placeholder,newPassword=password)
        member.save()
        company.registered=True
        company.save()
        return True
    @classmethod
    def createEmpty(cls):
        placeholder=uuid.uuid4().hex
        return cls.create(
            name=placeholder,
            email=placeholder,
            password=placeholder,
            currency="USD",
        )
    def getDefaultPricing(self):
        return self.UnitManager

from models.Customer import CompanyCustomer
from models.Member import Member
# [{"rules": [], "model": "stairstep", "plan": "plan1"}, {"rules": [{"cost": 44, "above": 100, "each": 3}, {"cost": 40, "above": 120, "each": 1}], "model": "volume", "plan": "plan3"}]
