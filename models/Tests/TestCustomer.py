import unittest,time,uuid
from mongoengine import *
from Customer.versions.v1.models import CompanyCustomer
from django.contrib.auth.models import User
from Company.versions.v1.models import Company
from UnitTemplateManager.versions.v1.models import UnitTemplateManager
import uuid
#Check for 3 "Quantity changed!" prints in the terminal
connect('testor')
class TestUnitTemplateManager(unittest.TestCase):
    def setUp(self):
        self.unitTemplateManager=UnitTemplateManager.create()
        self.pricingTemplate=self.unitTemplateManager.add(
            name='test_pricingTemplate'
        )
        self.company=Company.create(
            name=uuid.uuid4().hex,
            password='test_password',
            currency='USD',
            email='test_email'
        )
        self.company.GateWayConnector.stripeGateWayConnector.setStripe_user_id(
            'acct_103psN2ZQ5Pe7aai'
        )
        self.gateWayID='cus_AzqgdVqgbt0m25'
        self.CompanyCustomer=CompanyCustomer.create(
            gateWayID=self.gateWayID,
            company=self.company,
            pricingTemplate=self.pricingTemplate
        )
    def test_get(self):
        customer=CompanyCustomer.get(
            company=self.company,
            gateWayID=self.gateWayID
        )
        self.assertNotEqual(customer,False)
        stripeObj=customer.getCustomerObject()
        self.assertEqual(stripeObj._id,self.gateWayID)
    def test_use(self):
        pricingTemplate=self.unitTemplateManager.add(
            name='test_pricingTemplate2'
        )
        self.assertEqual(
            self.CompanyCustomer.PricingTemplate.name,
            'test_pricingTemplate'
        )
        self.CompanyCustomer.use(PricingTemplate=pricingTemplate)
        self.assertEqual(
            self.CompanyCustomer.PricingTemplate.name,
            'test_pricingTemplate2'
        )
    def test_reportQuantityChange(self):
        UnitManager=self.CompanyCustomer.PricingTemplate.UnitManager
        unit=UnitManager.add(
            id='test_id',
            name='test_name',
            description='test_description'
        )
        # class Recorder(object):
        #     records=None
        #     def __init__(self,CompanyCustomer):
        #         self.records=[]
        #     def record(self,plan,unit,quantity):
        #         record={}
        #         record['plan']=plan
        #         record['unit']=unit
        #         record['quantity']=quantity
        #         self.records.append(quantity)
        # recorder=Recorder(
        #     CompanyCustomer=self.CompanyCustomer
        # )
        # self.CompanyCustomer.reportQuantityChange=recorder.record
        unit.Quantity.set(quantity=1000)
        # self.assertEqual(
        #     len(recorder.records),1
        # )
        unit.Quantity.set(quantity=1000)
        # self.assertEqual(
        #     len(recorder.records),2
        # )
        unit.Quantity.set(quantity=1000)
        # self.assertEqual(
        #     len(recorder.records),2
        # )


    def tearDown(self):
        pass
if __name__ == 'main':
    unittest.main()
