import unittest,time,uuid
from mongoengine import *
from UnitTemplateManager.versions.v1.models import UnitTemplateManager
from django.contrib.auth.models import User

connect('testor')
class TestUnitTemplateManager(unittest.TestCase):
    def setUp(self):
        self.unitTemplateManager=UnitTemplateManager.create()
    def test_add(self):
        self.unitTemplateManager.add(name='test')
        count=len(self.unitTemplateManager.list())
        self.assertEqual(count,1)
        result=self.unitTemplateManager.add(name='test')
        self.assertFalse(result)
        self.unitTemplateManager.add(name='test2')
        count=len(self.unitTemplateManager.list())
        self.assertEqual(count,2)
    def test_remove(self):
        self.unitTemplateManager.add(name='test')
        count=len(self.unitTemplateManager.list())
        name=self.unitTemplateManager.list()[0].name
        self.assertEqual(count,1)
        self.assertEqual(name,'test')
        self.unitTemplateManager.add(name='test1')
        count=len(self.unitTemplateManager.list())
        name=self.unitTemplateManager.list()[1].name
        self.assertEqual(count,2)
        self.assertEqual(name,'test1')
        self.unitTemplateManager.add(name='test2')
        count=len(self.unitTemplateManager.list())
        name=self.unitTemplateManager.list()[2].name
        self.assertEqual(count,3)
        self.assertEqual(name,'test2')
        pricingTemplate=self.unitTemplateManager.get(
            name='test'
        )
        self.assertEqual(pricingTemplate.name,'test')
        pricingTemplate.delete()
        count=len(self.unitTemplateManager.list())
        self.assertEqual(count,2)
        pricingTemplate=self.unitTemplateManager.get(
            name='test1'
        )
        pricingTemplate.delete()
        count=len(self.unitTemplateManager.list())
        self.assertEqual(count,1)
        self.assertEqual(pricingTemplate.name,'test1')
        pricingTemplate=self.unitTemplateManager.get(
            name='test2'
        )
        self.assertEqual(pricingTemplate.name,'test2')
        pricingTemplate.delete()
        count=len(self.unitTemplateManager.list())
        self.assertEqual(count,0)
    def test_pricingTemplate_makeDefault_and_makeLegacy(self):
        self.unitTemplateManager.add(name='test')
        pricingTemplate1=self.unitTemplateManager.get(name='test')
        self.assertFalse(pricingTemplate1.isDefault)
        pricingTemplate2=self.unitTemplateManager.add('test2')
        self.assertFalse(pricingTemplate2.isDefault)
        pricingTemplate3=self.unitTemplateManager.add('test3')
        self.assertFalse(pricingTemplate3.isDefault)
        self.unitTemplateManager.makeDefault(name='test2')
        pricingTemplate1=self.unitTemplateManager.get(name='test')
        pricingTemplate2=self.unitTemplateManager.get('test2')
        pricingTemplate3=self.unitTemplateManager.get('test3')
        default=self.unitTemplateManager.getDefault()
        self.assertFalse(pricingTemplate1.isDefault)
        self.assertTrue(pricingTemplate2.isDefault)
        self.assertFalse(pricingTemplate3.isDefault)
        self.assertEqual(default.name,'test2')
        self.unitTemplateManager.makeDefault(name='test3')
        pricingTemplate1=self.unitTemplateManager.get(name='test')
        pricingTemplate2=self.unitTemplateManager.get('test2')
        pricingTemplate3=self.unitTemplateManager.get('test3')
        default=self.unitTemplateManager.getDefault()
        self.assertFalse(pricingTemplate1.isDefault)
        self.assertFalse(pricingTemplate2.isDefault)
        self.assertTrue(pricingTemplate3.isDefault)
        self.assertEqual(default.name,'test3')
    def tearDown(self):
        pass
if __name__ == 'main':
    unittest.main()
