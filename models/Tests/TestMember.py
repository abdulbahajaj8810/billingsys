import unittest,time,uuid
from GateWayConnector.versions.v1.models import GateWayConnector
from Company.versions.v1.models import Company
from mongoengine import *
from Member.versions.v1.models import Member
from UnitTemplateManager.versions.v1.models import UnitTemplateManager
from django.contrib.auth.models import User
from Helpers.versions.v1.Encryptor import Encryptor
class TestMember(unittest.TestCase):
    def setUp(self):
        self.name=uuid.uuid4().hex
        self.email='abdulbahajaj@gmail.com'
        self.password='bahajaj8810'
        company=Company(name=self.name)
        company.save()
        # company=None
        self.member=Member.create(
            email=self.email,
            password=self.password,
            company=company
        )
    def test_setCompanyKey(self):
        self.assertNotEqual(
            self.member.companyKey,
            None
        )
    def test_getCompanyKey(self):
        companyKey=self.member.getCompanyKey(password=self.password)
        self.assertNotEqual(
            companyKey,
            self.member.companyKey
        )
        self.assertNotEqual(
            companyKey,None
        )
    def test_get(self):
        member=Member.get(
            companyName=self.name,
            email=self.email,
            password=self.password
        )
        self.assertNotEqual(member,False)
        self.assertEqual(
            member.email,
            self.member.email
        )
        self.assertEqual(
            member.permission,
            self.member.permission
        )
        self.assertEqual(
            member.companyKey,
            self.member.companyKey
        )
        self.assertEqual(
            member.IV,
            self.member.IV
        )
        self.assertEqual(
            member.company.name,
            self.member.company.name
        )
    def test_authenticate(self):
        auth=self.member.authenticate(password=self.password)
        self.assertTrue(auth)
        auth=self.member.authenticate(password='WrongPassword')
        self.assertFalse(auth)
    def test_invitation_create_customer(self):
        invitation=self.member.invite(
            permission='customer',
            password=self.password
        )
        code=invitation.code
        invitation=Member.Invitation.get(code=code)
        self.assertNotEqual(invitation,False)
        self.assertEqual(invitation.code,code)
        self.assertNotEqual(invitation.code,None)
        self.assertEqual(len(invitation.code),32)
        self.assertEqual(invitation.issuedBy.id,self.member.id)
        self.assertEqual(
            invitation.company.name,self.member.company.name
        )
        self.assertEqual(
            invitation.company.id,self.member.company.id
        )
        self.assertEqual(invitation.permission,'customer')
        self.assertTrue(invitation.isAbled())
        self.assertIsInstance(invitation.created,int)
        self.assertEqual(invitation.duration,60*60*8)
        self.assertFalse(invitation.isExpired())
        invitation.duration=-10
        self.assertTrue(invitation.isExpired())
    def test_invitation_create_api(self):
        invitation=self.member.invite(permission='api',password='WrongPassword')
        self.assertFalse(invitation)
        invitation=self.member.invite(permission='api',password=self.password)
        self.assertNotEqual(invitation,False)
        self.assertNotEqual(invitation.companyKey,None)
        self.assertNotEqual(invitation.IV,None)
        self.assertFalse(invitation.isExpired())
        self.assertTrue(invitation.isAbled())
        invitation.disable()
        self.assertFalse(invitation.isExpired())
        self.assertEqual(invitation.permission,'api')
    def test_invitation_create_admin(self):
        invitation=self.member.invite(permission='admin',password='WrongPassword')
        self.assertFalse(invitation)
        invitation=self.member.invite(permission='admin',password=self.password)
        self.assertNotEqual(invitation,False)
        self.assertNotEqual(invitation.companyKey,None)
        self.assertNotEqual(invitation.IV,None)
        self.assertFalse(invitation.isExpired())
        self.assertTrue(invitation.isAbled())
        invitation.disable()
        self.assertFalse(invitation.isExpired())
        self.assertEqual(invitation.permission,'admin')
    def test_invitation_createMember_customer(self):
        invitation=self.member.invite(
            permission='customer',
            password=self.password
        )
        invitation.createMember(
            email='customer_email',
            password='customer_password',
            code=invitation.code
        )
        member=Member.get(
            companyName=self.name,
            email='customer_email',
            password='customer_password'
        )
        self.assertNotEqual(member,False)
        self.assertEqual(member.email,'customer_email')
        self.assertEqual(member.permission,'customer')
        self.assertEqual(member.company.name,self.name)
    def test_invitation_createMember_api(self):
        invitation=self.member.invite(
            permission='api',
            password=self.password
        )
        invitation.createMember(
            email='api_email',
            password='api_password',
            code=invitation.code
        )
        member=Member.get(
            companyName=self.name,
            email='api_email',
            password='api_password'
        )
        self.assertNotEqual(member,False)
        self.assertEqual(member.permission,'api')
        self.assertNotEqual(member.IV,None)
        self.assertNotEqual(member.companyKey,None)
        self.assertEqual(
            self.member.getCompanyKey(password=self.password),
            member.getCompanyKey(password='api_password')
        )
        self.assertEqual(
            self.member.company.name,
            member.company.name
        )
    def test_invitation_createMember_admin(self):

        invitation=self.member.invite(
            permission='admin',
            password=self.password
        )
        invitation.createMember(
            email='admin_email',
            password='admin_password',
            code=invitation.code
        )
        member=Member.get(
            companyName=self.name,
            email='admin_email',
            password='admin_password'
        )
        self.assertNotEqual(member,False)
        self.assertEqual(member.permission,'admin')
        self.assertNotEqual(member.IV,None)
        self.assertNotEqual(member.companyKey,None)
        self.assertEqual(
            self.member.getCompanyKey(password=self.password),
            member.getCompanyKey(password='admin_password')
        )
        self.assertEqual(
            self.member.company.name,
            member.company.name
        )
    def test_changePermission(self):
        self.assertEqual(self.member.permission,'admin')
        change=self.member.changePermission(
            email=self.member.email,
            permission='customer'
        )
        self.assertFalse(change)
        change=self.member.changePermission(
            email=self.member.email,
            permission='api'
        )
        self.assertFalse(change)
        change=self.member.changePermission(
            email=self.member.email,
            permission='admin'
        )
        self.assertTrue(change)
        invitation=self.member.invite(
            permission='customer',
            password=self.password
        )
        member=invitation.createMember(
            email="customer_email",
            password="customer_password",
            code=invitation.code
        )
        self.assertEqual(member.permission,'customer')
        change=member.changePermission(
            email=self.member.email,
            permission='customer'
        )
        self.assertFalse(change)
        change=self.member.changePermission(
            email=member.email,
            permission='api'
        )
        self.assertTrue(change)
        member=Member.get(
            companyName=self.name,
            email='customer_email',
            password='customer_password'
        )
        self.assertEqual(member.permission,'api')
        companyKey1=member.getCompanyKey(password='customer_password')
        companyKey2=self.member.getCompanyKey(password=self.password)
        self.assertEqual(companyKey1,companyKey2)
    def test_remove(self):
        pass
    def tearDown(self):
        self.member.remove()
if __name__ == 'main':
    unittest.main()
