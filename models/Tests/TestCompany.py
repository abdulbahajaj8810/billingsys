import unittest,time,uuid
from GateWayConnector.versions.v1.models import GateWayConnector
from Company.versions.v1.models import Company
from mongoengine import *
from Member.versions.v1.models import Member
from UnitTemplateManager.versions.v1.models import UnitTemplateManager
from django.contrib.auth.models import User

connect('testor')
class TestCompany(unittest.TestCase):
    def setUp(self):
        self.name=uuid.uuid4().hex
        self.email='abdulbahajaj@gmail.com'
        self.password='bahajaj8810'
        self.company=Company.create(
            name=self.name,
            email=self.email,
            password=self.password,
            currency='USD'
        )
    def test_get(self):
        company=Company.get(name=self.name)
        self.assertNotEqual(company,False)
        self.assertEqual(
            company.APIKey,
            self.company.APIKey
        )
        self.assertEqual(
            company.name,
            self.company.name
        )
        self.assertEqual(
            company.IV,
            self.company.IV
        )
    def test_edit(self):
        name='testEdit3'
        tempName=self.company.name
        self.assertTrue(
            self.company.edit(name=name)
        )
        company=Company.get(name=name)
        self.assertNotEqual(company,False)
        self.assertEqual(
            company.APIKey,
            self.company.APIKey
        )
        self.assertEqual(
            company.name,
            self.company.name
        )
        self.assertEqual(
            company.IV,
            self.company.IV
        )
        self.company.edit(name=tempName)
    def test_getAPIKey_AND_authentication(self):
        member=Member.get(
            companyName=self.name,
            email=self.email,
            password=self.password
        )
        companyKey=member.getCompanyKey(password=self.password)
        APIKey=self.company.getAPIKey(companyKey=companyKey)
        self.assertNotEqual(
            APIKey,
            False
        )
        self.assertTrue(
            self.company.authenticate(APIKey=APIKey)
        )
        self.assertFalse(
            self.company.authenticate(APIKey=APIKey + '1')
        )
        self.assertFalse(
            self.company.authenticate(APIKey=APIKey[:len(APIKey)-2]+'A')
        )
        self.assertFalse(
            self.company.authenticate(APIKey='FALSE API KEY')
        )
    def test_remove(self):
        templateManagerID=self.company.UnitTemplateManager.id
        systemUser_username=self.company.getUserID()

        member=Member.get(
            companyName=self.name,
            email=self.email,
            password=self.password
        )
        self.assertNotEqual(member,False)
        unitTemplateManager=UnitTemplateManager.objects(name=self.name)
        self.assertNotEqual(unitTemplateManager,False)
        company=Company.get(name=self.name)
        self.assertNotEqual(company,False)
        try:
            exceptionHappened=False
            systemUser=User.objects.get(username=systemUser_username)
        except:
            exceptionHappened=True
        self.assertFalse(exceptionHappened,False)


        self.company.remove()
        member=Member.get(
            companyName=self.name,
            email=self.email,
            password=self.password
        )
        self.assertFalse(member)
        unitTemplateManager=UnitTemplateManager.objects(name=self.name)
        self.assertFalse(unitTemplateManager)
        company=Company.get(name=self.name)
        self.assertFalse(company)
        try:
            exceptionHappened=False
            systemUser=User.objects.get(username=systemUser_username)
        except:
            exceptionHappened=True
        self.assertTrue(exceptionHappened)
    def tearDown(self):
        pass
if __name__ == 'main':
    unittest.main()
