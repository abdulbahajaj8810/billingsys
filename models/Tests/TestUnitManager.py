import unittest,time,uuid
from GateWayConnector.versions.v1.models import GateWayConnector
from Company.versions.v1.models import Company
from mongoengine import *
from Helpers.versions.v1.EventListener import *
from UnitManager.versions.v1.models import UnitManager
from Company.versions.v1.models import Company
connect('testor')
#Should see a pending item of 1.00 and 2.00
class testor(DynamicDocument):
    unitManager=EmbeddedDocumentField('UnitManager')
    @classmethod
    def create(cls):
        unitManager=UnitManager.create()
        um=cls(unitManager=unitManager)
        um.save()
        return um.id
class EventRecorder(object):
    events=None
    @classmethod
    def create(cls):
        eventRecorder=cls()
        eventRecorder.events=[]
        return eventRecorder
    def record(self,**args):
        self.events.append(args)
    def list(self):
        return self.events
class TestUnitManager(unittest.TestCase):
    def setUp(self):
        #Creating a gateWayConnector
        self.gateWayConnector=GateWayConnector.create(
            connector='stripe',
            currency='CAD'
        )
        self.gateWayConnector.initialize()
        self.gateWayConnector.stripeGateWayConnector.setStripe_user_id(
            'acct_103psN2ZQ5Pe7aai'
        )
        self.gateWayConnector.initialize()
        self.gateWayConnector.stripeGateWayConnector.setStripe_user_id(
            'acct_103psN2ZQ5Pe7aai'
        )
        #Creating a company
        self.company=Company()
        self.company.GateWayConnector=self.gateWayConnector
        self.company.save()
        #Creating a customer
        self.customerManager=GateWayConnector.CustomerManager(
            type='stripe',
            company=self.company
        )
        self.customer=self.customerManager.create(
            id='cus_AzqgdVqgbt0m25'
        )
        #Creating event recorder
        self.eventRecorder=EventRecorder.create()
        #Creating event listener
        self.EEL=EventListener.create()
        self.EEL.add(name='QuantityChange')
        self.EEL.register(
            name='QuantityChange',
            function=self.eventRecorder.record
        )
        #Creating a unit manager
        self.id=testor.create()
        self.testor=testor.objects(id=self.id)[0]
        self.unitManager=self.testor.unitManager
        self.unitManager.initialize(
            eventListener=self.EEL,
            gateWayCustomer=self.customer
        )
    def test_UnitManager(self):
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        self.assertEqual(
            len(self.unitManager.list()),
            1
        )
        unit1=self.unitManager.get(id='unit1')
        self.assertEqual(
            unit1.getID(),
            'unit1'
        )
        try:
            exceptionOccured=False
            self.unitManager.add(
                id='unit1',
                name='unit1_name_replica',
                description='unit1_description_replica'
            )
        except:
            exceptionOccured=True
        self.assertTrue(exceptionOccured)
    def test_UnitManager_Unit(self):
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        unit1=self.unitManager.get(id='unit1')
        unit1.edit(name='unit1_changed_name')
        self.assertEqual(
            unit1.p__name,
            'unit1_changed_name'
        )
        self.assertEqual(
            unit1.p__description,
            'unit1_description'
        )
        unit1.edit(description='unit1_changed_description')
        self.assertEqual(
            unit1.p__name,
            'unit1_changed_name'
        )
        self.assertEqual(
            unit1.p__description,
            'unit1_changed_description'
        )
    def test_UnitManager_Unit_Quantity(self):
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        unit1=self.unitManager.get(id='unit1')
        self.assertEqual(
            unit1.Quantity.value(),
            0
        )
        unit1.Quantity.set(quantity=101)
        self.assertEqual(
            unit1.Quantity.value(),
            101
        )
        self.assertEqual(
            len(self.eventRecorder.list()),
            1
        )
        self.assertEqual(
            self.eventRecorder.list()[0].get('unit'),
            'unit1'
        )
        self.assertEqual(
            self.eventRecorder.list()[0].get('quantity'),
            101
        )
        unit1.Quantity.add(quantity=100)
        self.assertEqual(
            unit1.Quantity.value(),
            201
        )
        self.assertEqual(
            len(self.eventRecorder.list()),
            2
        )
        self.assertEqual(
            self.eventRecorder.list()[1].get('quantity'),
            201
        )
        unit1.Quantity.restart()
        self.assertEqual(
            unit1.Quantity.value(),
            201
        )
        unit1.Quantity.edit(resetEachInterval=True)
        self.assertEqual(
            unit1.Quantity.value(),
            201
        )
        unit1.Quantity.restart()
        self.assertEqual(
            unit1.Quantity.value(),
            0
        )
    def test_UnitManager_Unit_PricingManager(self):
        #test add, get, delete, list
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        unit1=self.unitManager.get(id='unit1')
        unit1.PricingManager.add(plan='test1')
        self.assertEqual(
            len(unit1.PricingManager.list()),
            2
        )
        unit1.PricingManager.add(plan='test1')
        self.assertEqual(
            len(unit1.PricingManager.list()),
            2
        )
        unit1.PricingManager.add(plan='test2')
        self.assertEqual(
            len(unit1.PricingManager.list()),
            3
        )
        self.assertTrue(
            unit1.PricingManager.get(trialMode=True).isTrialMode()
        )
        self.assertFalse(
            unit1.PricingManager.get(plan='test3'),
        )
        self.assertEqual(
            unit1.PricingManager.get(plan='test2').getPlan(),
            'test2'
        )
        self.assertEqual(
            unit1.PricingManager.get(plan='test1').getPlan(),
            'test1'
        )
        unit1.PricingManager.delete(plan='test2')
        self.assertFalse(
            unit1.PricingManager.get(plan='test2'),
        )
        self.assertEqual(
            unit1.PricingManager.get(plan='test1').getPlan(),
            'test1'
        )
    def test_UnitManager_Unit_PricingManager_Pricing(self):
        #Test addRule, getRule, getModel, getPlan, cost, reset, edit
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        unit1=self.unitManager.get(id='unit1')
        unit1.PricingManager.add(plan='test1')
        pricing1=unit1.PricingManager.get(plan='test1')
        self.assertEqual(
            pricing1.cost(), 0
        )
        pricing1.addRule(above=10,cost=10,each=1)
        pricing1.addRule(above=20,cost=20,each=4)
        pricing1.addRule(above=30,cost=30,each=5)
        self.assertEqual(pricing1.cost(), 0)

        self.assertEqual(pricing1.getModel(),'volume')
        #Testing cost
        unit1.Quantity.set(quantity=0.5)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=9)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=10)
        self.assertEqual(pricing1.cost(),100)
        unit1.Quantity.set(quantity=10.5)
        self.assertEqual(pricing1.cost(),105)
        unit1.Quantity.set(quantity=11)
        self.assertEqual(pricing1.cost(),110)
        unit1.Quantity.set(quantity=14.5)
        self.assertEqual(pricing1.cost(),145)
        unit1.Quantity.set(quantity=15)
        self.assertEqual(pricing1.cost(),150)
        unit1.Quantity.set(quantity=19)
        self.assertEqual(pricing1.cost(),190)
        unit1.Quantity.set(quantity=20)
        self.assertEqual(pricing1.cost(),100)
        unit1.Quantity.set(quantity=21)
        self.assertEqual(pricing1.cost(),105)
        unit1.Quantity.set(quantity=29)
        self.assertEqual(pricing1.cost(),145)
        unit1.Quantity.set(quantity=30)
        self.assertEqual(pricing1.cost(),180)
        unit1.Quantity.set(quantity=31)
        self.assertEqual(pricing1.cost(),186)

        pricing1.edit(model='stairstep')
        self.assertEqual(pricing1.getModel(),'stairstep')
        unit1.Quantity.set(quantity=0.5)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=9)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=10)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=10.5)
        self.assertEqual(pricing1.cost(),15)
        unit1.Quantity.set(quantity=11)
        self.assertEqual(pricing1.cost(),20)
        unit1.Quantity.set(quantity=14.5)
        self.assertEqual(pricing1.cost(),55)
        unit1.Quantity.set(quantity=15)
        self.assertEqual(pricing1.cost(),60)
        unit1.Quantity.set(quantity=19)
        self.assertEqual(pricing1.cost(),100)
        unit1.Quantity.set(quantity=20)
        self.assertEqual(pricing1.cost(),120)
        unit1.Quantity.set(quantity=21)
        self.assertEqual(pricing1.cost(),140)
        unit1.Quantity.set(quantity=29)
        self.assertEqual(pricing1.cost(),300)
        unit1.Quantity.set(quantity=30)
        self.assertEqual(pricing1.cost(),330)
        unit1.Quantity.set(quantity=31)
        self.assertEqual(pricing1.cost(),360)

        pricing1.edit(model='package')
        self.assertEqual(pricing1.getModel(),'package')
        unit1.Quantity.set(quantity=0.5)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=9)
        self.assertEqual(pricing1.cost(),0)
        unit1.Quantity.set(quantity=10)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=10.5)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=11)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=14.5)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=15)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=19)
        self.assertEqual(pricing1.cost(),10)
        unit1.Quantity.set(quantity=20)
        self.assertEqual(pricing1.cost(),20)
        unit1.Quantity.set(quantity=21)
        self.assertEqual(pricing1.cost(),20)
        unit1.Quantity.set(quantity=29)
        self.assertEqual(pricing1.cost(),20)
        unit1.Quantity.set(quantity=30)
        self.assertEqual(pricing1.cost(),30)
        unit1.Quantity.set(quantity=31)
        self.assertEqual(pricing1.cost(),30)
    def test_UnitManager_Unit_Applier(self):
        '''
        Test
        Apply on
            With atEnd or not
            is on trial or not
        '''
        self.unitManager.add(
            id='unit1',
            name='unit1_name',
            description='unit1_description'
        )
        unit1=self.unitManager.get(id='unit1')
        unit1.PricingManager.add(plan='test3')
        unit1_pricing=unit1.PricingManager.get(trialMode=True)
        unit1_pricing.addRule(above=10,cost=20,each=1)
        #End or not
        unit1.Quantity.set(quantity=10)
        unit1.Applier.apply()
        unit1.Applier.edit(atEnd=False)
        unit1.Applier.apply()


        self.unitManager.add(
            id='unit2',
            name='unit2_name',
            description='unit2_description'
        )
        unit2=self.unitManager.get(id='unit2')
        print 'unit2: ', unit2
        unit2.PricingManager.add(plan='test1')
        unit2_pricing=unit2.PricingManager.get(plan='test1')
        unit2_pricing.addRule(above=10,cost=10,each=1)
        unit2.Quantity.set(quantity=10)
        unit2.Applier.apply()
        unit2.Applier.edit(atEnd=False)
        unit2.Applier.apply()
    def tearDown(self):
        self.testor.unitManager.p__exceptionManager=None
        self.testor.save()
        self.testor.delete()
        self.company.delete()
