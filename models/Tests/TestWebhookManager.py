from WebhookManager.versions.v1.models import WebhookManager
import unittest,time,uuid
#Inspect request @ https://requestb.in/18shpg91?inspect
#Check for 1 request made
class TestGateWayConnector(unittest.TestCase):
    def setUp(self):
        self.webhookManager=WebhookManager.create()
        self.webhookManager.set(
            plan='test1',
            unit='apiCall',
            quantity=1000,
            constants={'api test':1}
        )
        self.webhookManager.set(
            plan='test1',
            unit='apiCall',
            quantity=2000,
            constants={'api test':2}
        )
        self.webhookManager.set(
            plan='test3',
            unit='apiCall',
            quantity=2000,
            constants={'api test':1}
        )
        self.webhookManager.set(
            plan='test3',
            unit='apiCall',
            quantity=2000,
            constants={'api test':3}
        )
        self.webhookManager.set(
            plan='test4',
            unit='apiCall',
            quantity=1,
            constants={'api test':4}
        )
    def test_webhookManager(self):
        self.assertEqual(
            len(self.webhookManager.list()),
            4
        )
        self.assertEqual(
            self.webhookManager.get(
                unit="doesn't exist",
                quantity=5,
                plan='55'
            ),
            False
        )
        self.assertTrue(
            self.webhookManager.get(
                unit="apiCall",
                quantity=2000,
                plan='test3'
            ) is not False
        )
        selectedWebhook=self.webhookManager.get(
            plan='test3',
            unit='apiCall',
            quantity=2000,
        )
        self.assertEqual(
            selectedWebhook.constants.get('api test'),
            3
        )
    def test_webhook(self):
        self.webhookManager.decide(
            plan='test3',
            unit='apiCall',
            quantity=2000,
            gateWayID='cus_AzqgdVqgbt0m25',
            endpoint='https://requestb.in/18shpg91'
        )
        self.webhookManager.decide(
            plan="doesn't exist",
            unit='apiCall',
            quantity=2000,
            gateWayID='cus_AzqgdVqgbt0m25',
            endpoint='https://requestb.in/18shpg91'
        )
    def test_retries(self):
        self.webhookManager.decide(
            plan="test3",
            unit='apiCall',
            quantity=2000,
            gateWayID='cus_AzqgdVqgbt0m25',
            endpoint='http://httpstat.us/306'
        )
        self.webhookManager.decide(
            plan="test3",
            unit='apiCall',
            quantity=2000,
            gateWayID='cus_AzqgdVqgbt0m25',
            endpoint='http://httpstat.us/412'
        )
        self.webhookManager.decide(
            plan="test3",
            unit='apiCall',
            quantity=2000,
            gateWayID='cus_AzqgdVqgbt0m25',
            endpoint='http://httpstat.us/505'
        )
        self.assertEqual(
            len(WebhookManager.Retries.list(
                webhookManager=self.webhookManager
            )),
            3
        )
    def tearDown(self):
        self.webhookManager.delete()
        for retry in WebhookManager.Retries.list(
            webhookManager=self.webhookManager
        ): retry.delete()
if __name__ == 'main':
    unittest.main()
