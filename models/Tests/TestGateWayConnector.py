import unittest,time,uuid
from GateWayConnector.versions.v1.models import GateWayConnector
from Company.versions.v1.models import Company
from mongoengine import *
connect('testor')
#Check that there are a 100.00 transactions and no 5.00 transactions
#Have 3 plans test1, test2, test3
#Have a subscriptions with test1,test3 respectively
#Test 3 has a trial
#Check for a pending item of 9.99
#Check for a 61.00 charge
class testor(DynamicDocument):
    gateWayConnector=EmbeddedDocumentField('GateWayConnector')
    @classmethod
    def create(cls):
        gateWayConnector=GateWayConnector.create(
            connector='stripe',
            currency='CAD'
        )
        g=cls(gateWayConnector=gateWayConnector)
        g.save()
        return g.id
class TestGateWayConnector(unittest.TestCase):
    def setUp(self):
        self.id=testor.create()
        self.testor=testor.objects(id=self.id)[0]
        self.gateWayConnector=self.testor.gateWayConnector
        self.gateWayConnector.initialize()
        self.gateWayConnector.stripeGateWayConnector.setStripe_user_id(
            'acct_103psN2ZQ5Pe7aai'
        )
    def test_GateWayConnector(self):
        self.assertTrue(
            hasattr(self.gateWayConnector,'stripeGateWayConnector')
        )
        self.assertTrue(
            hasattr(self.gateWayConnector,'PlanManager')
        )
        self.assertTrue(
            hasattr(self.gateWayConnector,'Credentials')
        )
    def test_PlanManager(self):
        self.gateWayConnector.PlanManager.update()
        plans=self.gateWayConnector.PlanManager.list()
        initialCount=len(plans)
        self.gateWayConnector.PlanManager.add(
            id=uuid.uuid4().hex[:8],
            name='test',
            amount=2000,
            interval='month',
            interval_count=8,
            trial_period_days=2
        )
        self.gateWayConnector.PlanManager.update()
        newList=self.gateWayConnector.PlanManager.list()
        newCount=len(newList)
        self.assertEqual(newCount,initialCount+1)
    def tearDown(self):
        self.testor.delete()
class TestCustomer(unittest.TestCase):
    def setUp(self):
        self.id=testor.create()
        self.testor=testor.objects(id=self.id)[0]
        self.gateWayConnector=self.testor.gateWayConnector
        self.gateWayConnector.initialize()
        self.gateWayConnector.stripeGateWayConnector.setStripe_user_id(
            'acct_103psN2ZQ5Pe7aai'
        )
        company=Company()
        company.GateWayConnector=self.gateWayConnector
        company.save()
        self.customerManager=GateWayConnector.CustomerManager(
            type='stripe',
            company=company
        )
        self.customer=self.customerManager.create(
            id='cus_AzqgdVqgbt0m25'
        )
    def test_Customer(self):
        try:
            exceptionHappened1=False
            self.customer.charge(amount=-100000)
        except:
            exceptionHappened1=True
        try:
            exceptionHappened2=False
            self.customer.charge(amount=0.3)
        except:
            exceptionHappened2=True
        self.assertTrue(exceptionHappened2)
        credit=self.customer.Credit.get()
        self.customer.Credit.use(amount=credit)
        self.assertEqual(self.customer.Credit.get(),0)
        self.customer.charge(amount=6100)
    def test_Credit(self):
        initialCredit=self.customer.Credit.get()
        amount1=10000
        amount2=2000
        self.customer.Credit.add(amount=amount1)
        #check for a charge of 100.00
        self.customer.Credit.use(amount=amount2)
        newAmountTest=initialCredit+amount1-amount2
        newAmountActual=self.customer.Credit.get()
        self.assertEqual(newAmountActual,newAmountTest)
        self.customer.update()
        newAmountActual=self.customer.Credit.get()
        self.assertEqual(newAmountActual,newAmountTest)
        self.customer.Credit.add(amount=500,charge=False)
        newAmountTest=newAmountTest+500
        actualValue=self.customer.Credit.get()
        self.assertEqual(actualValue,newAmountTest)
    def test_SubscriptionManager(self):
        self.assertTrue(
            self.customer.SubscriptionManager.hasPlan(id='test1')
        )
        self.assertTrue(
            self.customer.SubscriptionManager.hasPlan(id='test3')
        )
        self.assertFalse(
            self.customer.SubscriptionManager.hasPlan(id='test2')
        )
        self.assertFalse(
            self.customer.SubscriptionManager.get(plan='test2')
        )
        subscription=self.customer.SubscriptionManager.get(plan='test3')
        self.assertTrue(
            subscription is not False
        )
    def test_SubscriptionManager_Subscription(self):
        subscription=self.customer.SubscriptionManager.get(plan='test3')
        self.assertTrue(
            subscription.has('test3')
        )
        self.assertFalse(
            subscription.has('test2')
        )
        self.assertFalse(
            subscription.has('test1')
        )
        subscription.addItem(amount=999)
        self.assertTrue(
            subscription.isFirstPeriod()
        )
    def test_SubscriptionManager_Subscription_PlanManager(self):
        subscription=self.customer.SubscriptionManager.get(plan='test3')
        self.assertTrue(
            subscription.PlanManager.get(id='test3') is not False
        )
        self.assertTrue(
            subscription.PlanManager.get(id='test1') is False
        )
        self.assertEqual(
            len(subscription.PlanManager.list()),1
        )
if __name__ == 'main':
    unittest.main()
