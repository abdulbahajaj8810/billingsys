import time
from mongoengine import *
from models.Tasks.WebhookSender import sendWebhook
class Webhook(DynamicEmbeddedDocument):
    plan=StringField()
    quantity=IntField()
    constants=DictField()
    trialMode=BooleanField(default=False)
    @staticmethod
    def listToDict(input):
        if type(input) is dict or not type(input) is list:
            return input
        result={};
        for el in input:
            name=el.get('name',None)
            value=el.get('value',None)
            if name is None or value is None: return
            result[name]=value
        return result
    @classmethod
    def create(cls,plan,quantity,constants,trialMode):
        constants=cls.listToDict(input=constants)
        return cls(
            plan=plan,
            quantity=quantity,
            constants=constants,
            trialMode=trialMode
        )
    def send(self,endpoint,unitID,gateWayID):
        data={}
        data['plan']=self.plan
        data['quantity']=self.quantity
        data['unit']=unitID
        data['gateWayID']=gateWayID
        data['constants']=self.constants
        sendWebhook(endpoint=endpoint,data=data)
    def match(self,plan,trialMode,init=None,final=None,quantity=None):
        if plan is None or trialMode is None:
            return False
        match = True
        match &=(self.plan==plan) or (self.trialMode and trialMode)
        if quantity is None:
            match &= final >= self.quantity > init
        else:
            match &= self.quantity==quantity
        return match
    def json(self):
        result={}
        result['plan']=self.plan
        result['quantity']=self.quantity
        result['trialMode']=self.trialMode
        constants=[]
        for name in self.constants:
            constants.append({
                'name':  name,
                'value': self.constants[name]
            })
        result['constants']=constants

        return result
class WebhookManager(DynamicEmbeddedDocument):
    webhooks=EmbeddedDocumentListField('Webhook')
    p__root=None
    @classmethod
    def create(cls):
        webhookManager=cls()
        return webhookManager
    def set(self,plan,quantity,trialMode,constants):
        self.remove(
            plan=plan,
            quantity=quantity,
            trialMode=trialMode,
        )
        webhook=Webhook.create(
            plan=plan,
            quantity=quantity,
            trialMode=trialMode,
            constants=constants,
        )
        self.webhooks.append(webhook)
        return webhook
    def setWebhooks(self,webhooksInput):
        self.webhooks=[]
        for webhook in webhooksInput:
            plan=webhook.get('plan',None)
            quantity=webhook.get('quantity',None)
            constants=webhook.get('constants',[])
            trialMode=webhook.get('trialMode',False)

            validityCheck=True
            validityCheck &=(plan is not None) or (trialMode is True)
            validityCheck &=(quantity is not None)    
            if not validityCheck: continue

            self.set(
                plan=plan,
                quantity=quantity,
                constants=constants,
                trialMode=trialMode
            )
    def json(self):
        results=[]
        for webhook in self.webhooks:
            results.append(webhook.json())
        return results
    def remove(self,plan,quantity,trialMode):
        for counter in range(0,len(self.webhooks)):
            webhook=self.webhooks[counter]
            if webhook.match(plan=plan,quantity=quantity,trialMode=trialMode):
                del self.webhooks[counter]
                return
            counter+=1
    def get(self,plan,init,final,trialMode):
        for webhook in self.webhooks:
            if webhook.match(plan=plan,init=init,final=final,trialMode=trialMode):
                return webhook
        return None
    def list(self):
        return self.webhooks
    def initialize(self,root):
        self.p__root=root
    def send(self,endpoint,gateWayID,diff):
        plan=diff.get('plan',None)
        init=diff.get('init',None)
        final=diff.get('final',None)
        trialMode=diff.get('trialMode',False)
        unitID=diff.get('id',None)
        webhook=self.get(
            plan=plan,
            init=init,
            final=final,
            trialMode=trialMode)
        if webhook is None: return
        webhook.send(unitID=unitID,endpoint=endpoint,gateWayID=gateWayID)
class UnitManagerLocker(DynamicDocument):
    company=ReferenceField('Company')
    gateWayID=StringField(default=None)
    timestamp=IntField(default=0)
    @classmethod
    def create(cls):
        logger=cls()
        return logger
    def lock(self,company,gateWayID):
        stamp=int(time.time()*(10**9))
        self.timestamp=stamp
        self.company=company
        self.gateWayID=gateWayID
        self.save()
        self.wait()
    def unlock(self):
        self.delete()
        return
    def wait(self):
        while True:
            result=UnitManagerLocker.objects(company=self.company, 
                gateWayID=self.gateWayID).order_by('timestamp')
            if len(result)<=1: break
            result=result[0]
            if str(result.id)==str(self.id): break
            time.sleep(1)
        return
class Quantity(DynamicEmbeddedDocument):
    p__root=None
    p__value=IntField()
    p__resetEachInterval=BooleanField(default=False)
    @classmethod
    def create(cls):
        q=cls(p__value=0,p__resetEachInterval=False)
        return q
    def edit(self,resetEachInterval=None):
        if resetEachInterval is not None:
            if resetEachInterval is True:
                self.p__resetEachInterval=True
            if resetEachInterval is False:
                self.p__resetEachInterval=False
    def value(self):
        return self.p__value
    def set(self,quantity):


        self.p__value=quantity



        return self.value()
    def add(self,quantity):
        # temp=self.p__value
        self.p__value+=quantity
        # try:
        #     self.digest()
        # except Exception as exp:
        #     self.p__value=temp
        #     raise exp
        # self.announceQuantityChange()
        return self.value()
        quantity=self.value()
    def initialize(self,root):
        self.p__root=root
    def restart(self):
        if self.p__resetEachInterval:
            self.set(quantity=0)
    def json(self):
        returnVal={}
        returnVal['value']=self.p__value
        returnVal['resetEachInterval']=self.p__resetEachInterval
        return returnVal
class Rule(DynamicEmbeddedDocument):
    above=FloatField()
    each=FloatField()
    cost=FloatField()
    @classmethod
    def create(cls,above,cost,each=1):
        if type(each) is not int and type(each) is not float:
            each=1
        return cls(above=above,cost=cost,each=each)
class Pricing(DynamicEmbeddedDocument):
    p__trialMode=BooleanField(default=False)
    p__plan=StringField(default=None)
    p__root=None
    p__model=StringField()
    __Rules=EmbeddedDocumentListField('Rule')
    def isTrialMode(self):
        return self.p__trialMode
    @classmethod
    def create(cls,plan=None,trialMode=False):
        pricing=cls(p__plan=plan,p__trialMode=trialMode)
        pricing.p__model='volume'
        return pricing
    def getRoot(self):
        return self.p__root
    def list(self):
        return self.__Rules
    def addRule(self,above,cost,each=1):
        newRule=Rule.create(above=above,cost=cost,each=each)
        self.__Rules.append(newRule)
    def getRule(self,below=None,equal=None,index=False):
        counter=False
        if equal is not None:
            exactly=False
            for rule in self.list():
                if counter is False:
                    counter=0
                else:
                    counter+=1
                if rule.above==equal:
                    exactly=rule
                    if index: return counter
                    break
            if not index: return exactly
            else: return False
        #highest above that is lower or equal to below
        elif below is not None:
            lowest=False
            for rule in self.list():
                if counter is False:
                    counter=0
                else:
                    counter+=1
                if below >= rule.above:
                    if lowest is False:
                        lowest=rule
                    elif rule.above > lowest.above:
                        lowest=rule
            if not index: return lowest
        return counter
    def initialize(self,root):
        self.p__root=root
    def getModel(self):
        return self.p__model
    def edit(self,model=None):
        if model is not None:
            model=model.lower()
            if model in ["volume","stairstep",'package']:
                self.p__model=model
    def getPlan(self):
        return self.p__plan
    def json(self):
        pricing={}
        pricing['plan']=self.p__plan
        pricing['trialMode']=self.p__trialMode
        pricing['model']=self.p__model
        pricing['rules']=[]
        for rule in self.list():
            pricing['rules'].append(dict(
                above=rule.above,
                cost=rule.cost,
                each=rule.each
            ))
        return pricing
    def cost(self):
        def volume(pricing):
            quantity=pricing.getRoot().Quantity.value()
            if quantity==0: return 0
            rule=pricing.getRule(below=quantity)
            if rule:
                cost=rule.cost
            else:
                return 0
            return float(cost)*(float(quantity)/float(rule.each))
        def stairstep(pricing):
            quantity=pricing.getRoot().Quantity.value()
            if quantity==0: return 0
            cost=0
            while True:
                rule=pricing.getRule(below=quantity)
                if not rule:
                    break
                cost+=float((quantity-rule.above+1))*float(rule.cost)
                quantity=rule.above-1
            return cost
        def package(pricing):
            quantity=pricing.getRoot().Quantity.value()
            rule=pricing.getRule(below=quantity)
            if not rule: return 0
            return rule.cost
        if self.p__model=="stairstep": cost=stairstep(pricing=self)
        if self.p__model=="volume": cost=volume(pricing=self)
        if self.p__model=="package": cost=package(pricing=self)
        return int(cost)
    def reset(self):
        self.__Rules=[]
class PricingManager(DynamicEmbeddedDocument):
    __pricings=EmbeddedDocumentListField('Pricing')
    p__root=None
    @classmethod
    def create(cls):
        pricingManager=cls()
        pricingManager.add(trialMode=True)
        return pricingManager
    def initialize(self,root):
        self.p__root=root
        for pricing in self.__pricings:
            pricing.initialize(root=root)
    def add(self,plan=None,trialMode=False):
        if trialMode is not False:
            self.delete(trialMode=trialMode)
            plan=None
        else:
            self.delete(plan=plan)
        pricing=Pricing.create(plan=plan,trialMode=trialMode)
        pricing.initialize(root=self.p__root)
        self.__pricings.append(pricing)
        return pricing
    def get(self,plan=None,trialMode=False):
        if plan is not None:
            for pricing in self.list():
                if plan==pricing.getPlan():
                    return pricing
        elif trialMode is not False:
            for pricing in self.list():
                if pricing.isTrialMode():
                    if len(pricing.list())==0:
                        return False
                    return pricing
        return False
    def delete(self,plan=None,trialMode=None):
        if plan is not None:
            counter=0
            for pricing in self.list():
                if plan==pricing.getPlan():
                    del self.list()[counter]
                    return True
                counter+=1
        if trialMode is not None:
            counter=0
            for pricing in self.list():
                if pricing.isTrialMode():
                    del self.list()[counter]
                    return True
                counter+=1
        return False
    def list(self):
        return self.__pricings
    def set(self,pricings):
        self.__pricings=[]
        for pricing in pricings:
            plan=pricing.get('plan',None)
            rules=pricing.get('rules',None)
            model=pricing.get('model',None)
            trialMode=pricing.get('trialMode',False)
            if plan is None and trialMode is False: continue
            pricing=self.add(plan=plan,trialMode=trialMode)
            pricing.edit(model=model)
            for rule in rules:
                above=rule.get('above',None)
                cost=rule.get('cost',None)
                each=rule.get('each',1)
                if above is None or cost is None: continue
                pricing.addRule(above=above,cost=cost,each=each)
        if not self.get(trialMode=True):
            self.add(trialMode=True)
    def json(self):
        response=[]
        for pricing in self.list():
            response.append(pricing.json())
        return response
class InvoiceGenerator(DynamicEmbeddedDocument):
    p__root=None          
    atEnd=BooleanField(default=True)
    @classmethod
    def create(cls):
        return cls()
    def initialize(self,root):
        self.p__root=root
    def getPricing(self):
        customerInfo=self.getCustomerInfo()
        if customerInfo is None: return None
        pricing=customerInfo.get('pricing',None)
        if pricing is None: return None
        return pricing
    def isOnTrialMode(self):
        pricing=self.getPricing()
        if pricing is None: return None
        return pricing.isTrialMode
    def getPlan(self):
        pricing=self.getPricing()
        if pricing is None: return None
        if pricing.isTrialMode:
            return None
        return pricing.getPlan()
    def getCustomerInfo(self):
        gateWayCustomer=self.p__root.gateWayCustomer

        selectedPricing=None
        selectedSubscription=None
        for pricing in self.p__root.PricingManager.list():
            if len( pricing.list() ) == 0: continue
            subscription=gateWayCustomer.SubscriptionManager.get(plan=pricing.getPlan())
            if subscription is not False:
                selectedPricing=pricing
                selectedSubscription=subscription
                if subscription.isOnTrial():
                    selectedPricing=self.p__root.PricingManager.get(trialMode=True)
                return {
                    'pricing': selectedPricing,
                    'subscription': selectedSubscription,
                    'gateWayCustomer': gateWayCustomer
                }

        return None
    def generate(self,subscriptionID):
        customerInfo=self.getCustomerInfo()
        if customerInfo is None:
            return None
        pricing=customerInfo.get('pricing')
        subscription=customerInfo.get('subscription')
        if not (subscription.getID()==subscriptionID):
            return None
        if self.atEnd and subscription.isFirstPeriod():
            return None
        gateWayIDCustomer=customerInfo.get('gateWayCustomer',None)
        if gateWayIDCustomer is None: return None
        amount=pricing.cost()
        subscription.addItem(amount=amount)
        self.p__root.Quantity.restart()
        usage={}
        usage['cost']=amount
        usage['planID']=pricing.getPlan()
        usage['isTrialMode']=pricing.isTrialMode()
        return usage
    def edit(self,atEnd=None):
        if atEnd is False:
            self.atEnd=False
        if atEnd is True:
            self.atEnd=True
    def json(self):
        response={}
        response['atEnd']=self.atEnd
        return response
class Unit(DynamicEmbeddedDocument):
    unitID=StringField()
    name=StringField()
    description=StringField()
    gateWayCustomer=None
    Quantity=EmbeddedDocumentField('Quantity')
    PricingManager=EmbeddedDocumentField('PricingManager')
    InvoiceGenerator=EmbeddedDocumentField('InvoiceGenerator')
    custom=BooleanField(default=False)
    WebhookManager=EmbeddedDocumentField('WebhookManager')
    def getWebhookManager(self):
        return self.WebhookManager
    def isCustom(self):
        return self.custom
    def getName(self):
        return self.name
    @classmethod
    def create(cls,id,name,description):
        unit=cls(unitID=id,name=name,description=description)
        unit.Quantity=Quantity.create()
        unit.InvoiceGenerator=InvoiceGenerator.create()
        unit.PricingManager=PricingManager.create()
        unit.WebhookManager=WebhookManager.create()
        return unit
    def getID(self):
        return self.unitID
    def edit(self,name=None,description=None,custom=None):
        if name is not None:
            name=str(name)
            self.name=name
        if description is not None:
            description=str(description)
            self.description=description
        if custom is not None and type(custom) is bool:
            self.custom=custom
    def initialize(self,gateWayCustomer=None):
        if gateWayCustomer is not None:
            self.gateWayCustomer=gateWayCustomer
        self.Quantity.initialize(root=self)
        self.PricingManager.initialize(root=self)
        self.InvoiceGenerator.initialize(root=self)
        self.WebhookManager.initialize(root=self)
    def json(self):
        unit={}
        unit['id']=self.unitID
        unit['name']=self.name
        unit['description']=self.description
        unit['quantity']=self.Quantity.json()
        unit['pricing']=self.PricingManager.json()
        unit['invoiceGenerator']=self.InvoiceGenerator.json()
        unit['webhooks']=self.WebhookManager.json()
        return unit
class UnitManager(DynamicEmbeddedDocument):
    p__gateWayCustomer=None
    p__units=EmbeddedDocumentListField('Unit')
    p__exceptionManager=None
    @classmethod
    def create(cls):
        unitManager=cls()
        return unitManager
    def add(self,id=None,name=None,description=None,unit=None):
        if (id is None or name is None or description is None) and unit is None:
            return None
        if id is None:
            id=unit.getID()
        self.p__verifyID(id=id)
        if unit is None:
            unit=Unit.create(id=id,name=name,description=description)

        self.p__units.append(unit)
        return unit
    def list(self):
        return self.p__units
    def get(self,id):
        gateWayCustomer=self.p__gateWayCustomer
        for unit in self.list():
            if unit.getID()==id:
                unit.initialize(gateWayCustomer=gateWayCustomer)
                return unit
        return None
    def p__verifyID(self,id):
        for unit in self.p__units:
            if unit.getID()==id:
                raise self.p__exceptionManager.getException(type='UnitIDExist')
    def initialize(self,gateWayCustomer=None):
        self.p__gateWayCustomer=gateWayCustomer
        for unit in self.p__units:
            unit.initialize(gateWayCustomer=gateWayCustomer)
    def json(self,defaultUnitManager=None):
        units=[]
        idsList=[]
        for unit in self.p__units:
            idsList.append(unit.getID())
            units.append(unit.json())
        if defaultUnitManager is not None:
            for unit in defaultUnitManager.list():
                if unit.getID() not in idsList:
                    units.append(unit.json())        
        return units
    def remove(self,id):
        index=0
        for unit in self.list():
            if unit.getID()==id:
                del self.list()[index]
                return True
            index+=1
        return False
    def generateInvoices(self,subscriptionID):
        usageData=[]
        for unit in self.list():
            quantity=unit.Quantity.value()
            usage=unit.InvoiceGenerator.generate(subscriptionID=subscriptionID)
            if usage is None: continue
            usage['unitID']=unit.getID()
            usage['quantity']=quantity
            usageData.append(usage)
        return usageData
    def append(self,unit):
        self.p__verifyID(id=unit.getID())
        self.p__units.append(unit)
    def getOrAdd(self,unit):
        currentUnit=self.get(id=unit.getID())
        if currentUnit is None:
            if unit is None: return None
            self.add(unit=unit)
            currentUnit=unit
        return currentUnit



