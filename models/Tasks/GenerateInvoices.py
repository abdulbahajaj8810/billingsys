from billingSys.celery import app
from mongoengine import connect
from models.Company import Company
connect(host='mongodb://localhost:27017/testor')
@app.task()
def generateInvoices(companyID,gateWayID,subscriptionID):
	company=Company.get(id=companyID)
	customer=company.getCustomer(gateWayID=gateWayID)
	customer.generateInvoices(subscriptionID=subscriptionID)
