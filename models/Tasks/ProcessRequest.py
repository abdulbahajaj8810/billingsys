from billingSys.celery import app
from time import sleep
from models.Company import Company
from mongoengine import connect
import json
from models.UnitManager import UnitManagerLocker
from models.Logger import Logger,Response
connect(host='mongodb://localhost:27017/testor')
'''
    data={
        gateWayID: 'gateWayID',
        change: [
            {
                id: 'calls',
                quantity: 10,
                action: 'add'/'set'
            }
        ]
    }
	[{id: 'calls',quantity: 10,action: 'add'/'set'}]
'''
class Responses(object):
	@staticmethod
	def WrongAPIKey():
		status='ERROR'
		message='The provided api key is wrong'
		code='WrongAPIKey'
		return Response.create(status=status,message=message,code=code)
	@staticmethod
	def MissingArgument(argument):
		status='ERROR'
		message=argument + ' is missing'
		code='MissingArgument'
		return Response.create(status=status,message=message,code=code)
	@staticmethod
	def NotAValidType(enteredType,realType):
		status='ERROR'
		message=enteredType + ' is not a valid ' + realType
		code='NotAValidType'
		return Response.create(status=status,message=message,code=code)
	@staticmethod
	def SuccessfulRequest():
		status='SUCCESS'
		message="Your request has been processed successfully"
		code='SuccessfulRequest'
		return Response.create(status=status,message=message,code=code)
	@staticmethod
	def NotFormatedProperly(entity):
		status='ERROR'
		message=entity + " is not formated properly"
		code='NotFormatedProperly'
		return Response.create(status=status,message=message,code=code)
	@staticmethod
	def DoesNotExist(entity,entityType):
		status='ERROR'
		message=entityType+ " " + entity + " does not exist"
		code='NotFormatedProperly'
		return Response.create(status=status,message=message,code=code)
class Log(object):
	request=None
	response=None
	endpoint=None
	company=None
	customer=None
	def add(self,request=None,
		response=None,endpoint=None,
		company=None,customer=None):
		if request is not None:
			self.request=request
		if response is not None:
			self.response=response
		if endpoint is not None:
			self.endpoint=endpoint
		if company is not None:
			self.company=company
		if customer is not None:
			self.customer=customer
	def validityCheck(self):
		check=True
		check &= (self.request is not None)
		check &= (self.response is not None)
		check &= (self.endpoint is not None)
		if not check:
			raise Exception("One or more log arguments are missing")
	def log(self):
		self.validityCheck()
		Logger.create(request=self.request,
			response=self.response,
			company=self.company,
			customer=self.customer,
			endpoint=self.endpoint)
def lockDec(func):
	def wrapper(*args,**kwargs):
		logger=Log()
		kwargs['logger']=logger
		locker=UnitManagerLocker.create()
		kwargs['locker']=locker

		output=func(*args,**kwargs)

		locker.unlock()
		logger.log()
		return output
	return wrapper


def parseRequest(logger, request):
	gateWayID=request.get('gateWayID',None)
	changes=request.get('changes',None)
	APIKey=request.get('APIKey',None)
	logger.add(request=request,endpoint='api/change')

	if changes is None:
		logger.add(response=Responses.MissingArgument(argument='changes'))
		raise Exception
	try:
		changes=json.loads(changes)
	except:
		logger.add(response=Responses.NotFormatedProperly(entity='changes'))
		raise Exception
	request['changes']=changes
	if APIKey is None:
		logger.add(response=Responses.MissingArgument(argument='APIKey'))
		raise Exception

	if gateWayID is None:
		logger.add(response=Responses.MissingArgument(argument='gateWayID'))
		raise Exception

	return (APIKey,changes,gateWayID)

def loadCompanyInfo(logger,APIKey):
	company=Company.objects(APIKey=APIKey)
	if len(company) != 1:
		logger.add(response=Responses.WrongAPIKey())
		raise Exception

	company=company[0]
	logger.add(company=company)

	endpoint=company.getEndPoint()
	defaultUnitManager=company.getDefaultPricing()
	return (defaultUnitManager,endpoint,company)

def loadCustomerInfo(logger,locker,gateWayID,company):
	locker.lock(gateWayID=gateWayID,company=company)
	customer=company.getCustomer(gateWayID=gateWayID)
	if customer is None:
		logger.add(response=Responses.NotAValidType(enteredType=gateWayID,realType='gateWayID'))
		raise Exception
	logger.add(customer=customer)
	unitManager=customer.getUnitManager()

	return (unitManager,customer)


def parseChange(logger,change,defaultUnitManager,unitManager):
	if type(change) is not dict:
		logger.add(response=Responses.NotFormatedProperly(entity='changes'))
		raise Exception
	id=change.get('id',None)
	quantity=change.get('quantity',None)
	action=change.get('action',None)
	if id is None:
		logger.add(response=Responses.MissingArgument(argument='id'))
		raise Exception
	if quantity is None:
		logger.add(response=Responses.MissingArgument(argument='quantity'))
		raise Exception
	if action is None:
		logger.add(response=Responses.MissingArgument(argument='action'))
		raise Exception
	try:
		quantity=int(quantity)
	except:
		logger.add(response=Responses.MissingArgument(argument='action'))
		raise Exception # quantity has to be an int


	defaultUnit=defaultUnitManager.get(id=id)
	unit=unitManager.getOrAdd(unit=defaultUnit)
	if unit is None:
		logger.add(response=Responses.DoesNotExist(entity=id,entityType='unit'))
		return ERROR #wrong unit id
	webhookManager=unit.getWebhookManager()


	return (id,unit,webhookManager,quantity,action)


ERROR='error'
SUCCESS='success'
@app.task()
@lockDec
def processAPIRequest(request,logger,locker):

	try:
		APIKey,changes,gateWayID=parseRequest(logger=logger, request=request)
		defaultUnitManager,endpoint,company=loadCompanyInfo(logger=logger,APIKey=APIKey)
		unitManager,customer=loadCustomerInfo(logger=logger,locker=locker,gateWayID=gateWayID,company=company)
	except:
		return ERROR

	diffList=[]

	for change in changes:
		try:
			id,unit,webhookManager,quantity,action=parseChange(
				logger=logger,
				change=change,
				defaultUnitManager=defaultUnitManager,
				unitManager=unitManager,
			)
		except:
			return ERROR

		diff={}
		diff['init']=unit.Quantity.value()
		if action=='add':
			unit.Quantity.add(quantity=quantity)
		elif action=='set':
			unit.Quantity.set(quantity=quantity)
		else:
			logger.add(response=Responses.NotAValidType(enteredType=action,realType='action'))
			return ERROR
		diff['final']=unit.Quantity.value()
		diff['trialMode']=unit.InvoiceGenerator.isOnTrialMode()
		diff['id']=id
		diff['plan']=unit.InvoiceGenerator.getPlan()
		diffList.append((diff,webhookManager))

	customer.save()
	locker.unlock()

	for diffItem in diffList:
		diff, webhookManager=diffItem
		webhookManager.send(diff=diff,
			endpoint=endpoint,
			customer=customer,
			company=company,	
		)

	logger.add(response=Responses.SuccessfulRequest())
	return SUCCESS