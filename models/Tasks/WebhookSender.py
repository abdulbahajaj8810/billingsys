from billingSys.celery import app
from mongoengine import connect
import requests
connect(host='mongodb://localhost:27017/testor')
ERROR='error'
SUCCESS='success'
@app.task(bind=True,max_retries=3)
def webhookTrial(self,endpoint,data):
	log=Logger.create(request=self.request,
		response=self.response,
		company=self.company,
		customer=self.customer,
		endpoint=endpoint,
		event=Logger.Events.sentWebhook
	)
	data['id']=str(log.id)
	request=requests.get(url=endpoint,data=data)
	status=str(request.status_code)[0]
	if not (status=='2'):
		self.retry()
		return ERROR
	return SUCCESS
def sendWebhook(endpoint,data):
	webhookTrial.delay(endpoint,data)
from models.Logger import Logger
