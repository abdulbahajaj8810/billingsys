'''
identified by company name, email address, password
has permissions
stores encrypted companyKey
can issue invitations
can retrieve customer
can retrieve company
can retrieve API keys
can authenticate given company name, email, password, apikey
'''
#Change password
#Change email
#change api key
from django.contrib.auth.models import User
from Crypto.Cipher import AES
import uuid,time
from models.Company import Company

from django.contrib.auth import authenticate, login
from mongoengine import *
class Member(DynamicDocument):
    email=StringField(default=None)
    company=ReferenceField('Company')
    @classmethod
    def create(cls,email,password,company):
        member=cls(email=email)
        #Case new company
        if not cls.integrityCheck(member=member):
            return False
        member.company=company
        member.save()
        member.createSystemUser(password=password)
        return member
    @classmethod
    def integrityCheck(self,member=None):
        members=Member.objects(
            email=member.email,
        )
        if len(members) != 0:
            return False
        return True
    def createSystemUser(self,password):
        return User.objects.create_user(
            username=str(self.id),
            password=password
        )
    def getSystemUser(self):
        return User.objects.get(username__exact=str(self.id))
    @classmethod
    def get(cls,password,email=None,company=None,request=None):
        if email is not None:
            member=Member.objects(
                email=email,
            )
        elif company is not None:
            member=cls.objects(company=company)
        if len(member) != 1:
            return -1 #Member is not found
        member=member[0]
        if not member.authenticate(password=password,request=request):
            return -2 #Company name, email, and password didn't match
        return member
    @classmethod
    def getByID(cls,id):
        member=cls.objects(id=id)
        if len(member) == 1:
            member=member[0]
            return member
        return None
    def authenticate(self,password,request=None):
        user=authenticate(username=self.id,password=password)
        if user is not None:
            if request is not None:
                login(request, user)
            return True
        return False
    def getAPIKey(self,password):
        if self.authenticate(password=password) is not True:
            return False
        return self.company.getAPIKey()
    def getCustomer(self,gateWayID):
        customer=Customer.get(
            gateWayID=gateWayID,
            company=self.company
        )
        customer.initialize()
        return customer
    def remove(self):
        Member.Invitation.objects(issuedBy=self).delete()
        User.objects.get(username=str(self.id)).delete()
        self.delete()
    def cancelInvitations(self):
        for invitation in invitations:
            invitation.disable()
        return True
    @classmethod
    def login(cls,request,email,password):
        member=cls.get(
            email=email,
            password=password,
            request=request
        )
        if isinstance(member,Member):
            return True
        return member
    def getCompany(self):
        return self.company
    def edit(self,email=None):
        if email is not None:
            self.email=email
            self.save()
    def editPassword(self,currentPassword,newPassword):
        if not self.authenticate(password=currentPassword):
            return False
        systemUser=self.getSystemUser()
        systemUser.set_password(newPassword)
        systemUser.save()
    def json(self):
        response={}
        response['id']=str(self.id)
        response['email']=self.email
        return response
    def resetAPI(self,password):
        if not self.authenticate(password=password):
            return None
        company=self.getCompany()
        company.setAPIKey()
