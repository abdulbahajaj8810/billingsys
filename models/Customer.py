from models.UnitManager import UnitManager
from mongoengine import *
from models.GateWayConnector import GateWayConnector
import datetime,time
class CompanyCustomer(DynamicDocument):
    gateWayID=StringField()
    company=ReferenceField('Company')
    UnitManager=EmbeddedDocumentField('UnitManager')
    @classmethod
    def create(cls,gateWayID,company,gateWayCustomer=None):
        if not cls.__integrityCheck(company=company,gateWayID=gateWayID): 
            return False
        customer=cls()
        customer.gateWayID=gateWayID
        customer.company=company
        customer.UnitManager=UnitManager.create()
        customer.save()
        customer.initialize(gateWayCustomer=gateWayCustomer)
        return customer
    @classmethod
    def __integrityCheck(cls,gateWayID,company):
        if cls.objects(
            gateWayID=gateWayID,
            company=company
        ): return False
        return True
    def initialize(self,gateWayCustomer=None):
        print 'CompanyCustomer.initialize'
        gateWayConnector=self.company.GateWayConnector
        gateWayConnector.initialize()        
        if gateWayCustomer is None:
            gateWayCustomer=self.getCustomerObject()
            print 'gateWayCustomer is: ',gateWayCustomer
        if gateWayCustomer is not False and gateWayCustomer is not None:
            gateWayCustomer.initialize(root=gateWayConnector)
        self.UnitManager.initialize(gateWayCustomer=gateWayCustomer)
    @classmethod
    def get(cls,gateWayID,company):
        customer=cls.objects(
            company=company,
            gateWayID=gateWayID
        )
        if len(customer) != 1: return False
        customer=customer[0]
        customer.initialize()
        return customer
    def getCustomerObject(self):
        customerManager=GateWayConnector.CustomerManager(
            type='stripe',
            company=self.company
        )
        customer=customerManager.get(
            id=self.gateWayID
        )
        return customer
    def getUnitManager(self):
        return self.UnitManager
    def json(self):
        unitManager=self.getUnitManager()
        defaultUnitManager=self.company.getDefaultPricing()
        response={}
        response['gateWayID']=self.gateWayID
        response['units']=unitManager.json(defaultUnitManager=defaultUnitManager)
        return response
    def generateInvoices(self,subscriptionID):
        unitManager=self.getUnitManager()
        usageData=unitManager.generateInvoices(subscriptionID=subscriptionID)
        for usage in usageData:
            usage['customer']=self
            usage['company']=self.company
            HistoryLogger.log(**usage)
    def sync(self):
        customerObj=self.getCustomerObject()
        if customerObj is None or customerObj is False: return False
        customerObj.update()
        return True
    @classmethod
    def syncAll(cls,company):
        GateWayConnector=company.GateWayConnector
        customerManager=GateWayConnector.CustomerManager(company=company)
        customerManager.updateAll()

        gatewayCustomers=customerManager.list()        
        for gatewayCustomer in gatewayCustomers:
            customer=cls.objects(
                company=company,
                gateWayID=gatewayCustomer.gateWayID        
            )
            if len(customer) > 0:
                customer=customer[0]
                return
            CompanyCustomer.create(
                gateWayID=gatewayCustomer.gateWayID,
                company=company,
                gateWayCustomer=gatewayCustomer
            )
    @classmethod
    def applyGrandfatheringPolicy(cls,company,grandFatherAllCustomers,
        grandfatherCustomersWithCustomPricing,
        unit=None):
        if grandFatherAllCustomers: return
        customersToUpdate=cls.objects(company=company)
        for customer in customersToUpdate:
            unitManager=customer.getUnitManager()
            currentUnit=unitManager.get(id=unit.getID())
            if currentUnit is None: continue
            if grandfatherCustomersWithCustomPricing:   
                if currentUnit.isCustom():
                    continue
            currentValue=currentUnit.Quantity.value()
            unit.Quantity.set(quantity=currentValue)
            unitManager.remove(id=unit.getID())
            unitManager.add(unit=unit)
            customer.save()

class HistoryLogger(DynamicDocument):
    quantity=IntField(default=0)
    cost=IntField(default=0)
    timestamp=IntField(default=0)
    day=IntField(default=0)
    month=IntField(default=0)
    year=IntField(default=0)
    unitID=StringField(default='')
    customer=ReferenceField('CompanyCustomer')
    company=ReferenceField('Company')
    planID=StringField(default=None)
    isTrialMode=BooleanField(default=False)
    @classmethod
    def log(cls,quantity,cost,customer,company,unitID,planID,isTrialMode):
        logger=cls()
        logger.customer=customer
        logger.company=company
        logger.quantity=quantity
        logger.cost=cost
        logger.unitID=unitID
        logger.planID=planID
        logger.isTrialMode=isTrialMode
        logger.timestamp=time.time()

        today=datetime.datetime.today()
        logger.day=today.day
        logger.month=today.month
        logger.year=today.year

        logger.save()
        return logger
    @classmethod
    def sum(cls,year,month,unitID,company=None,customer=None):
        query={}
        query['year']=year
        query['month']=month
        query['unitID']=unitID
        if company is not None:
            query['company']=company
        elif customer is not None:
            query['customer']=customer
        else:
            return None
        selection=cls.objects(**query)
        results={}
        results['quantity']=selection.sum('quantity')
        results['cost']=selection.sum('cost')
        if results['cost']==0 and results['quantity']==0: return None;
        return results
    @classmethod
    def list(cls,customer=None,company=None):
        today=datetime.datetime.today()
        thisYear=today.year
        thisMonth=today.month
        if company is not None:
            if len(cls.objects(company=company)) == 0:
                return None
            unitManager=company.getDefaultPricing()
        elif customer is not None:
            if len(cls.objects(customer=customer)) == 0:
                return None
            unitManager=customer.getUnitManager()
        else:
            return None
        units=unitManager.list()
        results={}
        results['units']={}
        results['total']={'year': thisYear, 'cost': 0}
        aHit=False
        for unit in units:
            unitID=unit.getID()
            if unitID is None: continue
            results['units'][unitID]={}
            for year in range(2017,thisYear+1):
                for month in range(1,13):
                    if (thisMonth+1)==month and thisYear==year: break
                    sum=cls.sum(unitID=unitID,year=year,
                        month=month,customer=customer,company=company)
                    if sum is None: continue
                    aHit=True
                    if results['units'][unitID].get(year,None) is None:
                        results['units'][unitID][year]={}
                    results['units'][unitID][year][month]=sum
                    if year==thisYear:
                        results['total']['cost']+=sum.get('cost',0)
        if aHit is not True:
            return None
        return results

