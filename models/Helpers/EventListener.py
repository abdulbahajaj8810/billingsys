class EventListener(object):
    __events=None
    def __init__(self):
        self.__events={}
    @classmethod
    def create(cls):
        return cls()
    def list(self):
        return self.__events
    def add(self,name):
        self.__events[name]=self.Event()
        return self.__events[name]
    def get(self,name):
        return self.__events.get(name,False)
    def announce(self,name,**args):
        event=self.get(name=name)
        # print "announcing "+name, " quantity: " + str(args['quantity'])
        if event:
            event.announce(**args)
            return True
        return False
    def register(self,name,function):
        event=self.get(name=name)
        if event:
            event.register(function=function)
            return True
        return False
    class Event(object):
        __registered=None
        def __init__(self):
            self.__registered=[]
        def register(self,function):
            self.__registered.append(function)
        def announce(self,**args):
            for function in self.__registered:
                function(**args)
            return True
        def clear(self):
            self.__registered=[]
