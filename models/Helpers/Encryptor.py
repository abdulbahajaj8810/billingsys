from Crypto.Cipher import AES
from Crypto import Random
import bson
class Encryptor(object):
    password=None
    IV=None
    def __init__(self,password,IV=None):
        self.password=password
        if IV is None:
            self.IV=bson.Binary(Random.new().read(16))
        else:
            self.IV=IV
    def encrypt(self,text):
        encryptionOBJ=self.createEncryptionObject()
        encryptedText=encryptionOBJ.encrypt(text)
        encryptedText=bson.Binary( encryptedText )
        return encryptedText
    def decrypt(self,text):
        encryptionOBJ=self.createEncryptionObject()
        return encryptionOBJ.decrypt(text)
    def getIV(self):
        IV=self.IV
        return IV
    def createEncryptionObject(self):
        if not self.normalize():
            return False
        return AES.new(
            self.password,
            AES.MODE_CBC,
            self.IV
        )

    def normalize(self):
        if len(self.password) > 32:
            return False
        while len(self.password) != 32:
            self.password+='c'
        return True
# AES.block_size
'''
from Helpers.versions.v1.Encryptor import Encryptor
r=Encryptor(password='11')
r.createEncryptionObject()
'''
