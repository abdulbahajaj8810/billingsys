class ExceptionManager(object):
    __types=None
    def __init__(self):
        self.__types={}
    @classmethod
    def create(cls):
        return cls()
    def add(self,type,message=None):
        typeInstance=self.get(type=type)
        if not typeInstance:
            typeInstance=self.Type.create(type=type)
            self.__types[type]=typeInstance
        if message:
            typeInstance.add(message=message)
        return typeInstance
    def get(self,type):
        type=self.__types.get(type,False)
        if not type:
            return False
        return type
    def getException(self,type,message=None,context={}):
        typeInstance=self.get(type=type)
        if not typeInstance:
            raise Exception("Exception type is not allowed. Given: "+type)
        return typeInstance.get(message=message,context=context)
    class Type(object):
        __messages=None
        __type=None
        def __init__(self,type):
            self.__messages={}
            self.__type=type
        @classmethod
        def create(cls,type):
            return cls(type=type)
        def add(self,message):
            self.__messages[message]=self.Message.create(type=self.__type,message=message)
        def get(self,message=None,context={}):
            if not message and len(self.__messages) > 1:
                raise Exception("Exception message required")
            elif len(self.__messages)==0:
                raise Exception("No messages provided")
            elif not message:
                for err in self.__messages:
                    message=self.__messages[err]
            else:
                message=self.__messages.get(message,False)
            if not message:
                raise Exception("message doesn't exist")
            return message.getException()
        class Message(object):
            type=None
            message=None
            def __init__(self,type,message):
                self.type=type
                self.message=message
            @classmethod
            def create(cls,type,message):
                return cls(type=type,message=message)
            def getException(self,context={}):
                return self.Instance.create(
                    type=self.type,message=self.message,context=context
                )
            class Instance(Exception):
                @classmethod
                def create(cls,type,message,context):
                    return cls(type=type,message=message,context=context)
                def __init__(self,type,message,context):
                    super(ExceptionManager.Type.Message.Instance, self).__init__(message)
                    self.type=type
                    self.context=context
