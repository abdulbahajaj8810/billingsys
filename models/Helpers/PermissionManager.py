import inspect
def Enforce(AllowedPermissions):
    def replacer(func):
        def function(self_or_cls,member=None,*args,**kwargs):
            if member is not None:
                member_permission=member.permission
            elif self_or_cls is not None:
                member_permission=self_or_cls.permission
            if not member_permission in AllowedPermissions:
                return False
            print member_permission
            functionArguments=inspect.getargspec(func).args
            if 'member' in functionArguments:
                kwargs['member']=member
            if 'self' in functionArguments:
                kwargs['self']=self_or_cls
            if 'cls' in functionArguments:
                kwargs['cls']=self_or_cls
            # print kwargs
            # print functionArguments
            return func(*args,**kwargs)
        return function
    return replacer
