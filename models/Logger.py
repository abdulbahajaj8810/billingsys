from mongoengine import *
import datetime
import time,json as jsonLibrary

class Response(DynamicEmbeddedDocument):
    status=StringField()
    code=StringField()
    message=StringField()
    @classmethod
    def create(cls,status,code,message):
        response=cls()
        response.status=status
        response.code=code
        response.message=message
        return response
    def json(self):
        response={}
        response['status']=self.status
        response['code']=self.code
        response['message']=self.message
        return response
class Date(DynamicEmbeddedDocument):
    minute=IntField(default=None)
    hour=IntField(default=None)
    day=IntField(default=None)
    month=IntField(default=None)
    year=IntField(default=None)
    timestamp=IntField()
    @classmethod
    def create(cls):
        date=cls()
        today=datetime.datetime.today()
        date.day=today.day
        date.month=today.month
        date.year=today.year
        date.hour=today.hour
        date.timestamp=time.time()
        date.minute=today.minute
        return date
    def json(self):
        response={}
        response['minute']=self.minute
        response['hour']=self.hour
        response['day']=self.day
        response['month']=self.month
        response['year']=self.year
        response['timestamp']=self.timestamp
        return response
class Cache(DynamicEmbeddedDocument):
    company=ReferenceField('Company')
    customer=ReferenceField('CompanyCustomer')
    @classmethod
    def create(cls,company,customer):
        cache=cls()
        if company is not None:
            cache.company=company
        if customer is not None:
            cache.customer=customer
        return cache
    def json(self):
        response={}
        if self.company is not None:
            response['company']=self.company.json()
        if self.customer is not None:
            response['customer']=self.customer.json()
            return response
class Logger(DynamicDocument):
    class Events(object):
        allowed=[ 
            'sentWebhook',
            'quantityChange',
        ]
        def isAllowed(cls,eventName):
            return eventName in cls.allowed
    for event in Events.allowed:
        setattr(Events,event,event)
    request=StringField()
    response=EmbeddedDocumentField('Response')
    endpoint=StringField()
    cache=EmbeddedDocumentField('Cache')
    date=EmbeddedDocumentField('Date')
    event=StringField(default=None)
    @classmethod
    def create(cls,event,endpoint,request,response,company,customer):
        apiKey=request.get('APIKey',None)
        if apiKey is not None:
            request['APIKey']='****'
        request=jsonLibrary.dumps(request)
        cache=Cache.create(company=company,customer=customer)
        date=Date.create()
        log=cls(
            request=request,
            response=response,
            endpoint=endpoint,
            date=date,
            cache=cache,
            event=event,
        )
        log.save()
        return log
    @classmethod
    def get(cls,company,id):
        query={}
        query['id']=id
        query['cache__company']=company
        return cls.objects(**query)
    @classmethod
    def verify(cls,company,id):
        result=cls.get(company=company,id=id)
        if len(result)==1:
            return True
        return False
    def json(self):
        response={}
        response['id']=str(self.id)
        response['response']=self.response.json()
        response['request']=jsonLibrary.loads( self.request )
        response['endpoint']=self.endpoint
        response['status']=self.response.status
        response['date']=self.date.json()
        return response
    @classmethod
    def list(cls,company,step=0,customer=None,endpoint=None,all=False,json=False,**kwargs):
        listSize=25
        step+=listSize
        query={}
        query['cache__company']=company.id
        if endpoint is not None:
            query['endpoint']=endpoint
        if customer is not None:
            query['cache__customer']=customer.id
        results=cls.objects(**query).order_by("-date__timestamp")
        if not all:
            results=results[ (step-listSize) : step ]
        else:
            step=None
        if json:
            temp=[]
            for log in results:
                temp.append(log.json())
            results=temp
        return (results,step)
# from models.Customer import CompanyCustomer
