"""billingSys URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from Public.views import accessible
# urlpatterns = [
#     # url(r'^api/v1/test/', test),
#     url(r'^api/login/', login ),
#     url(r'^api/register/', register ),
#     url(r'^api/load/all',GetResources.all),
#     url(r'^api/create/unit',Unit.create),
#     url(r'(?s).*',UI),
#
# ]
# print 'urls: ',accessible.urlPatterns
urlpatterns = accessible.urlPatterns
